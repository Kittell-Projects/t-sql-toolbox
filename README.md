T-SQL Toolbox
=============

Project Description
-------------------

T-SQL Toolbox is a MS SQL Server database with a set of handy utilities for T-SQL database developers.

**NOTE:** This project was originally pulled from [https://archive.codeplex.com/?p=tsqltoolbox](https://archive.codeplex.com/?p=tsqltoolbox)

Scripts
-------

*   **TSqlToolbox.sql**
    *   Original script with German and English text.
*   **TSqlToolbox-EN-US.sql**
    *   Modified script to replace German text with English text.

Modules and Features
--------------------

**DateTimeUtil**  
A set of functions and configuration data for extended datetime handling, e. g. it offers easy timezone conversion incl. support for daylight saving times using native T-SQL

The following functions are available:

*   UDF\_ConvertLocalToLocalByTimezoneId
*   UDF\_ConvertLocalToLocalByTimezoneIdentifier
*   UDF\_ConvertLocalToUtcByTimezoneId
*   UDF\_ConvertLocalToUtcByTimezoneIdentifier
*   UDF\_ConvertUtcToLocalByTimezoneId
*   UDF\_ConvertUtcToLocalByTimezoneIdentifier
*   UDF\_GetFirstWeekdayInMonth
*   UDF\_GetLastWeekdayInMonth
*   UDF\_GetStartOfDay
*   UDF\_GetEndOfDay
*   UDF\_GetStartOfMonth
*   UDF\_GetEndOfMonth
*   UDF\_GetStartOfQuarter
*   UDF\_GetEndOfQuarter
*   UDF\_GetStartOfWeek
*   UDF\_GetEndOfWeek
*   UDF\_GetStartOfYear
*   UDF\_GetEndOfYear