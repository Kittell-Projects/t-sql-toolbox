﻿USE [master]
GO
IF EXISTS (SELECT name FROM sys.databases WHERE name = N'TSqlToolbox')
BEGIN
    ALTER DATABASE [TSqlToolbox] SET SINGLE_USER WITH ROLLBACK IMMEDIATE
    DROP DATABASE [TSqlToolbox]
END
GO
CREATE DATABASE [TSqlToolbox]
GO

USE [TSqlToolbox]
GO
CREATE SCHEMA [DateTimeUtil]
GO
SET ANSI_NULLS ON
GO
SET ANSI_PADDING ON
GO
SET QUOTED_IDENTIFIER ON
GO



CREATE TABLE [DateTimeUtil].[Timezone](
	[Id] [int] NOT NULL,
	[Identifier] [nvarchar](100) NULL,
	[StandardName] [nvarchar](100) NULL,
	[DisplayName] [nvarchar](100) NULL,
	[DaylightName] [nvarchar](100) NULL,
	[SupportsDaylightSavingTime] [bit] NULL,
	[BaseUtcOffsetSec] [int] NULL,
 CONSTRAINT [PK_Timezone] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)
)
GO

INSERT INTO [DateTimeUtil].[Timezone] ([Id], [Identifier], [StandardName], [DisplayName], [DaylightName], [SupportsDaylightSavingTime], [BaseUtcOffsetSec]) VALUES (1, N'Dateline Standard Time', N'Datumsgrenze Normalzeit', N'(UTC-12:00) Internationale Datumsgrenze (Westen)', N'Datumsgrenze Sommerzeit', 0, -43200)
INSERT INTO [DateTimeUtil].[Timezone] ([Id], [Identifier], [StandardName], [DisplayName], [DaylightName], [SupportsDaylightSavingTime], [BaseUtcOffsetSec]) VALUES (2, N'UTC-11', N'UTC-11', N'(UTC-11:00) Koordinierte Weltzeit-11', N'UTC-11', 0, -39600)
INSERT INTO [DateTimeUtil].[Timezone] ([Id], [Identifier], [StandardName], [DisplayName], [DaylightName], [SupportsDaylightSavingTime], [BaseUtcOffsetSec]) VALUES (3, N'Hawaiian Standard Time', N'Hawaii Normalzeit', N'(UTC-10:00) Hawaii', N'Hawaii Sommerzeit', 0, -36000)
INSERT INTO [DateTimeUtil].[Timezone] ([Id], [Identifier], [StandardName], [DisplayName], [DaylightName], [SupportsDaylightSavingTime], [BaseUtcOffsetSec]) VALUES (4, N'Alaskan Standard Time', N'Alaska Normalzeit', N'(UTC-09:00) Alaska', N'Alaska Sommerzeit', 1, -32400)
INSERT INTO [DateTimeUtil].[Timezone] ([Id], [Identifier], [StandardName], [DisplayName], [DaylightName], [SupportsDaylightSavingTime], [BaseUtcOffsetSec]) VALUES (5, N'Pacific Standard Time (Mexico)', N'Pacific Normalzeit (Mexiko)', N'(UTC-08:00) Niederkalifornien', N'Pacific Sommerzeit (Mexiko)', 1, -28800)
INSERT INTO [DateTimeUtil].[Timezone] ([Id], [Identifier], [StandardName], [DisplayName], [DaylightName], [SupportsDaylightSavingTime], [BaseUtcOffsetSec]) VALUES (6, N'Pacific Standard Time', N'Pacific Normalzeit', N'(UTC-08:00) Pacific Zeit (USA & Kanada)', N'Pacific Sommerzeit', 1, -28800)
INSERT INTO [DateTimeUtil].[Timezone] ([Id], [Identifier], [StandardName], [DisplayName], [DaylightName], [SupportsDaylightSavingTime], [BaseUtcOffsetSec]) VALUES (7, N'US Mountain Standard Time', N'Mountain Normalzeit (Arizona)', N'(UTC-07:00) Arizona', N'Mountain Sommerzeit (Arizona)', 0, -25200)
INSERT INTO [DateTimeUtil].[Timezone] ([Id], [Identifier], [StandardName], [DisplayName], [DaylightName], [SupportsDaylightSavingTime], [BaseUtcOffsetSec]) VALUES (8, N'Mountain Standard Time (Mexico)', N'Mountain Normalzeit (Mexiko)', N'(UTC-07:00) Chihuahua, La Paz, Mazatlan', N'Mountain Sommerzeit (Mexiko)', 1, -25200)
INSERT INTO [DateTimeUtil].[Timezone] ([Id], [Identifier], [StandardName], [DisplayName], [DaylightName], [SupportsDaylightSavingTime], [BaseUtcOffsetSec]) VALUES (9, N'Mountain Standard Time', N'Mountain Normalzeit', N'(UTC-07:00) Mountain Zeit (USA & Kanada)', N'Mountain Sommerzeit', 1, -25200)
INSERT INTO [DateTimeUtil].[Timezone] ([Id], [Identifier], [StandardName], [DisplayName], [DaylightName], [SupportsDaylightSavingTime], [BaseUtcOffsetSec]) VALUES (10, N'Central Standard Time', N'Central Normalzeit', N'(UTC-06:00) Central Zeit (USA & Kanada)', N'Central Sommerzeit', 1, -21600)
INSERT INTO [DateTimeUtil].[Timezone] ([Id], [Identifier], [StandardName], [DisplayName], [DaylightName], [SupportsDaylightSavingTime], [BaseUtcOffsetSec]) VALUES (11, N'Central Standard Time (Mexico)', N'Central Normalzeit (Mexiko)', N'(UTC-06:00) Guadalajara, Mexiko-Stadt, Monterrey', N'Central Sommerzeit (Mexiko)', 1, -21600)
INSERT INTO [DateTimeUtil].[Timezone] ([Id], [Identifier], [StandardName], [DisplayName], [DaylightName], [SupportsDaylightSavingTime], [BaseUtcOffsetSec]) VALUES (12, N'Central America Standard Time', N'Mittelamerikanische Normalzeit', N'(UTC-06:00) Mittelamerika', N'Mittelamerikanische Sommerzeit', 0, -21600)
INSERT INTO [DateTimeUtil].[Timezone] ([Id], [Identifier], [StandardName], [DisplayName], [DaylightName], [SupportsDaylightSavingTime], [BaseUtcOffsetSec]) VALUES (13, N'Canada Central Standard Time', N'Central Normalzeit (Kanada)', N'(UTC-06:00) Saskatchewan', N'Central Sommerzeit (Kanada)', 0, -21600)
INSERT INTO [DateTimeUtil].[Timezone] ([Id], [Identifier], [StandardName], [DisplayName], [DaylightName], [SupportsDaylightSavingTime], [BaseUtcOffsetSec]) VALUES (14, N'SA Pacific Standard Time', N'Westl. Südamerika Normalzeit', N'(UTC-05:00) Bogotá, Lima, Quito, Rio Branco', N'Westl. Südamerika Sommerzeit', 0, -18000)
INSERT INTO [DateTimeUtil].[Timezone] ([Id], [Identifier], [StandardName], [DisplayName], [DaylightName], [SupportsDaylightSavingTime], [BaseUtcOffsetSec]) VALUES (15, N'Eastern Standard Time', N'Eastern Normalzeit', N'(UTC-05:00) Eastern Zeit (USA & Kanada)', N'Eastern Sommerzeit', 1, -18000)
INSERT INTO [DateTimeUtil].[Timezone] ([Id], [Identifier], [StandardName], [DisplayName], [DaylightName], [SupportsDaylightSavingTime], [BaseUtcOffsetSec]) VALUES (16, N'US Eastern Standard Time', N'Eastern Normalzeit (Indiana)', N'(UTC-05:00) Indiana (Ost)', N'Eastern Sommerzeit (Indiana)', 1, -18000)
INSERT INTO [DateTimeUtil].[Timezone] ([Id], [Identifier], [StandardName], [DisplayName], [DaylightName], [SupportsDaylightSavingTime], [BaseUtcOffsetSec]) VALUES (17, N'Venezuela Standard Time', N'Venezuela Normalzeit', N'(UTC-04:30) Caracas', N'Venezuela Sommerzeit', 0, -16200)
INSERT INTO [DateTimeUtil].[Timezone] ([Id], [Identifier], [StandardName], [DisplayName], [DaylightName], [SupportsDaylightSavingTime], [BaseUtcOffsetSec]) VALUES (18, N'Paraguay Standard Time', N'Paraguay Normalzeit', N'(UTC-04:00) Asuncion', N'Paraguay Sommerzeit', 1, -14400)
INSERT INTO [DateTimeUtil].[Timezone] ([Id], [Identifier], [StandardName], [DisplayName], [DaylightName], [SupportsDaylightSavingTime], [BaseUtcOffsetSec]) VALUES (19, N'Atlantic Standard Time', N'Atlantic Normalzeit', N'(UTC-04:00) Atlantic (Kanada)', N'Atlantic Sommerzeit', 1, -14400)
INSERT INTO [DateTimeUtil].[Timezone] ([Id], [Identifier], [StandardName], [DisplayName], [DaylightName], [SupportsDaylightSavingTime], [BaseUtcOffsetSec]) VALUES (20, N'Central Brazilian Standard Time', N'Zentalbrasilianische Normalzeit', N'(UTC-04:00) Cuiaba', N'Zentalbrasilianische Sommerzeit', 1, -14400)
INSERT INTO [DateTimeUtil].[Timezone] ([Id], [Identifier], [StandardName], [DisplayName], [DaylightName], [SupportsDaylightSavingTime], [BaseUtcOffsetSec]) VALUES (21, N'SA Western Standard Time', N'Mittl. Südamerika Normalzeit', N'(UTC-04:00) Georgetown, La Paz, Manaus, San Juan', N'Mittl. Südamerika Sommerzeit', 0, -14400)
INSERT INTO [DateTimeUtil].[Timezone] ([Id], [Identifier], [StandardName], [DisplayName], [DaylightName], [SupportsDaylightSavingTime], [BaseUtcOffsetSec]) VALUES (22, N'Pacific SA Standard Time', N'Chilenische Normalzeit', N'(UTC-04:00) Santiago', N'Chilenische Sommerzeit', 1, -14400)
INSERT INTO [DateTimeUtil].[Timezone] ([Id], [Identifier], [StandardName], [DisplayName], [DaylightName], [SupportsDaylightSavingTime], [BaseUtcOffsetSec]) VALUES (23, N'Newfoundland Standard Time', N'Neufundland Normalzeit', N'(UTC-03:30) Neufundland', N'Neufundland Sommerzeit', 1, -12600)
INSERT INTO [DateTimeUtil].[Timezone] ([Id], [Identifier], [StandardName], [DisplayName], [DaylightName], [SupportsDaylightSavingTime], [BaseUtcOffsetSec]) VALUES (24, N'E. South America Standard Time', N'Östl. Südamerika Normalzeit', N'(UTC-03:00) Brasilia', N'Östl. Südamerika Sommerzeit', 1, -10800)
INSERT INTO [DateTimeUtil].[Timezone] ([Id], [Identifier], [StandardName], [DisplayName], [DaylightName], [SupportsDaylightSavingTime], [BaseUtcOffsetSec]) VALUES (25, N'Argentina Standard Time', N'Argentinien Normalzeit', N'(UTC-03:00) Buenos Aires', N'Argentinien Sommerzeit', 1, -10800)
INSERT INTO [DateTimeUtil].[Timezone] ([Id], [Identifier], [StandardName], [DisplayName], [DaylightName], [SupportsDaylightSavingTime], [BaseUtcOffsetSec]) VALUES (26, N'SA Eastern Standard Time', N'Östl. Südamerika Normalzeit  ', N'(UTC-03:00) Cayenne, Fortaleza', N'Östl. Südamerika Sommerzeit ', 0, -10800)
INSERT INTO [DateTimeUtil].[Timezone] ([Id], [Identifier], [StandardName], [DisplayName], [DaylightName], [SupportsDaylightSavingTime], [BaseUtcOffsetSec]) VALUES (27, N'Greenland Standard Time', N'Grönland Normalzeit', N'(UTC-03:00) Grönland', N'Grönland Sommerzeit', 1, -10800)
INSERT INTO [DateTimeUtil].[Timezone] ([Id], [Identifier], [StandardName], [DisplayName], [DaylightName], [SupportsDaylightSavingTime], [BaseUtcOffsetSec]) VALUES (28, N'Montevideo Standard Time', N'Montevideo Normalzeit', N'(UTC-03:00) Montevideo', N'Montevideo Sommerzeit', 1, -10800)
INSERT INTO [DateTimeUtil].[Timezone] ([Id], [Identifier], [StandardName], [DisplayName], [DaylightName], [SupportsDaylightSavingTime], [BaseUtcOffsetSec]) VALUES (29, N'Bahia Standard Time', N'Bahia Normalzeit', N'(UTC-03:00) Salvador', N'Bahia Sommerzeit', 1, -10800)
INSERT INTO [DateTimeUtil].[Timezone] ([Id], [Identifier], [StandardName], [DisplayName], [DaylightName], [SupportsDaylightSavingTime], [BaseUtcOffsetSec]) VALUES (30, N'UTC-02', N'UTC-02', N'(UTC-02:00) Koordinierte Weltzeit-02', N'UTC-02', 0, -7200)
INSERT INTO [DateTimeUtil].[Timezone] ([Id], [Identifier], [StandardName], [DisplayName], [DaylightName], [SupportsDaylightSavingTime], [BaseUtcOffsetSec]) VALUES (31, N'Mid-Atlantic Standard Time', N'Mittelatlantik Normalzeit', N'(UTC-02:00) Mittelatlantik - Alt', N'Mittelatlantik Sommerzeit', 1, -7200)
INSERT INTO [DateTimeUtil].[Timezone] ([Id], [Identifier], [StandardName], [DisplayName], [DaylightName], [SupportsDaylightSavingTime], [BaseUtcOffsetSec]) VALUES (32, N'Azores Standard Time', N'Azoren Normalzeit', N'(UTC-01:00) Azoren', N'Azoren Sommerzeit', 1, -3600)
INSERT INTO [DateTimeUtil].[Timezone] ([Id], [Identifier], [StandardName], [DisplayName], [DaylightName], [SupportsDaylightSavingTime], [BaseUtcOffsetSec]) VALUES (33, N'Cape Verde Standard Time', N'Kap Verde Normalzeit', N'(UTC-01:00) Kap Verde', N'Kap Verde Sommerzeit', 0, -3600)
INSERT INTO [DateTimeUtil].[Timezone] ([Id], [Identifier], [StandardName], [DisplayName], [DaylightName], [SupportsDaylightSavingTime], [BaseUtcOffsetSec]) VALUES (34, N'Morocco Standard Time', N'Marokko Normalzeit', N'(UTC) Casablanca', N'Marokko Sommerzeit', 1, 0)
INSERT INTO [DateTimeUtil].[Timezone] ([Id], [Identifier], [StandardName], [DisplayName], [DaylightName], [SupportsDaylightSavingTime], [BaseUtcOffsetSec]) VALUES (35, N'GMT Standard Time', N'Westeuropäische Zeit', N'(UTC) Dublin, Edinburgh, Lissabon, London', N'Westeuropäische Sommerzeit', 1, 0)
INSERT INTO [DateTimeUtil].[Timezone] ([Id], [Identifier], [StandardName], [DisplayName], [DaylightName], [SupportsDaylightSavingTime], [BaseUtcOffsetSec]) VALUES (36, N'UTC', N'Koordinierte Weltzeit', N'(UTC) Koordinierte Weltzeit', N'Koordinierte Weltzeit ', 0, 0)
INSERT INTO [DateTimeUtil].[Timezone] ([Id], [Identifier], [StandardName], [DisplayName], [DaylightName], [SupportsDaylightSavingTime], [BaseUtcOffsetSec]) VALUES (37, N'Greenwich Standard Time', N'Westafrikanische Normalzeit', N'(UTC) Monrovia, Reykjavik', N'Westafrikanische Sommerzeit', 0, 0)
INSERT INTO [DateTimeUtil].[Timezone] ([Id], [Identifier], [StandardName], [DisplayName], [DaylightName], [SupportsDaylightSavingTime], [BaseUtcOffsetSec]) VALUES (38, N'W. Europe Standard Time', N'Mitteleuropäische Zeit', N'(UTC+01:00) Amsterdam, Berlin, Bern, Rom, Stockholm, Wien', N'Mitteleuropäische Sommerzeit', 1, 3600)
INSERT INTO [DateTimeUtil].[Timezone] ([Id], [Identifier], [StandardName], [DisplayName], [DaylightName], [SupportsDaylightSavingTime], [BaseUtcOffsetSec]) VALUES (39, N'Central Europe Standard Time', N'Mitteleuropäische Zeit ', N'(UTC+01:00) Belgrad, Bratislava (Pressburg), Budapest, Ljubljana, Prag', N'Mitteleuropäische Sommerzeit ', 1, 3600)
INSERT INTO [DateTimeUtil].[Timezone] ([Id], [Identifier], [StandardName], [DisplayName], [DaylightName], [SupportsDaylightSavingTime], [BaseUtcOffsetSec]) VALUES (40, N'Romance Standard Time', N'Mitteleuropäische Zeit    ', N'(UTC+01:00) Brüssel, Kopenhagen, Madrid, Paris', N'Mitteleuropäische Sommerzeit   ', 1, 3600)
INSERT INTO [DateTimeUtil].[Timezone] ([Id], [Identifier], [StandardName], [DisplayName], [DaylightName], [SupportsDaylightSavingTime], [BaseUtcOffsetSec]) VALUES (41, N'Central European Standard Time', N'Mitteleuropäische Zeit  ', N'(UTC+01:00) Sarajevo, Skopje, Warschau, Zagreb', N'Mitteleuropäische Sommerzeit  ', 1, 3600)
INSERT INTO [DateTimeUtil].[Timezone] ([Id], [Identifier], [StandardName], [DisplayName], [DaylightName], [SupportsDaylightSavingTime], [BaseUtcOffsetSec]) VALUES (42, N'W. Central Africa Standard Time', N'West-Zentralafrika Normalzeit', N'(UTC+01:00) West-Zentralafrika', N'West-Zentralafrika Sommerzeit', 0, 3600)
INSERT INTO [DateTimeUtil].[Timezone] ([Id], [Identifier], [StandardName], [DisplayName], [DaylightName], [SupportsDaylightSavingTime], [BaseUtcOffsetSec]) VALUES (43, N'Namibia Standard Time', N'Namibia Normalzeit', N'(UTC+01:00) Windhuk', N'Namibia Sommerzeit', 1, 3600)
INSERT INTO [DateTimeUtil].[Timezone] ([Id], [Identifier], [StandardName], [DisplayName], [DaylightName], [SupportsDaylightSavingTime], [BaseUtcOffsetSec]) VALUES (44, N'Jordan Standard Time', N'Jordanien Normalzeit', N'(UTC+02:00) Amman', N'Jordanien Sommerzeit', 1, 7200)
INSERT INTO [DateTimeUtil].[Timezone] ([Id], [Identifier], [StandardName], [DisplayName], [DaylightName], [SupportsDaylightSavingTime], [BaseUtcOffsetSec]) VALUES (45, N'GTB Standard Time', N'Osteuropäische Zeit', N'(UTC+02:00) Athen, Bukarest', N'Osteuropäische Sommerzeit', 1, 7200)
INSERT INTO [DateTimeUtil].[Timezone] ([Id], [Identifier], [StandardName], [DisplayName], [DaylightName], [SupportsDaylightSavingTime], [BaseUtcOffsetSec]) VALUES (46, N'Middle East Standard Time', N'Mittlerer Osten Normalzeit', N'(UTC+02:00) Beirut', N'Mittlerer Osten Sommerzeit', 1, 7200)
INSERT INTO [DateTimeUtil].[Timezone] ([Id], [Identifier], [StandardName], [DisplayName], [DaylightName], [SupportsDaylightSavingTime], [BaseUtcOffsetSec]) VALUES (47, N'Syria Standard Time', N'Syrien Normalzeit', N'(UTC+02:00) Damaskus', N'Syrien Sommerzeit', 1, 7200)
INSERT INTO [DateTimeUtil].[Timezone] ([Id], [Identifier], [StandardName], [DisplayName], [DaylightName], [SupportsDaylightSavingTime], [BaseUtcOffsetSec]) VALUES (48, N'South Africa Standard Time', N'Südafrika Normalzeit', N'(UTC+02:00) Harare, Pretoria', N'Südafrika Sommerzeit', 0, 7200)
INSERT INTO [DateTimeUtil].[Timezone] ([Id], [Identifier], [StandardName], [DisplayName], [DaylightName], [SupportsDaylightSavingTime], [BaseUtcOffsetSec]) VALUES (49, N'FLE Standard Time', N'Osteuropäische Zeit  ', N'(UTC+02:00) Helsinki, Kiew, Riga, Sofia, Tallinn, Wilna', N'Osteuropäische Sommerzeit  ', 1, 7200)
INSERT INTO [DateTimeUtil].[Timezone] ([Id], [Identifier], [StandardName], [DisplayName], [DaylightName], [SupportsDaylightSavingTime], [BaseUtcOffsetSec]) VALUES (50, N'Turkey Standard Time', N'Türkei Normalzeit', N'(UTC+02:00) Istanbul', N'Türkei Sommerzeit', 1, 7200)
INSERT INTO [DateTimeUtil].[Timezone] ([Id], [Identifier], [StandardName], [DisplayName], [DaylightName], [SupportsDaylightSavingTime], [BaseUtcOffsetSec]) VALUES (51, N'Israel Standard Time', N'Jerusalem Normalzeit', N'(UTC+02:00) Jerusalem', N'Jerusalem Sommerzeit', 1, 7200)
INSERT INTO [DateTimeUtil].[Timezone] ([Id], [Identifier], [StandardName], [DisplayName], [DaylightName], [SupportsDaylightSavingTime], [BaseUtcOffsetSec]) VALUES (52, N'Egypt Standard Time', N'Ägypten Normalzeit', N'(UTC+02:00) Kairo', N'Ägypten Sommerzeit', 1, 7200)
INSERT INTO [DateTimeUtil].[Timezone] ([Id], [Identifier], [StandardName], [DisplayName], [DaylightName], [SupportsDaylightSavingTime], [BaseUtcOffsetSec]) VALUES (53, N'E. Europe Standard Time', N'Osteuropäische Zeit ', N'(UTC+02:00) Osteuropa', N'Osteuropäische Sommerzeit ', 1, 7200)
INSERT INTO [DateTimeUtil].[Timezone] ([Id], [Identifier], [StandardName], [DisplayName], [DaylightName], [SupportsDaylightSavingTime], [BaseUtcOffsetSec]) VALUES (54, N'Libya Standard Time', N'Libyen Normalzeit', N'(UTC+02:00) Tripolis', N'Libyen Sommerzeit', 1, 7200)
INSERT INTO [DateTimeUtil].[Timezone] ([Id], [Identifier], [StandardName], [DisplayName], [DaylightName], [SupportsDaylightSavingTime], [BaseUtcOffsetSec]) VALUES (55, N'Arabic Standard Time', N'Arabische Normalzeit ', N'(UTC+03:00) Bagdad', N'Arabische Sommerzeit ', 1, 10800)
INSERT INTO [DateTimeUtil].[Timezone] ([Id], [Identifier], [StandardName], [DisplayName], [DaylightName], [SupportsDaylightSavingTime], [BaseUtcOffsetSec]) VALUES (56, N'Kaliningrad Standard Time', N'Kaliningrad Normalzeit', N'(UTC+03:00) Kaliningrad, Minsk', N'Kaliningrad Sommerzeit', 1, 10800)
INSERT INTO [DateTimeUtil].[Timezone] ([Id], [Identifier], [StandardName], [DisplayName], [DaylightName], [SupportsDaylightSavingTime], [BaseUtcOffsetSec]) VALUES (57, N'Arab Standard Time', N'Arabische Normalzeit', N'(UTC+03:00) Kuwait, Riad', N'Arabische Sommerzeit', 0, 10800)
INSERT INTO [DateTimeUtil].[Timezone] ([Id], [Identifier], [StandardName], [DisplayName], [DaylightName], [SupportsDaylightSavingTime], [BaseUtcOffsetSec]) VALUES (58, N'E. Africa Standard Time', N'Ostafrikanische Normalzeit', N'(UTC+03:00) Nairobi', N'Ostafrikanische Sommerzeit', 0, 10800)
INSERT INTO [DateTimeUtil].[Timezone] ([Id], [Identifier], [StandardName], [DisplayName], [DaylightName], [SupportsDaylightSavingTime], [BaseUtcOffsetSec]) VALUES (59, N'Iran Standard Time', N'Iran Normalzeit', N'(UTC+03:30) Teheran', N'Iran Sommerzeit', 1, 12600)
INSERT INTO [DateTimeUtil].[Timezone] ([Id], [Identifier], [StandardName], [DisplayName], [DaylightName], [SupportsDaylightSavingTime], [BaseUtcOffsetSec]) VALUES (60, N'Arabian Standard Time', N'Arabische Normalzeit  ', N'(UTC+04:00) Abu Dhabi, Muskat', N'Arabische Sommerzeit  ', 0, 14400)
INSERT INTO [DateTimeUtil].[Timezone] ([Id], [Identifier], [StandardName], [DisplayName], [DaylightName], [SupportsDaylightSavingTime], [BaseUtcOffsetSec]) VALUES (61, N'Azerbaijan Standard Time', N'Aserbaidschan Normalzeit', N'(UTC+04:00) Baku', N'Aserbaidschan Sommerzeit', 1, 14400)
INSERT INTO [DateTimeUtil].[Timezone] ([Id], [Identifier], [StandardName], [DisplayName], [DaylightName], [SupportsDaylightSavingTime], [BaseUtcOffsetSec]) VALUES (62, N'Caucasus Standard Time', N'Kaukasische Normalzeit', N'(UTC+04:00) Eriwan', N'Kaukasische Sommerzeit', 1, 14400)
INSERT INTO [DateTimeUtil].[Timezone] ([Id], [Identifier], [StandardName], [DisplayName], [DaylightName], [SupportsDaylightSavingTime], [BaseUtcOffsetSec]) VALUES (63, N'Russian Standard Time', N'Russische Normalzeit', N'(UTC+04:00) Moskau, St. Petersburg, Wolgograd', N'Russische Sommerzeit', 1, 14400)
INSERT INTO [DateTimeUtil].[Timezone] ([Id], [Identifier], [StandardName], [DisplayName], [DaylightName], [SupportsDaylightSavingTime], [BaseUtcOffsetSec]) VALUES (64, N'Mauritius Standard Time', N'Mauritius Normalzeit', N'(UTC+04:00) Port Louis', N'Mauritius Sommerzeit', 1, 14400)
INSERT INTO [DateTimeUtil].[Timezone] ([Id], [Identifier], [StandardName], [DisplayName], [DaylightName], [SupportsDaylightSavingTime], [BaseUtcOffsetSec]) VALUES (65, N'Georgian Standard Time', N'Georgische Normalzeit', N'(UTC+04:00) Tiflis', N'Georgische Sommerzeit', 0, 14400)
INSERT INTO [DateTimeUtil].[Timezone] ([Id], [Identifier], [StandardName], [DisplayName], [DaylightName], [SupportsDaylightSavingTime], [BaseUtcOffsetSec]) VALUES (66, N'Afghanistan Standard Time', N'Afghanistan Normalzeit', N'(UTC+04:30) Kabul', N'Afghanistan Sommerzeit', 0, 16200)
INSERT INTO [DateTimeUtil].[Timezone] ([Id], [Identifier], [StandardName], [DisplayName], [DaylightName], [SupportsDaylightSavingTime], [BaseUtcOffsetSec]) VALUES (67, N'West Asia Standard Time', N'West Asien Normalzeit', N'(UTC+05:00) Aschgabat, Taschkent', N'West Asien Sommerzeit', 0, 18000)
INSERT INTO [DateTimeUtil].[Timezone] ([Id], [Identifier], [StandardName], [DisplayName], [DaylightName], [SupportsDaylightSavingTime], [BaseUtcOffsetSec]) VALUES (68, N'Pakistan Standard Time', N'Pakistan Normalzeit', N'(UTC+05:00) Islamabad, Karatschi', N'Pakistan Sommerzeit', 1, 18000)
INSERT INTO [DateTimeUtil].[Timezone] ([Id], [Identifier], [StandardName], [DisplayName], [DaylightName], [SupportsDaylightSavingTime], [BaseUtcOffsetSec]) VALUES (69, N'India Standard Time', N'Indien Normalzeit', N'(UTC+05:30) Chennai, Kolkata, Mumbai, New Delhi', N'Indien Sommerzeit', 0, 19800)
INSERT INTO [DateTimeUtil].[Timezone] ([Id], [Identifier], [StandardName], [DisplayName], [DaylightName], [SupportsDaylightSavingTime], [BaseUtcOffsetSec]) VALUES (70, N'Sri Lanka Standard Time', N'Sri Lanka Normalzeit', N'(UTC+05:30) Sri Jayawardenepura', N'Sri Lanka Sommerzeit', 0, 19800)
INSERT INTO [DateTimeUtil].[Timezone] ([Id], [Identifier], [StandardName], [DisplayName], [DaylightName], [SupportsDaylightSavingTime], [BaseUtcOffsetSec]) VALUES (71, N'Nepal Standard Time', N'Nepal Normalzeit', N'(UTC+05:45) Katmandu', N'Nepal Sommerzeit', 0, 20700)
INSERT INTO [DateTimeUtil].[Timezone] ([Id], [Identifier], [StandardName], [DisplayName], [DaylightName], [SupportsDaylightSavingTime], [BaseUtcOffsetSec]) VALUES (72, N'Central Asia Standard Time', N'Zentralasien Normalzeit', N'(UTC+06:00) Astana', N'Zentralasien Sommerzeit', 0, 21600)
INSERT INTO [DateTimeUtil].[Timezone] ([Id], [Identifier], [StandardName], [DisplayName], [DaylightName], [SupportsDaylightSavingTime], [BaseUtcOffsetSec]) VALUES (73, N'Bangladesh Standard Time', N'Bangladesch Normalzeit', N'(UTC+06:00) Dakka', N'Bangladesch Sommerzeit', 1, 21600)
INSERT INTO [DateTimeUtil].[Timezone] ([Id], [Identifier], [StandardName], [DisplayName], [DaylightName], [SupportsDaylightSavingTime], [BaseUtcOffsetSec]) VALUES (74, N'Ekaterinburg Standard Time', N'Jekaterinburg Normalzeit', N'(UTC+06:00) Jekaterinburg', N'Jekaterinburg Sommerzeit', 1, 21600)
INSERT INTO [DateTimeUtil].[Timezone] ([Id], [Identifier], [StandardName], [DisplayName], [DaylightName], [SupportsDaylightSavingTime], [BaseUtcOffsetSec]) VALUES (75, N'Myanmar Standard Time', N'Myanmar Normalzeit', N'(UTC+06:30) Yangon (Rangun)', N'Myanmar Sommerzeit', 0, 23400)
INSERT INTO [DateTimeUtil].[Timezone] ([Id], [Identifier], [StandardName], [DisplayName], [DaylightName], [SupportsDaylightSavingTime], [BaseUtcOffsetSec]) VALUES (76, N'SE Asia Standard Time', N'Südostasiatische Normalzeit', N'(UTC+07:00) Bangkok, Hanoi, Jakarta', N'Südostasiatische Sommerzeit', 0, 25200)
INSERT INTO [DateTimeUtil].[Timezone] ([Id], [Identifier], [StandardName], [DisplayName], [DaylightName], [SupportsDaylightSavingTime], [BaseUtcOffsetSec]) VALUES (77, N'N. Central Asia Standard Time', N'Nord-Zentralasien Normalzeit', N'(UTC+07:00) Nowosibirsk', N'Nord-Zentralasien Sommerzeit', 1, 25200)
INSERT INTO [DateTimeUtil].[Timezone] ([Id], [Identifier], [StandardName], [DisplayName], [DaylightName], [SupportsDaylightSavingTime], [BaseUtcOffsetSec]) VALUES (78, N'North Asia Standard Time', N'Nordasien Normalzeit', N'(UTC+08:00) Krasnojarsk', N'Nordasien Sommerzeit', 1, 28800)
INSERT INTO [DateTimeUtil].[Timezone] ([Id], [Identifier], [StandardName], [DisplayName], [DaylightName], [SupportsDaylightSavingTime], [BaseUtcOffsetSec]) VALUES (79, N'Singapore Standard Time', N'Malaiische Halbinsel Normalzeit', N'(UTC+08:00) Kuala Lumpur, Singapur', N'Malaiische Halbinsel Sommerzeit', 0, 28800)
INSERT INTO [DateTimeUtil].[Timezone] ([Id], [Identifier], [StandardName], [DisplayName], [DaylightName], [SupportsDaylightSavingTime], [BaseUtcOffsetSec]) VALUES (80, N'China Standard Time', N'China Normalzeit', N'(UTC+08:00) Peking, Chongqing, Hongkong (SAR), Urumchi', N'China Sommerzeit', 0, 28800)
INSERT INTO [DateTimeUtil].[Timezone] ([Id], [Identifier], [StandardName], [DisplayName], [DaylightName], [SupportsDaylightSavingTime], [BaseUtcOffsetSec]) VALUES (81, N'W. Australia Standard Time', N'Westaustralische Normalzeit', N'(UTC+08:00) Perth', N'Westaustralische Sommerzeit', 1, 28800)
INSERT INTO [DateTimeUtil].[Timezone] ([Id], [Identifier], [StandardName], [DisplayName], [DaylightName], [SupportsDaylightSavingTime], [BaseUtcOffsetSec]) VALUES (82, N'Taipei Standard Time', N'Taipeh Normalzeit', N'(UTC+08:00) Taipeh', N'Taipeh Sommerzeit', 0, 28800)
INSERT INTO [DateTimeUtil].[Timezone] ([Id], [Identifier], [StandardName], [DisplayName], [DaylightName], [SupportsDaylightSavingTime], [BaseUtcOffsetSec]) VALUES (83, N'Ulaanbaatar Standard Time', N'Ulan-Bator Normalzeit', N'(UTC+08:00) Ulan-Bator', N'Ulan-Bator Sommerzeit', 0, 28800)
INSERT INTO [DateTimeUtil].[Timezone] ([Id], [Identifier], [StandardName], [DisplayName], [DaylightName], [SupportsDaylightSavingTime], [BaseUtcOffsetSec]) VALUES (84, N'North Asia East Standard Time', N'Ost-Nordasiatische Normalzeit', N'(UTC+09:00) Irkutsk', N'Ost-Nordasiatische Sommerzeit', 1, 32400)
INSERT INTO [DateTimeUtil].[Timezone] ([Id], [Identifier], [StandardName], [DisplayName], [DaylightName], [SupportsDaylightSavingTime], [BaseUtcOffsetSec]) VALUES (85, N'Tokyo Standard Time', N'Japanische Normalzeit', N'(UTC+09:00) Osaka, Sapporo, Tokio', N'Japanische Sommerzeit', 0, 32400)
INSERT INTO [DateTimeUtil].[Timezone] ([Id], [Identifier], [StandardName], [DisplayName], [DaylightName], [SupportsDaylightSavingTime], [BaseUtcOffsetSec]) VALUES (86, N'Korea Standard Time', N'Koreanische Normalzeit', N'(UTC+09:00) Seoul', N'Koreanische Sommerzeit', 0, 32400)
INSERT INTO [DateTimeUtil].[Timezone] ([Id], [Identifier], [StandardName], [DisplayName], [DaylightName], [SupportsDaylightSavingTime], [BaseUtcOffsetSec]) VALUES (87, N'Cen. Australia Standard Time', N'Zentralaustralische Normalzeit ', N'(UTC+09:30) Adelaide', N'Zentralaustralische Sommerzeit ', 1, 34200)
INSERT INTO [DateTimeUtil].[Timezone] ([Id], [Identifier], [StandardName], [DisplayName], [DaylightName], [SupportsDaylightSavingTime], [BaseUtcOffsetSec]) VALUES (88, N'AUS Central Standard Time', N'Zentralaustralische Normalzeit', N'(UTC+09:30) Darwin', N'Zentralaustralische Sommerzeit', 0, 34200)
INSERT INTO [DateTimeUtil].[Timezone] ([Id], [Identifier], [StandardName], [DisplayName], [DaylightName], [SupportsDaylightSavingTime], [BaseUtcOffsetSec]) VALUES (89, N'E. Australia Standard Time', N'Ostaustralische Normalzeit ', N'(UTC+10:00) Brisbane', N'Ostaustralische Sommerzeit ', 0, 36000)
INSERT INTO [DateTimeUtil].[Timezone] ([Id], [Identifier], [StandardName], [DisplayName], [DaylightName], [SupportsDaylightSavingTime], [BaseUtcOffsetSec]) VALUES (90, N'AUS Eastern Standard Time', N'Ostaustralische Normalzeit', N'(UTC+10:00) Canberra, Melbourne, Sydney', N'Ostaustralische Sommerzeit', 1, 36000)
INSERT INTO [DateTimeUtil].[Timezone] ([Id], [Identifier], [StandardName], [DisplayName], [DaylightName], [SupportsDaylightSavingTime], [BaseUtcOffsetSec]) VALUES (91, N'West Pacific Standard Time', N'Westpazifische Normalzeit', N'(UTC+10:00) Guam, Port Moresby', N'Westpazifische Sommerzeit', 0, 36000)
INSERT INTO [DateTimeUtil].[Timezone] ([Id], [Identifier], [StandardName], [DisplayName], [DaylightName], [SupportsDaylightSavingTime], [BaseUtcOffsetSec]) VALUES (92, N'Tasmania Standard Time', N'Tasmanien Normalzeit', N'(UTC+10:00) Hobart', N'Tasmanien Sommerzeit', 1, 36000)
INSERT INTO [DateTimeUtil].[Timezone] ([Id], [Identifier], [StandardName], [DisplayName], [DaylightName], [SupportsDaylightSavingTime], [BaseUtcOffsetSec]) VALUES (93, N'Yakutsk Standard Time', N'Jakutsk Normalzeit', N'(UTC+10:00) Jakutsk', N'Jakutsk Sommerzeit', 1, 36000)
INSERT INTO [DateTimeUtil].[Timezone] ([Id], [Identifier], [StandardName], [DisplayName], [DaylightName], [SupportsDaylightSavingTime], [BaseUtcOffsetSec]) VALUES (94, N'Central Pacific Standard Time', N'Zentralpazifische Normalzeit', N'(UTC+11:00) Salomonen, Neu-Kaledonien', N'Zentralpazifische Sommerzeit', 0, 39600)
INSERT INTO [DateTimeUtil].[Timezone] ([Id], [Identifier], [StandardName], [DisplayName], [DaylightName], [SupportsDaylightSavingTime], [BaseUtcOffsetSec]) VALUES (95, N'Vladivostok Standard Time', N'Wladiwostok Normalzeit', N'(UTC+11:00) Wladiwostok', N'Wladiwostok Sommerzeit', 1, 39600)
INSERT INTO [DateTimeUtil].[Timezone] ([Id], [Identifier], [StandardName], [DisplayName], [DaylightName], [SupportsDaylightSavingTime], [BaseUtcOffsetSec]) VALUES (96, N'New Zealand Standard Time', N'Neuseeland Normalzeit', N'(UTC+12:00) Auckland, Wellington', N'Neuseeland Sommerzeit', 1, 43200)
INSERT INTO [DateTimeUtil].[Timezone] ([Id], [Identifier], [StandardName], [DisplayName], [DaylightName], [SupportsDaylightSavingTime], [BaseUtcOffsetSec]) VALUES (97, N'Fiji Standard Time', N'Fidschi Normalzeit', N'(UTC+12:00) Fidschi', N'Fidschi Sommerzeit', 1, 43200)
INSERT INTO [DateTimeUtil].[Timezone] ([Id], [Identifier], [StandardName], [DisplayName], [DaylightName], [SupportsDaylightSavingTime], [BaseUtcOffsetSec]) VALUES (98, N'UTC+12', N'UTC+12', N'(UTC+12:00) Koordinierte Weltzeit+12', N'UTC+12', 0, 43200)
INSERT INTO [DateTimeUtil].[Timezone] ([Id], [Identifier], [StandardName], [DisplayName], [DaylightName], [SupportsDaylightSavingTime], [BaseUtcOffsetSec]) VALUES (99, N'Magadan Standard Time', N'Magadan Normalzeit', N'(UTC+12:00) Magadan', N'Magadan Sommerzeit', 1, 43200)
INSERT INTO [DateTimeUtil].[Timezone] ([Id], [Identifier], [StandardName], [DisplayName], [DaylightName], [SupportsDaylightSavingTime], [BaseUtcOffsetSec]) VALUES (100, N'Kamchatka Standard Time', N'Kamtschatka Normalzeit', N'(UTC+12:00) Petropawlowsk-Kamtschatski - veraltet', N'Kamtschatka Sommerzeit', 1, 43200)
INSERT INTO [DateTimeUtil].[Timezone] ([Id], [Identifier], [StandardName], [DisplayName], [DaylightName], [SupportsDaylightSavingTime], [BaseUtcOffsetSec]) VALUES (101, N'Tonga Standard Time', N'Tonga Normalzeit', N'(UTC+13:00) Nuku''alofa', N'Tonga Sommerzeit', 0, 46800)
INSERT INTO [DateTimeUtil].[Timezone] ([Id], [Identifier], [StandardName], [DisplayName], [DaylightName], [SupportsDaylightSavingTime], [BaseUtcOffsetSec]) VALUES (102, N'Samoa Standard Time', N'Samoa Normalzeit', N'(UTC+13:00) Samoa', N'Samoa Sommerzeit', 1, 46800)
GO

CREATE TABLE [DateTimeUtil].[TimezoneAdjustmentRule](
	[Id] [int] NOT NULL,
	[TimezoneId] [int] NULL,
	[RuleNo] [int] NULL,
	[DateStart] [datetime2](7) NULL,
	[DateEnd] [datetime2](7) NULL,
	[DaylightTransitionStartIsFixedDateRule] [bit] NULL,
	[DaylightTransitionStartMonth] [int] NULL,
	[DaylightTransitionStartDay] [int] NULL,
	[DaylightTransitionStartWeek] [int] NULL,
	[DaylightTransitionStartDayOfWeek] [int] NULL,
	[DaylightTransitionStartTimeOfDay] [time](7) NULL,
	[DaylightTransitionEndIsFixedDateRule] [bit] NULL,
	[DaylightTransitionEndMonth] [int] NULL,
	[DaylightTransitionEndDay] [int] NULL,
	[DaylightTransitionEndWeek] [int] NULL,
	[DaylightTransitionEndDayOfWeek] [int] NULL,
	[DaylightTransitionEndTimeOfDay] [time](7) NULL,
	[DaylightDeltaSec] [int] NULL,
 CONSTRAINT [PK_TimezoneAdjustmentRule] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)
)
GO

CREATE UNIQUE NONCLUSTERED INDEX [UX_Timezone_Identifier] ON [DateTimeUtil].[Timezone]
(
	[Identifier] ASC
)
GO

CREATE UNIQUE NONCLUSTERED INDEX [UX_TimezoneAdjustmentRule_TimezoneId_DateStart_DateEnd] ON [DateTimeUtil].[TimezoneAdjustmentRule]
(
	[TimezoneId] ASC,
	[DateStart] ASC,
	[DateEnd] ASC
)
GO

ALTER TABLE [DateTimeUtil].[TimezoneAdjustmentRule]  WITH CHECK ADD  CONSTRAINT [FK_TimezoneAdjustmentRule_Timezone] FOREIGN KEY([TimezoneId])
REFERENCES [DateTimeUtil].[Timezone] ([Id])
GO

ALTER TABLE [DateTimeUtil].[TimezoneAdjustmentRule] CHECK CONSTRAINT [FK_TimezoneAdjustmentRule_Timezone]
GO

INSERT INTO [DateTimeUtil].[TimezoneAdjustmentRule] ([Id], [TimezoneId], [RuleNo], [DateStart], [DateEnd], [DaylightTransitionStartIsFixedDateRule], [DaylightTransitionStartMonth], [DaylightTransitionStartDay], [DaylightTransitionStartWeek], [DaylightTransitionStartDayOfWeek], [DaylightTransitionStartTimeOfDay], [DaylightTransitionEndIsFixedDateRule], [DaylightTransitionEndMonth], [DaylightTransitionEndDay], [DaylightTransitionEndWeek], [DaylightTransitionEndDayOfWeek], [DaylightTransitionEndTimeOfDay], [DaylightDeltaSec]) VALUES (1, 4, 1, N'0001-01-01 00:00:00', N'2006-12-31 00:00:00', 0, 4, 1, 1, 0, N'02:00:00', 0, 10, 1, 5, 0, N'02:00:00', 3600)
INSERT INTO [DateTimeUtil].[TimezoneAdjustmentRule] ([Id], [TimezoneId], [RuleNo], [DateStart], [DateEnd], [DaylightTransitionStartIsFixedDateRule], [DaylightTransitionStartMonth], [DaylightTransitionStartDay], [DaylightTransitionStartWeek], [DaylightTransitionStartDayOfWeek], [DaylightTransitionStartTimeOfDay], [DaylightTransitionEndIsFixedDateRule], [DaylightTransitionEndMonth], [DaylightTransitionEndDay], [DaylightTransitionEndWeek], [DaylightTransitionEndDayOfWeek], [DaylightTransitionEndTimeOfDay], [DaylightDeltaSec]) VALUES (2, 4, 2, N'2007-01-01 00:00:00', N'9999-12-31 00:00:00', 0, 3, 1, 2, 0, N'02:00:00', 0, 11, 1, 1, 0, N'02:00:00', 3600)
INSERT INTO [DateTimeUtil].[TimezoneAdjustmentRule] ([Id], [TimezoneId], [RuleNo], [DateStart], [DateEnd], [DaylightTransitionStartIsFixedDateRule], [DaylightTransitionStartMonth], [DaylightTransitionStartDay], [DaylightTransitionStartWeek], [DaylightTransitionStartDayOfWeek], [DaylightTransitionStartTimeOfDay], [DaylightTransitionEndIsFixedDateRule], [DaylightTransitionEndMonth], [DaylightTransitionEndDay], [DaylightTransitionEndWeek], [DaylightTransitionEndDayOfWeek], [DaylightTransitionEndTimeOfDay], [DaylightDeltaSec]) VALUES (3, 5, 1, N'0001-01-01 00:00:00', N'9999-12-31 00:00:00', 0, 4, 1, 1, 0, N'02:00:00', 0, 10, 1, 5, 0, N'02:00:00', 3600)
INSERT INTO [DateTimeUtil].[TimezoneAdjustmentRule] ([Id], [TimezoneId], [RuleNo], [DateStart], [DateEnd], [DaylightTransitionStartIsFixedDateRule], [DaylightTransitionStartMonth], [DaylightTransitionStartDay], [DaylightTransitionStartWeek], [DaylightTransitionStartDayOfWeek], [DaylightTransitionStartTimeOfDay], [DaylightTransitionEndIsFixedDateRule], [DaylightTransitionEndMonth], [DaylightTransitionEndDay], [DaylightTransitionEndWeek], [DaylightTransitionEndDayOfWeek], [DaylightTransitionEndTimeOfDay], [DaylightDeltaSec]) VALUES (4, 6, 1, N'0001-01-01 00:00:00', N'2006-12-31 00:00:00', 0, 4, 1, 1, 0, N'02:00:00', 0, 10, 1, 5, 0, N'02:00:00', 3600)
INSERT INTO [DateTimeUtil].[TimezoneAdjustmentRule] ([Id], [TimezoneId], [RuleNo], [DateStart], [DateEnd], [DaylightTransitionStartIsFixedDateRule], [DaylightTransitionStartMonth], [DaylightTransitionStartDay], [DaylightTransitionStartWeek], [DaylightTransitionStartDayOfWeek], [DaylightTransitionStartTimeOfDay], [DaylightTransitionEndIsFixedDateRule], [DaylightTransitionEndMonth], [DaylightTransitionEndDay], [DaylightTransitionEndWeek], [DaylightTransitionEndDayOfWeek], [DaylightTransitionEndTimeOfDay], [DaylightDeltaSec]) VALUES (5, 6, 2, N'2007-01-01 00:00:00', N'9999-12-31 00:00:00', 0, 3, 1, 2, 0, N'02:00:00', 0, 11, 1, 1, 0, N'02:00:00', 3600)
INSERT INTO [DateTimeUtil].[TimezoneAdjustmentRule] ([Id], [TimezoneId], [RuleNo], [DateStart], [DateEnd], [DaylightTransitionStartIsFixedDateRule], [DaylightTransitionStartMonth], [DaylightTransitionStartDay], [DaylightTransitionStartWeek], [DaylightTransitionStartDayOfWeek], [DaylightTransitionStartTimeOfDay], [DaylightTransitionEndIsFixedDateRule], [DaylightTransitionEndMonth], [DaylightTransitionEndDay], [DaylightTransitionEndWeek], [DaylightTransitionEndDayOfWeek], [DaylightTransitionEndTimeOfDay], [DaylightDeltaSec]) VALUES (6, 8, 1, N'0001-01-01 00:00:00', N'9999-12-31 00:00:00', 0, 4, 1, 1, 0, N'02:00:00', 0, 10, 1, 5, 0, N'02:00:00', 3600)
INSERT INTO [DateTimeUtil].[TimezoneAdjustmentRule] ([Id], [TimezoneId], [RuleNo], [DateStart], [DateEnd], [DaylightTransitionStartIsFixedDateRule], [DaylightTransitionStartMonth], [DaylightTransitionStartDay], [DaylightTransitionStartWeek], [DaylightTransitionStartDayOfWeek], [DaylightTransitionStartTimeOfDay], [DaylightTransitionEndIsFixedDateRule], [DaylightTransitionEndMonth], [DaylightTransitionEndDay], [DaylightTransitionEndWeek], [DaylightTransitionEndDayOfWeek], [DaylightTransitionEndTimeOfDay], [DaylightDeltaSec]) VALUES (7, 9, 1, N'0001-01-01 00:00:00', N'2006-12-31 00:00:00', 0, 4, 1, 1, 0, N'02:00:00', 0, 10, 1, 5, 0, N'02:00:00', 3600)
INSERT INTO [DateTimeUtil].[TimezoneAdjustmentRule] ([Id], [TimezoneId], [RuleNo], [DateStart], [DateEnd], [DaylightTransitionStartIsFixedDateRule], [DaylightTransitionStartMonth], [DaylightTransitionStartDay], [DaylightTransitionStartWeek], [DaylightTransitionStartDayOfWeek], [DaylightTransitionStartTimeOfDay], [DaylightTransitionEndIsFixedDateRule], [DaylightTransitionEndMonth], [DaylightTransitionEndDay], [DaylightTransitionEndWeek], [DaylightTransitionEndDayOfWeek], [DaylightTransitionEndTimeOfDay], [DaylightDeltaSec]) VALUES (8, 9, 2, N'2007-01-01 00:00:00', N'9999-12-31 00:00:00', 0, 3, 1, 2, 0, N'02:00:00', 0, 11, 1, 1, 0, N'02:00:00', 3600)
INSERT INTO [DateTimeUtil].[TimezoneAdjustmentRule] ([Id], [TimezoneId], [RuleNo], [DateStart], [DateEnd], [DaylightTransitionStartIsFixedDateRule], [DaylightTransitionStartMonth], [DaylightTransitionStartDay], [DaylightTransitionStartWeek], [DaylightTransitionStartDayOfWeek], [DaylightTransitionStartTimeOfDay], [DaylightTransitionEndIsFixedDateRule], [DaylightTransitionEndMonth], [DaylightTransitionEndDay], [DaylightTransitionEndWeek], [DaylightTransitionEndDayOfWeek], [DaylightTransitionEndTimeOfDay], [DaylightDeltaSec]) VALUES (9, 10, 1, N'0001-01-01 00:00:00', N'2006-12-31 00:00:00', 0, 4, 1, 1, 0, N'02:00:00', 0, 10, 1, 5, 0, N'02:00:00', 3600)
INSERT INTO [DateTimeUtil].[TimezoneAdjustmentRule] ([Id], [TimezoneId], [RuleNo], [DateStart], [DateEnd], [DaylightTransitionStartIsFixedDateRule], [DaylightTransitionStartMonth], [DaylightTransitionStartDay], [DaylightTransitionStartWeek], [DaylightTransitionStartDayOfWeek], [DaylightTransitionStartTimeOfDay], [DaylightTransitionEndIsFixedDateRule], [DaylightTransitionEndMonth], [DaylightTransitionEndDay], [DaylightTransitionEndWeek], [DaylightTransitionEndDayOfWeek], [DaylightTransitionEndTimeOfDay], [DaylightDeltaSec]) VALUES (10, 10, 2, N'2007-01-01 00:00:00', N'9999-12-31 00:00:00', 0, 3, 1, 2, 0, N'02:00:00', 0, 11, 1, 1, 0, N'02:00:00', 3600)
INSERT INTO [DateTimeUtil].[TimezoneAdjustmentRule] ([Id], [TimezoneId], [RuleNo], [DateStart], [DateEnd], [DaylightTransitionStartIsFixedDateRule], [DaylightTransitionStartMonth], [DaylightTransitionStartDay], [DaylightTransitionStartWeek], [DaylightTransitionStartDayOfWeek], [DaylightTransitionStartTimeOfDay], [DaylightTransitionEndIsFixedDateRule], [DaylightTransitionEndMonth], [DaylightTransitionEndDay], [DaylightTransitionEndWeek], [DaylightTransitionEndDayOfWeek], [DaylightTransitionEndTimeOfDay], [DaylightDeltaSec]) VALUES (11, 11, 1, N'0001-01-01 00:00:00', N'9999-12-31 00:00:00', 0, 4, 1, 1, 0, N'02:00:00', 0, 10, 1, 5, 0, N'02:00:00', 3600)
INSERT INTO [DateTimeUtil].[TimezoneAdjustmentRule] ([Id], [TimezoneId], [RuleNo], [DateStart], [DateEnd], [DaylightTransitionStartIsFixedDateRule], [DaylightTransitionStartMonth], [DaylightTransitionStartDay], [DaylightTransitionStartWeek], [DaylightTransitionStartDayOfWeek], [DaylightTransitionStartTimeOfDay], [DaylightTransitionEndIsFixedDateRule], [DaylightTransitionEndMonth], [DaylightTransitionEndDay], [DaylightTransitionEndWeek], [DaylightTransitionEndDayOfWeek], [DaylightTransitionEndTimeOfDay], [DaylightDeltaSec]) VALUES (12, 15, 1, N'0001-01-01 00:00:00', N'2006-12-31 00:00:00', 0, 4, 1, 1, 0, N'02:00:00', 0, 10, 1, 5, 0, N'02:00:00', 3600)
INSERT INTO [DateTimeUtil].[TimezoneAdjustmentRule] ([Id], [TimezoneId], [RuleNo], [DateStart], [DateEnd], [DaylightTransitionStartIsFixedDateRule], [DaylightTransitionStartMonth], [DaylightTransitionStartDay], [DaylightTransitionStartWeek], [DaylightTransitionStartDayOfWeek], [DaylightTransitionStartTimeOfDay], [DaylightTransitionEndIsFixedDateRule], [DaylightTransitionEndMonth], [DaylightTransitionEndDay], [DaylightTransitionEndWeek], [DaylightTransitionEndDayOfWeek], [DaylightTransitionEndTimeOfDay], [DaylightDeltaSec]) VALUES (13, 15, 2, N'2007-01-01 00:00:00', N'9999-12-31 00:00:00', 0, 3, 1, 2, 0, N'02:00:00', 0, 11, 1, 1, 0, N'02:00:00', 3600)
INSERT INTO [DateTimeUtil].[TimezoneAdjustmentRule] ([Id], [TimezoneId], [RuleNo], [DateStart], [DateEnd], [DaylightTransitionStartIsFixedDateRule], [DaylightTransitionStartMonth], [DaylightTransitionStartDay], [DaylightTransitionStartWeek], [DaylightTransitionStartDayOfWeek], [DaylightTransitionStartTimeOfDay], [DaylightTransitionEndIsFixedDateRule], [DaylightTransitionEndMonth], [DaylightTransitionEndDay], [DaylightTransitionEndWeek], [DaylightTransitionEndDayOfWeek], [DaylightTransitionEndTimeOfDay], [DaylightDeltaSec]) VALUES (14, 16, 1, N'2006-01-01 00:00:00', N'2006-12-31 00:00:00', 0, 4, 1, 1, 0, N'02:00:00', 0, 10, 1, 5, 0, N'02:00:00', 3600)
INSERT INTO [DateTimeUtil].[TimezoneAdjustmentRule] ([Id], [TimezoneId], [RuleNo], [DateStart], [DateEnd], [DaylightTransitionStartIsFixedDateRule], [DaylightTransitionStartMonth], [DaylightTransitionStartDay], [DaylightTransitionStartWeek], [DaylightTransitionStartDayOfWeek], [DaylightTransitionStartTimeOfDay], [DaylightTransitionEndIsFixedDateRule], [DaylightTransitionEndMonth], [DaylightTransitionEndDay], [DaylightTransitionEndWeek], [DaylightTransitionEndDayOfWeek], [DaylightTransitionEndTimeOfDay], [DaylightDeltaSec]) VALUES (15, 16, 2, N'2007-01-01 00:00:00', N'9999-12-31 00:00:00', 0, 3, 1, 2, 0, N'02:00:00', 0, 11, 1, 1, 0, N'02:00:00', 3600)
INSERT INTO [DateTimeUtil].[TimezoneAdjustmentRule] ([Id], [TimezoneId], [RuleNo], [DateStart], [DateEnd], [DaylightTransitionStartIsFixedDateRule], [DaylightTransitionStartMonth], [DaylightTransitionStartDay], [DaylightTransitionStartWeek], [DaylightTransitionStartDayOfWeek], [DaylightTransitionStartTimeOfDay], [DaylightTransitionEndIsFixedDateRule], [DaylightTransitionEndMonth], [DaylightTransitionEndDay], [DaylightTransitionEndWeek], [DaylightTransitionEndDayOfWeek], [DaylightTransitionEndTimeOfDay], [DaylightDeltaSec]) VALUES (16, 18, 1, N'0001-01-01 00:00:00', N'2008-12-31 00:00:00', 0, 10, 1, 3, 6, N'23:59:59.999', 0, 3, 1, 2, 6, N'23:59:59.999', 3600)
INSERT INTO [DateTimeUtil].[TimezoneAdjustmentRule] ([Id], [TimezoneId], [RuleNo], [DateStart], [DateEnd], [DaylightTransitionStartIsFixedDateRule], [DaylightTransitionStartMonth], [DaylightTransitionStartDay], [DaylightTransitionStartWeek], [DaylightTransitionStartDayOfWeek], [DaylightTransitionStartTimeOfDay], [DaylightTransitionEndIsFixedDateRule], [DaylightTransitionEndMonth], [DaylightTransitionEndDay], [DaylightTransitionEndWeek], [DaylightTransitionEndDayOfWeek], [DaylightTransitionEndTimeOfDay], [DaylightDeltaSec]) VALUES (17, 18, 2, N'2009-01-01 00:00:00', N'2009-12-31 00:00:00', 0, 10, 1, 3, 6, N'23:59:59.999', 0, 3, 1, 1, 6, N'23:59:59.999', 3600)
INSERT INTO [DateTimeUtil].[TimezoneAdjustmentRule] ([Id], [TimezoneId], [RuleNo], [DateStart], [DateEnd], [DaylightTransitionStartIsFixedDateRule], [DaylightTransitionStartMonth], [DaylightTransitionStartDay], [DaylightTransitionStartWeek], [DaylightTransitionStartDayOfWeek], [DaylightTransitionStartTimeOfDay], [DaylightTransitionEndIsFixedDateRule], [DaylightTransitionEndMonth], [DaylightTransitionEndDay], [DaylightTransitionEndWeek], [DaylightTransitionEndDayOfWeek], [DaylightTransitionEndTimeOfDay], [DaylightDeltaSec]) VALUES (18, 18, 3, N'2010-01-01 00:00:00', N'2010-12-31 00:00:00', 0, 10, 1, 1, 6, N'23:59:59.999', 0, 4, 1, 2, 6, N'23:59:59.999', 3600)
INSERT INTO [DateTimeUtil].[TimezoneAdjustmentRule] ([Id], [TimezoneId], [RuleNo], [DateStart], [DateEnd], [DaylightTransitionStartIsFixedDateRule], [DaylightTransitionStartMonth], [DaylightTransitionStartDay], [DaylightTransitionStartWeek], [DaylightTransitionStartDayOfWeek], [DaylightTransitionStartTimeOfDay], [DaylightTransitionEndIsFixedDateRule], [DaylightTransitionEndMonth], [DaylightTransitionEndDay], [DaylightTransitionEndWeek], [DaylightTransitionEndDayOfWeek], [DaylightTransitionEndTimeOfDay], [DaylightDeltaSec]) VALUES (19, 18, 4, N'2011-01-01 00:00:00', N'2011-12-31 00:00:00', 0, 10, 1, 1, 6, N'23:59:59.999', 0, 4, 1, 2, 6, N'23:59:59.999', 3600)
INSERT INTO [DateTimeUtil].[TimezoneAdjustmentRule] ([Id], [TimezoneId], [RuleNo], [DateStart], [DateEnd], [DaylightTransitionStartIsFixedDateRule], [DaylightTransitionStartMonth], [DaylightTransitionStartDay], [DaylightTransitionStartWeek], [DaylightTransitionStartDayOfWeek], [DaylightTransitionStartTimeOfDay], [DaylightTransitionEndIsFixedDateRule], [DaylightTransitionEndMonth], [DaylightTransitionEndDay], [DaylightTransitionEndWeek], [DaylightTransitionEndDayOfWeek], [DaylightTransitionEndTimeOfDay], [DaylightDeltaSec]) VALUES (20, 18, 5, N'2012-01-01 00:00:00', N'2012-12-31 00:00:00', 0, 10, 1, 1, 6, N'23:59:59.999', 0, 4, 1, 1, 6, N'23:59:59.999', 3600)
INSERT INTO [DateTimeUtil].[TimezoneAdjustmentRule] ([Id], [TimezoneId], [RuleNo], [DateStart], [DateEnd], [DaylightTransitionStartIsFixedDateRule], [DaylightTransitionStartMonth], [DaylightTransitionStartDay], [DaylightTransitionStartWeek], [DaylightTransitionStartDayOfWeek], [DaylightTransitionStartTimeOfDay], [DaylightTransitionEndIsFixedDateRule], [DaylightTransitionEndMonth], [DaylightTransitionEndDay], [DaylightTransitionEndWeek], [DaylightTransitionEndDayOfWeek], [DaylightTransitionEndTimeOfDay], [DaylightDeltaSec]) VALUES (21, 18, 6, N'2013-01-01 00:00:00', N'2013-12-31 00:00:00', 0, 10, 1, 1, 6, N'23:59:59.999', 0, 3, 1, 4, 6, N'23:59:59.999', 3600)
INSERT INTO [DateTimeUtil].[TimezoneAdjustmentRule] ([Id], [TimezoneId], [RuleNo], [DateStart], [DateEnd], [DaylightTransitionStartIsFixedDateRule], [DaylightTransitionStartMonth], [DaylightTransitionStartDay], [DaylightTransitionStartWeek], [DaylightTransitionStartDayOfWeek], [DaylightTransitionStartTimeOfDay], [DaylightTransitionEndIsFixedDateRule], [DaylightTransitionEndMonth], [DaylightTransitionEndDay], [DaylightTransitionEndWeek], [DaylightTransitionEndDayOfWeek], [DaylightTransitionEndTimeOfDay], [DaylightDeltaSec]) VALUES (22, 18, 7, N'2014-01-01 00:00:00', N'2014-12-31 00:00:00', 0, 10, 1, 1, 6, N'23:59:59.999', 0, 3, 1, 4, 6, N'23:59:59.999', 3600)
INSERT INTO [DateTimeUtil].[TimezoneAdjustmentRule] ([Id], [TimezoneId], [RuleNo], [DateStart], [DateEnd], [DaylightTransitionStartIsFixedDateRule], [DaylightTransitionStartMonth], [DaylightTransitionStartDay], [DaylightTransitionStartWeek], [DaylightTransitionStartDayOfWeek], [DaylightTransitionStartTimeOfDay], [DaylightTransitionEndIsFixedDateRule], [DaylightTransitionEndMonth], [DaylightTransitionEndDay], [DaylightTransitionEndWeek], [DaylightTransitionEndDayOfWeek], [DaylightTransitionEndTimeOfDay], [DaylightDeltaSec]) VALUES (23, 18, 8, N'2015-01-01 00:00:00', N'2015-12-31 00:00:00', 0, 10, 1, 1, 6, N'23:59:59.999', 0, 3, 1, 3, 6, N'23:59:59.999', 3600)
INSERT INTO [DateTimeUtil].[TimezoneAdjustmentRule] ([Id], [TimezoneId], [RuleNo], [DateStart], [DateEnd], [DaylightTransitionStartIsFixedDateRule], [DaylightTransitionStartMonth], [DaylightTransitionStartDay], [DaylightTransitionStartWeek], [DaylightTransitionStartDayOfWeek], [DaylightTransitionStartTimeOfDay], [DaylightTransitionEndIsFixedDateRule], [DaylightTransitionEndMonth], [DaylightTransitionEndDay], [DaylightTransitionEndWeek], [DaylightTransitionEndDayOfWeek], [DaylightTransitionEndTimeOfDay], [DaylightDeltaSec]) VALUES (24, 18, 9, N'2016-01-01 00:00:00', N'2016-12-31 00:00:00', 0, 10, 1, 1, 6, N'23:59:59.999', 0, 3, 1, 5, 6, N'23:59:59.999', 3600)
INSERT INTO [DateTimeUtil].[TimezoneAdjustmentRule] ([Id], [TimezoneId], [RuleNo], [DateStart], [DateEnd], [DaylightTransitionStartIsFixedDateRule], [DaylightTransitionStartMonth], [DaylightTransitionStartDay], [DaylightTransitionStartWeek], [DaylightTransitionStartDayOfWeek], [DaylightTransitionStartTimeOfDay], [DaylightTransitionEndIsFixedDateRule], [DaylightTransitionEndMonth], [DaylightTransitionEndDay], [DaylightTransitionEndWeek], [DaylightTransitionEndDayOfWeek], [DaylightTransitionEndTimeOfDay], [DaylightDeltaSec]) VALUES (25, 18, 10, N'2017-01-01 00:00:00', N'2017-12-31 00:00:00', 0, 9, 1, 5, 6, N'23:59:59.999', 0, 3, 1, 5, 6, N'23:59:59.999', 3600)
INSERT INTO [DateTimeUtil].[TimezoneAdjustmentRule] ([Id], [TimezoneId], [RuleNo], [DateStart], [DateEnd], [DaylightTransitionStartIsFixedDateRule], [DaylightTransitionStartMonth], [DaylightTransitionStartDay], [DaylightTransitionStartWeek], [DaylightTransitionStartDayOfWeek], [DaylightTransitionStartTimeOfDay], [DaylightTransitionEndIsFixedDateRule], [DaylightTransitionEndMonth], [DaylightTransitionEndDay], [DaylightTransitionEndWeek], [DaylightTransitionEndDayOfWeek], [DaylightTransitionEndTimeOfDay], [DaylightDeltaSec]) VALUES (26, 18, 11, N'2018-01-01 00:00:00', N'2018-12-31 00:00:00', 0, 10, 1, 1, 6, N'23:59:59.999', 0, 3, 1, 4, 6, N'23:59:59.999', 3600)
INSERT INTO [DateTimeUtil].[TimezoneAdjustmentRule] ([Id], [TimezoneId], [RuleNo], [DateStart], [DateEnd], [DaylightTransitionStartIsFixedDateRule], [DaylightTransitionStartMonth], [DaylightTransitionStartDay], [DaylightTransitionStartWeek], [DaylightTransitionStartDayOfWeek], [DaylightTransitionStartTimeOfDay], [DaylightTransitionEndIsFixedDateRule], [DaylightTransitionEndMonth], [DaylightTransitionEndDay], [DaylightTransitionEndWeek], [DaylightTransitionEndDayOfWeek], [DaylightTransitionEndTimeOfDay], [DaylightDeltaSec]) VALUES (27, 18, 12, N'2019-01-01 00:00:00', N'2019-12-31 00:00:00', 0, 10, 1, 1, 6, N'23:59:59.999', 0, 3, 1, 4, 6, N'23:59:59.999', 3600)
INSERT INTO [DateTimeUtil].[TimezoneAdjustmentRule] ([Id], [TimezoneId], [RuleNo], [DateStart], [DateEnd], [DaylightTransitionStartIsFixedDateRule], [DaylightTransitionStartMonth], [DaylightTransitionStartDay], [DaylightTransitionStartWeek], [DaylightTransitionStartDayOfWeek], [DaylightTransitionStartTimeOfDay], [DaylightTransitionEndIsFixedDateRule], [DaylightTransitionEndMonth], [DaylightTransitionEndDay], [DaylightTransitionEndWeek], [DaylightTransitionEndDayOfWeek], [DaylightTransitionEndTimeOfDay], [DaylightDeltaSec]) VALUES (28, 18, 13, N'2020-01-01 00:00:00', N'2020-12-31 00:00:00', 0, 10, 1, 1, 6, N'23:59:59.999', 0, 3, 1, 3, 6, N'23:59:59.999', 3600)
INSERT INTO [DateTimeUtil].[TimezoneAdjustmentRule] ([Id], [TimezoneId], [RuleNo], [DateStart], [DateEnd], [DaylightTransitionStartIsFixedDateRule], [DaylightTransitionStartMonth], [DaylightTransitionStartDay], [DaylightTransitionStartWeek], [DaylightTransitionStartDayOfWeek], [DaylightTransitionStartTimeOfDay], [DaylightTransitionEndIsFixedDateRule], [DaylightTransitionEndMonth], [DaylightTransitionEndDay], [DaylightTransitionEndWeek], [DaylightTransitionEndDayOfWeek], [DaylightTransitionEndTimeOfDay], [DaylightDeltaSec]) VALUES (29, 18, 14, N'2021-01-01 00:00:00', N'9999-12-31 00:00:00', 0, 10, 1, 1, 6, N'23:59:59.999', 0, 3, 1, 5, 6, N'23:59:59.999', 3600)
INSERT INTO [DateTimeUtil].[TimezoneAdjustmentRule] ([Id], [TimezoneId], [RuleNo], [DateStart], [DateEnd], [DaylightTransitionStartIsFixedDateRule], [DaylightTransitionStartMonth], [DaylightTransitionStartDay], [DaylightTransitionStartWeek], [DaylightTransitionStartDayOfWeek], [DaylightTransitionStartTimeOfDay], [DaylightTransitionEndIsFixedDateRule], [DaylightTransitionEndMonth], [DaylightTransitionEndDay], [DaylightTransitionEndWeek], [DaylightTransitionEndDayOfWeek], [DaylightTransitionEndTimeOfDay], [DaylightDeltaSec]) VALUES (30, 19, 1, N'0001-01-01 00:00:00', N'2006-12-31 00:00:00', 0, 4, 1, 1, 0, N'02:00:00', 0, 10, 1, 5, 0, N'02:00:00', 3600)
INSERT INTO [DateTimeUtil].[TimezoneAdjustmentRule] ([Id], [TimezoneId], [RuleNo], [DateStart], [DateEnd], [DaylightTransitionStartIsFixedDateRule], [DaylightTransitionStartMonth], [DaylightTransitionStartDay], [DaylightTransitionStartWeek], [DaylightTransitionStartDayOfWeek], [DaylightTransitionStartTimeOfDay], [DaylightTransitionEndIsFixedDateRule], [DaylightTransitionEndMonth], [DaylightTransitionEndDay], [DaylightTransitionEndWeek], [DaylightTransitionEndDayOfWeek], [DaylightTransitionEndTimeOfDay], [DaylightDeltaSec]) VALUES (31, 19, 2, N'2007-01-01 00:00:00', N'9999-12-31 00:00:00', 0, 3, 1, 2, 0, N'02:00:00', 0, 11, 1, 1, 0, N'02:00:00', 3600)
INSERT INTO [DateTimeUtil].[TimezoneAdjustmentRule] ([Id], [TimezoneId], [RuleNo], [DateStart], [DateEnd], [DaylightTransitionStartIsFixedDateRule], [DaylightTransitionStartMonth], [DaylightTransitionStartDay], [DaylightTransitionStartWeek], [DaylightTransitionStartDayOfWeek], [DaylightTransitionStartTimeOfDay], [DaylightTransitionEndIsFixedDateRule], [DaylightTransitionEndMonth], [DaylightTransitionEndDay], [DaylightTransitionEndWeek], [DaylightTransitionEndDayOfWeek], [DaylightTransitionEndTimeOfDay], [DaylightDeltaSec]) VALUES (32, 20, 1, N'0001-01-01 00:00:00', N'2006-12-31 00:00:00', 0, 11, 1, 1, 0, N'00:00:00', 0, 2, 1, 2, 0, N'02:00:00', 3600)
INSERT INTO [DateTimeUtil].[TimezoneAdjustmentRule] ([Id], [TimezoneId], [RuleNo], [DateStart], [DateEnd], [DaylightTransitionStartIsFixedDateRule], [DaylightTransitionStartMonth], [DaylightTransitionStartDay], [DaylightTransitionStartWeek], [DaylightTransitionStartDayOfWeek], [DaylightTransitionStartTimeOfDay], [DaylightTransitionEndIsFixedDateRule], [DaylightTransitionEndMonth], [DaylightTransitionEndDay], [DaylightTransitionEndWeek], [DaylightTransitionEndDayOfWeek], [DaylightTransitionEndTimeOfDay], [DaylightDeltaSec]) VALUES (33, 20, 2, N'2007-01-01 00:00:00', N'2007-12-31 00:00:00', 0, 10, 1, 2, 0, N'00:00:00', 0, 2, 1, 5, 0, N'00:00:00', 3600)
INSERT INTO [DateTimeUtil].[TimezoneAdjustmentRule] ([Id], [TimezoneId], [RuleNo], [DateStart], [DateEnd], [DaylightTransitionStartIsFixedDateRule], [DaylightTransitionStartMonth], [DaylightTransitionStartDay], [DaylightTransitionStartWeek], [DaylightTransitionStartDayOfWeek], [DaylightTransitionStartTimeOfDay], [DaylightTransitionEndIsFixedDateRule], [DaylightTransitionEndMonth], [DaylightTransitionEndDay], [DaylightTransitionEndWeek], [DaylightTransitionEndDayOfWeek], [DaylightTransitionEndTimeOfDay], [DaylightDeltaSec]) VALUES (34, 20, 3, N'2008-01-01 00:00:00', N'2008-12-31 00:00:00', 0, 10, 1, 3, 6, N'23:59:59.999', 0, 2, 1, 3, 0, N'00:00:00', 3600)
INSERT INTO [DateTimeUtil].[TimezoneAdjustmentRule] ([Id], [TimezoneId], [RuleNo], [DateStart], [DateEnd], [DaylightTransitionStartIsFixedDateRule], [DaylightTransitionStartMonth], [DaylightTransitionStartDay], [DaylightTransitionStartWeek], [DaylightTransitionStartDayOfWeek], [DaylightTransitionStartTimeOfDay], [DaylightTransitionEndIsFixedDateRule], [DaylightTransitionEndMonth], [DaylightTransitionEndDay], [DaylightTransitionEndWeek], [DaylightTransitionEndDayOfWeek], [DaylightTransitionEndTimeOfDay], [DaylightDeltaSec]) VALUES (35, 20, 4, N'2009-01-01 00:00:00', N'2009-12-31 00:00:00', 0, 10, 1, 3, 6, N'23:59:59.999', 0, 2, 1, 2, 6, N'23:59:59.999', 3600)
INSERT INTO [DateTimeUtil].[TimezoneAdjustmentRule] ([Id], [TimezoneId], [RuleNo], [DateStart], [DateEnd], [DaylightTransitionStartIsFixedDateRule], [DaylightTransitionStartMonth], [DaylightTransitionStartDay], [DaylightTransitionStartWeek], [DaylightTransitionStartDayOfWeek], [DaylightTransitionStartTimeOfDay], [DaylightTransitionEndIsFixedDateRule], [DaylightTransitionEndMonth], [DaylightTransitionEndDay], [DaylightTransitionEndWeek], [DaylightTransitionEndDayOfWeek], [DaylightTransitionEndTimeOfDay], [DaylightDeltaSec]) VALUES (36, 20, 5, N'2010-01-01 00:00:00', N'2010-12-31 00:00:00', 0, 10, 1, 3, 6, N'23:59:59.999', 0, 2, 1, 3, 6, N'23:59:59.999', 3600)
INSERT INTO [DateTimeUtil].[TimezoneAdjustmentRule] ([Id], [TimezoneId], [RuleNo], [DateStart], [DateEnd], [DaylightTransitionStartIsFixedDateRule], [DaylightTransitionStartMonth], [DaylightTransitionStartDay], [DaylightTransitionStartWeek], [DaylightTransitionStartDayOfWeek], [DaylightTransitionStartTimeOfDay], [DaylightTransitionEndIsFixedDateRule], [DaylightTransitionEndMonth], [DaylightTransitionEndDay], [DaylightTransitionEndWeek], [DaylightTransitionEndDayOfWeek], [DaylightTransitionEndTimeOfDay], [DaylightDeltaSec]) VALUES (37, 20, 6, N'2011-01-01 00:00:00', N'2011-12-31 00:00:00', 0, 10, 1, 3, 6, N'23:59:59.999', 0, 2, 1, 3, 6, N'23:59:59.999', 3600)
INSERT INTO [DateTimeUtil].[TimezoneAdjustmentRule] ([Id], [TimezoneId], [RuleNo], [DateStart], [DateEnd], [DaylightTransitionStartIsFixedDateRule], [DaylightTransitionStartMonth], [DaylightTransitionStartDay], [DaylightTransitionStartWeek], [DaylightTransitionStartDayOfWeek], [DaylightTransitionStartTimeOfDay], [DaylightTransitionEndIsFixedDateRule], [DaylightTransitionEndMonth], [DaylightTransitionEndDay], [DaylightTransitionEndWeek], [DaylightTransitionEndDayOfWeek], [DaylightTransitionEndTimeOfDay], [DaylightDeltaSec]) VALUES (38, 20, 7, N'2012-01-01 00:00:00', N'2012-12-31 00:00:00', 0, 10, 1, 3, 6, N'23:59:59.999', 0, 2, 1, 4, 6, N'23:59:59.999', 3600)
INSERT INTO [DateTimeUtil].[TimezoneAdjustmentRule] ([Id], [TimezoneId], [RuleNo], [DateStart], [DateEnd], [DaylightTransitionStartIsFixedDateRule], [DaylightTransitionStartMonth], [DaylightTransitionStartDay], [DaylightTransitionStartWeek], [DaylightTransitionStartDayOfWeek], [DaylightTransitionStartTimeOfDay], [DaylightTransitionEndIsFixedDateRule], [DaylightTransitionEndMonth], [DaylightTransitionEndDay], [DaylightTransitionEndWeek], [DaylightTransitionEndDayOfWeek], [DaylightTransitionEndTimeOfDay], [DaylightDeltaSec]) VALUES (39, 20, 8, N'2013-01-01 00:00:00', N'2013-12-31 00:00:00', 0, 10, 1, 3, 6, N'23:59:59.999', 0, 2, 1, 3, 6, N'23:59:59.999', 3600)
INSERT INTO [DateTimeUtil].[TimezoneAdjustmentRule] ([Id], [TimezoneId], [RuleNo], [DateStart], [DateEnd], [DaylightTransitionStartIsFixedDateRule], [DaylightTransitionStartMonth], [DaylightTransitionStartDay], [DaylightTransitionStartWeek], [DaylightTransitionStartDayOfWeek], [DaylightTransitionStartTimeOfDay], [DaylightTransitionEndIsFixedDateRule], [DaylightTransitionEndMonth], [DaylightTransitionEndDay], [DaylightTransitionEndWeek], [DaylightTransitionEndDayOfWeek], [DaylightTransitionEndTimeOfDay], [DaylightDeltaSec]) VALUES (40, 20, 9, N'2014-01-01 00:00:00', N'2014-12-31 00:00:00', 0, 10, 1, 3, 6, N'23:59:59.999', 0, 2, 1, 3, 6, N'23:59:59.999', 3600)
INSERT INTO [DateTimeUtil].[TimezoneAdjustmentRule] ([Id], [TimezoneId], [RuleNo], [DateStart], [DateEnd], [DaylightTransitionStartIsFixedDateRule], [DaylightTransitionStartMonth], [DaylightTransitionStartDay], [DaylightTransitionStartWeek], [DaylightTransitionStartDayOfWeek], [DaylightTransitionStartTimeOfDay], [DaylightTransitionEndIsFixedDateRule], [DaylightTransitionEndMonth], [DaylightTransitionEndDay], [DaylightTransitionEndWeek], [DaylightTransitionEndDayOfWeek], [DaylightTransitionEndTimeOfDay], [DaylightDeltaSec]) VALUES (41, 20, 10, N'2015-01-01 00:00:00', N'2015-12-31 00:00:00', 0, 10, 1, 3, 6, N'23:59:59.999', 0, 2, 1, 3, 6, N'23:59:59.999', 3600)
INSERT INTO [DateTimeUtil].[TimezoneAdjustmentRule] ([Id], [TimezoneId], [RuleNo], [DateStart], [DateEnd], [DaylightTransitionStartIsFixedDateRule], [DaylightTransitionStartMonth], [DaylightTransitionStartDay], [DaylightTransitionStartWeek], [DaylightTransitionStartDayOfWeek], [DaylightTransitionStartTimeOfDay], [DaylightTransitionEndIsFixedDateRule], [DaylightTransitionEndMonth], [DaylightTransitionEndDay], [DaylightTransitionEndWeek], [DaylightTransitionEndDayOfWeek], [DaylightTransitionEndTimeOfDay], [DaylightDeltaSec]) VALUES (42, 20, 11, N'2016-01-01 00:00:00', N'2016-12-31 00:00:00', 0, 10, 1, 3, 6, N'23:59:59.999', 0, 2, 1, 3, 6, N'23:59:59.999', 3600)
INSERT INTO [DateTimeUtil].[TimezoneAdjustmentRule] ([Id], [TimezoneId], [RuleNo], [DateStart], [DateEnd], [DaylightTransitionStartIsFixedDateRule], [DaylightTransitionStartMonth], [DaylightTransitionStartDay], [DaylightTransitionStartWeek], [DaylightTransitionStartDayOfWeek], [DaylightTransitionStartTimeOfDay], [DaylightTransitionEndIsFixedDateRule], [DaylightTransitionEndMonth], [DaylightTransitionEndDay], [DaylightTransitionEndWeek], [DaylightTransitionEndDayOfWeek], [DaylightTransitionEndTimeOfDay], [DaylightDeltaSec]) VALUES (43, 20, 12, N'2017-01-01 00:00:00', N'2017-12-31 00:00:00', 0, 10, 1, 2, 6, N'23:59:59.999', 0, 2, 1, 3, 6, N'23:59:59.999', 3600)
INSERT INTO [DateTimeUtil].[TimezoneAdjustmentRule] ([Id], [TimezoneId], [RuleNo], [DateStart], [DateEnd], [DaylightTransitionStartIsFixedDateRule], [DaylightTransitionStartMonth], [DaylightTransitionStartDay], [DaylightTransitionStartWeek], [DaylightTransitionStartDayOfWeek], [DaylightTransitionStartTimeOfDay], [DaylightTransitionEndIsFixedDateRule], [DaylightTransitionEndMonth], [DaylightTransitionEndDay], [DaylightTransitionEndWeek], [DaylightTransitionEndDayOfWeek], [DaylightTransitionEndTimeOfDay], [DaylightDeltaSec]) VALUES (44, 20, 13, N'2018-01-01 00:00:00', N'2018-12-31 00:00:00', 0, 10, 1, 3, 6, N'23:59:59.999', 0, 2, 1, 3, 6, N'23:59:59.999', 3600)
INSERT INTO [DateTimeUtil].[TimezoneAdjustmentRule] ([Id], [TimezoneId], [RuleNo], [DateStart], [DateEnd], [DaylightTransitionStartIsFixedDateRule], [DaylightTransitionStartMonth], [DaylightTransitionStartDay], [DaylightTransitionStartWeek], [DaylightTransitionStartDayOfWeek], [DaylightTransitionStartTimeOfDay], [DaylightTransitionEndIsFixedDateRule], [DaylightTransitionEndMonth], [DaylightTransitionEndDay], [DaylightTransitionEndWeek], [DaylightTransitionEndDayOfWeek], [DaylightTransitionEndTimeOfDay], [DaylightDeltaSec]) VALUES (45, 20, 14, N'2019-01-01 00:00:00', N'2019-12-31 00:00:00', 0, 10, 1, 3, 6, N'23:59:59.999', 0, 2, 1, 3, 6, N'23:59:59.999', 3600)
INSERT INTO [DateTimeUtil].[TimezoneAdjustmentRule] ([Id], [TimezoneId], [RuleNo], [DateStart], [DateEnd], [DaylightTransitionStartIsFixedDateRule], [DaylightTransitionStartMonth], [DaylightTransitionStartDay], [DaylightTransitionStartWeek], [DaylightTransitionStartDayOfWeek], [DaylightTransitionStartTimeOfDay], [DaylightTransitionEndIsFixedDateRule], [DaylightTransitionEndMonth], [DaylightTransitionEndDay], [DaylightTransitionEndWeek], [DaylightTransitionEndDayOfWeek], [DaylightTransitionEndTimeOfDay], [DaylightDeltaSec]) VALUES (46, 20, 15, N'2020-01-01 00:00:00', N'2020-12-31 00:00:00', 0, 10, 1, 3, 6, N'23:59:59.999', 0, 2, 1, 3, 6, N'23:59:59.999', 3600)
INSERT INTO [DateTimeUtil].[TimezoneAdjustmentRule] ([Id], [TimezoneId], [RuleNo], [DateStart], [DateEnd], [DaylightTransitionStartIsFixedDateRule], [DaylightTransitionStartMonth], [DaylightTransitionStartDay], [DaylightTransitionStartWeek], [DaylightTransitionStartDayOfWeek], [DaylightTransitionStartTimeOfDay], [DaylightTransitionEndIsFixedDateRule], [DaylightTransitionEndMonth], [DaylightTransitionEndDay], [DaylightTransitionEndWeek], [DaylightTransitionEndDayOfWeek], [DaylightTransitionEndTimeOfDay], [DaylightDeltaSec]) VALUES (47, 20, 16, N'2021-01-01 00:00:00', N'2021-12-31 00:00:00', 0, 10, 1, 3, 6, N'23:59:59.999', 0, 2, 1, 3, 6, N'23:59:59.999', 3600)
INSERT INTO [DateTimeUtil].[TimezoneAdjustmentRule] ([Id], [TimezoneId], [RuleNo], [DateStart], [DateEnd], [DaylightTransitionStartIsFixedDateRule], [DaylightTransitionStartMonth], [DaylightTransitionStartDay], [DaylightTransitionStartWeek], [DaylightTransitionStartDayOfWeek], [DaylightTransitionStartTimeOfDay], [DaylightTransitionEndIsFixedDateRule], [DaylightTransitionEndMonth], [DaylightTransitionEndDay], [DaylightTransitionEndWeek], [DaylightTransitionEndDayOfWeek], [DaylightTransitionEndTimeOfDay], [DaylightDeltaSec]) VALUES (48, 20, 17, N'2022-01-01 00:00:00', N'2022-12-31 00:00:00', 0, 10, 1, 3, 6, N'23:59:59.999', 0, 2, 1, 3, 6, N'23:59:59.999', 3600)
INSERT INTO [DateTimeUtil].[TimezoneAdjustmentRule] ([Id], [TimezoneId], [RuleNo], [DateStart], [DateEnd], [DaylightTransitionStartIsFixedDateRule], [DaylightTransitionStartMonth], [DaylightTransitionStartDay], [DaylightTransitionStartWeek], [DaylightTransitionStartDayOfWeek], [DaylightTransitionStartTimeOfDay], [DaylightTransitionEndIsFixedDateRule], [DaylightTransitionEndMonth], [DaylightTransitionEndDay], [DaylightTransitionEndWeek], [DaylightTransitionEndDayOfWeek], [DaylightTransitionEndTimeOfDay], [DaylightDeltaSec]) VALUES (49, 20, 18, N'2023-01-01 00:00:00', N'2023-12-31 00:00:00', 0, 10, 1, 2, 6, N'23:59:59.999', 0, 2, 1, 4, 6, N'23:59:59.999', 3600)
INSERT INTO [DateTimeUtil].[TimezoneAdjustmentRule] ([Id], [TimezoneId], [RuleNo], [DateStart], [DateEnd], [DaylightTransitionStartIsFixedDateRule], [DaylightTransitionStartMonth], [DaylightTransitionStartDay], [DaylightTransitionStartWeek], [DaylightTransitionStartDayOfWeek], [DaylightTransitionStartTimeOfDay], [DaylightTransitionEndIsFixedDateRule], [DaylightTransitionEndMonth], [DaylightTransitionEndDay], [DaylightTransitionEndWeek], [DaylightTransitionEndDayOfWeek], [DaylightTransitionEndTimeOfDay], [DaylightDeltaSec]) VALUES (50, 20, 19, N'2024-01-01 00:00:00', N'2024-12-31 00:00:00', 0, 10, 1, 3, 6, N'23:59:59.999', 0, 2, 1, 3, 6, N'23:59:59.999', 3600)
INSERT INTO [DateTimeUtil].[TimezoneAdjustmentRule] ([Id], [TimezoneId], [RuleNo], [DateStart], [DateEnd], [DaylightTransitionStartIsFixedDateRule], [DaylightTransitionStartMonth], [DaylightTransitionStartDay], [DaylightTransitionStartWeek], [DaylightTransitionStartDayOfWeek], [DaylightTransitionStartTimeOfDay], [DaylightTransitionEndIsFixedDateRule], [DaylightTransitionEndMonth], [DaylightTransitionEndDay], [DaylightTransitionEndWeek], [DaylightTransitionEndDayOfWeek], [DaylightTransitionEndTimeOfDay], [DaylightDeltaSec]) VALUES (51, 20, 20, N'2025-01-01 00:00:00', N'2025-12-31 00:00:00', 0, 10, 1, 3, 6, N'23:59:59.999', 0, 2, 1, 3, 6, N'23:59:59.999', 3600)
INSERT INTO [DateTimeUtil].[TimezoneAdjustmentRule] ([Id], [TimezoneId], [RuleNo], [DateStart], [DateEnd], [DaylightTransitionStartIsFixedDateRule], [DaylightTransitionStartMonth], [DaylightTransitionStartDay], [DaylightTransitionStartWeek], [DaylightTransitionStartDayOfWeek], [DaylightTransitionStartTimeOfDay], [DaylightTransitionEndIsFixedDateRule], [DaylightTransitionEndMonth], [DaylightTransitionEndDay], [DaylightTransitionEndWeek], [DaylightTransitionEndDayOfWeek], [DaylightTransitionEndTimeOfDay], [DaylightDeltaSec]) VALUES (52, 20, 21, N'2026-01-01 00:00:00', N'2026-12-31 00:00:00', 0, 10, 1, 3, 6, N'23:59:59.999', 0, 2, 1, 3, 6, N'23:59:59.999', 3600)
INSERT INTO [DateTimeUtil].[TimezoneAdjustmentRule] ([Id], [TimezoneId], [RuleNo], [DateStart], [DateEnd], [DaylightTransitionStartIsFixedDateRule], [DaylightTransitionStartMonth], [DaylightTransitionStartDay], [DaylightTransitionStartWeek], [DaylightTransitionStartDayOfWeek], [DaylightTransitionStartTimeOfDay], [DaylightTransitionEndIsFixedDateRule], [DaylightTransitionEndMonth], [DaylightTransitionEndDay], [DaylightTransitionEndWeek], [DaylightTransitionEndDayOfWeek], [DaylightTransitionEndTimeOfDay], [DaylightDeltaSec]) VALUES (53, 20, 22, N'2027-01-01 00:00:00', N'2027-12-31 00:00:00', 0, 10, 1, 3, 6, N'23:59:59.999', 0, 2, 1, 3, 6, N'23:59:59.999', 3600)
INSERT INTO [DateTimeUtil].[TimezoneAdjustmentRule] ([Id], [TimezoneId], [RuleNo], [DateStart], [DateEnd], [DaylightTransitionStartIsFixedDateRule], [DaylightTransitionStartMonth], [DaylightTransitionStartDay], [DaylightTransitionStartWeek], [DaylightTransitionStartDayOfWeek], [DaylightTransitionStartTimeOfDay], [DaylightTransitionEndIsFixedDateRule], [DaylightTransitionEndMonth], [DaylightTransitionEndDay], [DaylightTransitionEndWeek], [DaylightTransitionEndDayOfWeek], [DaylightTransitionEndTimeOfDay], [DaylightDeltaSec]) VALUES (54, 20, 23, N'2028-01-01 00:00:00', N'2028-12-31 00:00:00', 0, 10, 1, 2, 6, N'23:59:59.999', 0, 2, 1, 3, 6, N'23:59:59.999', 3600)
INSERT INTO [DateTimeUtil].[TimezoneAdjustmentRule] ([Id], [TimezoneId], [RuleNo], [DateStart], [DateEnd], [DaylightTransitionStartIsFixedDateRule], [DaylightTransitionStartMonth], [DaylightTransitionStartDay], [DaylightTransitionStartWeek], [DaylightTransitionStartDayOfWeek], [DaylightTransitionStartTimeOfDay], [DaylightTransitionEndIsFixedDateRule], [DaylightTransitionEndMonth], [DaylightTransitionEndDay], [DaylightTransitionEndWeek], [DaylightTransitionEndDayOfWeek], [DaylightTransitionEndTimeOfDay], [DaylightDeltaSec]) VALUES (55, 20, 24, N'2029-01-01 00:00:00', N'2029-12-31 00:00:00', 0, 10, 1, 3, 6, N'23:59:59.999', 0, 2, 1, 3, 6, N'23:59:59.999', 3600)
INSERT INTO [DateTimeUtil].[TimezoneAdjustmentRule] ([Id], [TimezoneId], [RuleNo], [DateStart], [DateEnd], [DaylightTransitionStartIsFixedDateRule], [DaylightTransitionStartMonth], [DaylightTransitionStartDay], [DaylightTransitionStartWeek], [DaylightTransitionStartDayOfWeek], [DaylightTransitionStartTimeOfDay], [DaylightTransitionEndIsFixedDateRule], [DaylightTransitionEndMonth], [DaylightTransitionEndDay], [DaylightTransitionEndWeek], [DaylightTransitionEndDayOfWeek], [DaylightTransitionEndTimeOfDay], [DaylightDeltaSec]) VALUES (56, 20, 25, N'2030-01-01 00:00:00', N'2030-12-31 00:00:00', 0, 10, 1, 3, 6, N'23:59:59.999', 0, 2, 1, 3, 6, N'23:59:59.999', 3600)
INSERT INTO [DateTimeUtil].[TimezoneAdjustmentRule] ([Id], [TimezoneId], [RuleNo], [DateStart], [DateEnd], [DaylightTransitionStartIsFixedDateRule], [DaylightTransitionStartMonth], [DaylightTransitionStartDay], [DaylightTransitionStartWeek], [DaylightTransitionStartDayOfWeek], [DaylightTransitionStartTimeOfDay], [DaylightTransitionEndIsFixedDateRule], [DaylightTransitionEndMonth], [DaylightTransitionEndDay], [DaylightTransitionEndWeek], [DaylightTransitionEndDayOfWeek], [DaylightTransitionEndTimeOfDay], [DaylightDeltaSec]) VALUES (57, 20, 26, N'2031-01-01 00:00:00', N'2031-12-31 00:00:00', 0, 10, 1, 3, 6, N'23:59:59.999', 0, 2, 1, 3, 6, N'23:59:59.999', 3600)
INSERT INTO [DateTimeUtil].[TimezoneAdjustmentRule] ([Id], [TimezoneId], [RuleNo], [DateStart], [DateEnd], [DaylightTransitionStartIsFixedDateRule], [DaylightTransitionStartMonth], [DaylightTransitionStartDay], [DaylightTransitionStartWeek], [DaylightTransitionStartDayOfWeek], [DaylightTransitionStartTimeOfDay], [DaylightTransitionEndIsFixedDateRule], [DaylightTransitionEndMonth], [DaylightTransitionEndDay], [DaylightTransitionEndWeek], [DaylightTransitionEndDayOfWeek], [DaylightTransitionEndTimeOfDay], [DaylightDeltaSec]) VALUES (58, 20, 27, N'2032-01-01 00:00:00', N'2032-12-31 00:00:00', 0, 10, 1, 3, 6, N'23:59:59.999', 0, 2, 1, 2, 6, N'23:59:59.999', 3600)
INSERT INTO [DateTimeUtil].[TimezoneAdjustmentRule] ([Id], [TimezoneId], [RuleNo], [DateStart], [DateEnd], [DaylightTransitionStartIsFixedDateRule], [DaylightTransitionStartMonth], [DaylightTransitionStartDay], [DaylightTransitionStartWeek], [DaylightTransitionStartDayOfWeek], [DaylightTransitionStartTimeOfDay], [DaylightTransitionEndIsFixedDateRule], [DaylightTransitionEndMonth], [DaylightTransitionEndDay], [DaylightTransitionEndWeek], [DaylightTransitionEndDayOfWeek], [DaylightTransitionEndTimeOfDay], [DaylightDeltaSec]) VALUES (59, 20, 28, N'2033-01-01 00:00:00', N'2033-12-31 00:00:00', 0, 10, 1, 3, 6, N'23:59:59.999', 0, 2, 1, 3, 6, N'23:59:59.999', 3600)
INSERT INTO [DateTimeUtil].[TimezoneAdjustmentRule] ([Id], [TimezoneId], [RuleNo], [DateStart], [DateEnd], [DaylightTransitionStartIsFixedDateRule], [DaylightTransitionStartMonth], [DaylightTransitionStartDay], [DaylightTransitionStartWeek], [DaylightTransitionStartDayOfWeek], [DaylightTransitionStartTimeOfDay], [DaylightTransitionEndIsFixedDateRule], [DaylightTransitionEndMonth], [DaylightTransitionEndDay], [DaylightTransitionEndWeek], [DaylightTransitionEndDayOfWeek], [DaylightTransitionEndTimeOfDay], [DaylightDeltaSec]) VALUES (60, 20, 29, N'2034-01-01 00:00:00', N'2034-12-31 00:00:00', 0, 10, 1, 2, 6, N'23:59:59.999', 0, 2, 1, 4, 6, N'23:59:59.999', 3600)
INSERT INTO [DateTimeUtil].[TimezoneAdjustmentRule] ([Id], [TimezoneId], [RuleNo], [DateStart], [DateEnd], [DaylightTransitionStartIsFixedDateRule], [DaylightTransitionStartMonth], [DaylightTransitionStartDay], [DaylightTransitionStartWeek], [DaylightTransitionStartDayOfWeek], [DaylightTransitionStartTimeOfDay], [DaylightTransitionEndIsFixedDateRule], [DaylightTransitionEndMonth], [DaylightTransitionEndDay], [DaylightTransitionEndWeek], [DaylightTransitionEndDayOfWeek], [DaylightTransitionEndTimeOfDay], [DaylightDeltaSec]) VALUES (61, 20, 30, N'2035-01-01 00:00:00', N'2035-12-31 00:00:00', 0, 10, 1, 3, 6, N'23:59:59.999', 0, 2, 1, 3, 6, N'23:59:59.999', 3600)
INSERT INTO [DateTimeUtil].[TimezoneAdjustmentRule] ([Id], [TimezoneId], [RuleNo], [DateStart], [DateEnd], [DaylightTransitionStartIsFixedDateRule], [DaylightTransitionStartMonth], [DaylightTransitionStartDay], [DaylightTransitionStartWeek], [DaylightTransitionStartDayOfWeek], [DaylightTransitionStartTimeOfDay], [DaylightTransitionEndIsFixedDateRule], [DaylightTransitionEndMonth], [DaylightTransitionEndDay], [DaylightTransitionEndWeek], [DaylightTransitionEndDayOfWeek], [DaylightTransitionEndTimeOfDay], [DaylightDeltaSec]) VALUES (62, 20, 31, N'2036-01-01 00:00:00', N'2036-12-31 00:00:00', 0, 10, 1, 3, 6, N'23:59:59.999', 0, 2, 1, 3, 6, N'23:59:59.999', 3600)
INSERT INTO [DateTimeUtil].[TimezoneAdjustmentRule] ([Id], [TimezoneId], [RuleNo], [DateStart], [DateEnd], [DaylightTransitionStartIsFixedDateRule], [DaylightTransitionStartMonth], [DaylightTransitionStartDay], [DaylightTransitionStartWeek], [DaylightTransitionStartDayOfWeek], [DaylightTransitionStartTimeOfDay], [DaylightTransitionEndIsFixedDateRule], [DaylightTransitionEndMonth], [DaylightTransitionEndDay], [DaylightTransitionEndWeek], [DaylightTransitionEndDayOfWeek], [DaylightTransitionEndTimeOfDay], [DaylightDeltaSec]) VALUES (63, 20, 32, N'2037-01-01 00:00:00', N'2037-12-31 00:00:00', 0, 10, 1, 3, 6, N'23:59:59.999', 0, 2, 1, 3, 6, N'23:59:59.999', 3600)
INSERT INTO [DateTimeUtil].[TimezoneAdjustmentRule] ([Id], [TimezoneId], [RuleNo], [DateStart], [DateEnd], [DaylightTransitionStartIsFixedDateRule], [DaylightTransitionStartMonth], [DaylightTransitionStartDay], [DaylightTransitionStartWeek], [DaylightTransitionStartDayOfWeek], [DaylightTransitionStartTimeOfDay], [DaylightTransitionEndIsFixedDateRule], [DaylightTransitionEndMonth], [DaylightTransitionEndDay], [DaylightTransitionEndWeek], [DaylightTransitionEndDayOfWeek], [DaylightTransitionEndTimeOfDay], [DaylightDeltaSec]) VALUES (64, 20, 33, N'2038-01-01 00:00:00', N'2038-12-31 00:00:00', 0, 10, 1, 3, 6, N'23:59:59.999', 0, 2, 1, 3, 6, N'23:59:59.999', 3600)
INSERT INTO [DateTimeUtil].[TimezoneAdjustmentRule] ([Id], [TimezoneId], [RuleNo], [DateStart], [DateEnd], [DaylightTransitionStartIsFixedDateRule], [DaylightTransitionStartMonth], [DaylightTransitionStartDay], [DaylightTransitionStartWeek], [DaylightTransitionStartDayOfWeek], [DaylightTransitionStartTimeOfDay], [DaylightTransitionEndIsFixedDateRule], [DaylightTransitionEndMonth], [DaylightTransitionEndDay], [DaylightTransitionEndWeek], [DaylightTransitionEndDayOfWeek], [DaylightTransitionEndTimeOfDay], [DaylightDeltaSec]) VALUES (65, 20, 34, N'2039-01-01 00:00:00', N'2039-12-31 00:00:00', 0, 10, 1, 3, 6, N'23:59:59.999', 0, 2, 1, 4, 6, N'23:59:59.999', 3600)
INSERT INTO [DateTimeUtil].[TimezoneAdjustmentRule] ([Id], [TimezoneId], [RuleNo], [DateStart], [DateEnd], [DaylightTransitionStartIsFixedDateRule], [DaylightTransitionStartMonth], [DaylightTransitionStartDay], [DaylightTransitionStartWeek], [DaylightTransitionStartDayOfWeek], [DaylightTransitionStartTimeOfDay], [DaylightTransitionEndIsFixedDateRule], [DaylightTransitionEndMonth], [DaylightTransitionEndDay], [DaylightTransitionEndWeek], [DaylightTransitionEndDayOfWeek], [DaylightTransitionEndTimeOfDay], [DaylightDeltaSec]) VALUES (66, 20, 35, N'2040-01-01 00:00:00', N'9999-12-31 00:00:00', 0, 10, 1, 3, 6, N'23:59:59.999', 0, 2, 1, 3, 6, N'23:59:59.999', 3600)
INSERT INTO [DateTimeUtil].[TimezoneAdjustmentRule] ([Id], [TimezoneId], [RuleNo], [DateStart], [DateEnd], [DaylightTransitionStartIsFixedDateRule], [DaylightTransitionStartMonth], [DaylightTransitionStartDay], [DaylightTransitionStartWeek], [DaylightTransitionStartDayOfWeek], [DaylightTransitionStartTimeOfDay], [DaylightTransitionEndIsFixedDateRule], [DaylightTransitionEndMonth], [DaylightTransitionEndDay], [DaylightTransitionEndWeek], [DaylightTransitionEndDayOfWeek], [DaylightTransitionEndTimeOfDay], [DaylightDeltaSec]) VALUES (67, 22, 1, N'0001-01-01 00:00:00', N'2007-12-31 00:00:00', 0, 10, 1, 2, 6, N'23:59:59.999', 0, 3, 1, 2, 6, N'23:59:59.999', 3600)
INSERT INTO [DateTimeUtil].[TimezoneAdjustmentRule] ([Id], [TimezoneId], [RuleNo], [DateStart], [DateEnd], [DaylightTransitionStartIsFixedDateRule], [DaylightTransitionStartMonth], [DaylightTransitionStartDay], [DaylightTransitionStartWeek], [DaylightTransitionStartDayOfWeek], [DaylightTransitionStartTimeOfDay], [DaylightTransitionEndIsFixedDateRule], [DaylightTransitionEndMonth], [DaylightTransitionEndDay], [DaylightTransitionEndWeek], [DaylightTransitionEndDayOfWeek], [DaylightTransitionEndTimeOfDay], [DaylightDeltaSec]) VALUES (68, 22, 2, N'2008-01-01 00:00:00', N'2008-12-31 00:00:00', 0, 10, 1, 2, 6, N'23:59:59.999', 0, 3, 1, 5, 6, N'23:59:59.999', 3600)
INSERT INTO [DateTimeUtil].[TimezoneAdjustmentRule] ([Id], [TimezoneId], [RuleNo], [DateStart], [DateEnd], [DaylightTransitionStartIsFixedDateRule], [DaylightTransitionStartMonth], [DaylightTransitionStartDay], [DaylightTransitionStartWeek], [DaylightTransitionStartDayOfWeek], [DaylightTransitionStartTimeOfDay], [DaylightTransitionEndIsFixedDateRule], [DaylightTransitionEndMonth], [DaylightTransitionEndDay], [DaylightTransitionEndWeek], [DaylightTransitionEndDayOfWeek], [DaylightTransitionEndTimeOfDay], [DaylightDeltaSec]) VALUES (69, 22, 3, N'2009-01-01 00:00:00', N'2009-12-31 00:00:00', 0, 10, 1, 2, 6, N'23:59:59.999', 0, 3, 1, 2, 6, N'23:59:59.999', 3600)
INSERT INTO [DateTimeUtil].[TimezoneAdjustmentRule] ([Id], [TimezoneId], [RuleNo], [DateStart], [DateEnd], [DaylightTransitionStartIsFixedDateRule], [DaylightTransitionStartMonth], [DaylightTransitionStartDay], [DaylightTransitionStartWeek], [DaylightTransitionStartDayOfWeek], [DaylightTransitionStartTimeOfDay], [DaylightTransitionEndIsFixedDateRule], [DaylightTransitionEndMonth], [DaylightTransitionEndDay], [DaylightTransitionEndWeek], [DaylightTransitionEndDayOfWeek], [DaylightTransitionEndTimeOfDay], [DaylightDeltaSec]) VALUES (70, 22, 4, N'2010-01-01 00:00:00', N'2010-12-31 00:00:00', 0, 10, 1, 2, 6, N'23:59:59.999', 0, 4, 1, 1, 6, N'23:59:59.999', 3600)
INSERT INTO [DateTimeUtil].[TimezoneAdjustmentRule] ([Id], [TimezoneId], [RuleNo], [DateStart], [DateEnd], [DaylightTransitionStartIsFixedDateRule], [DaylightTransitionStartMonth], [DaylightTransitionStartDay], [DaylightTransitionStartWeek], [DaylightTransitionStartDayOfWeek], [DaylightTransitionStartTimeOfDay], [DaylightTransitionEndIsFixedDateRule], [DaylightTransitionEndMonth], [DaylightTransitionEndDay], [DaylightTransitionEndWeek], [DaylightTransitionEndDayOfWeek], [DaylightTransitionEndTimeOfDay], [DaylightDeltaSec]) VALUES (71, 22, 5, N'2011-01-01 00:00:00', N'2011-12-31 00:00:00', 0, 8, 1, 3, 6, N'23:59:59.999', 0, 5, 1, 1, 6, N'23:59:59.999', 3600)
INSERT INTO [DateTimeUtil].[TimezoneAdjustmentRule] ([Id], [TimezoneId], [RuleNo], [DateStart], [DateEnd], [DaylightTransitionStartIsFixedDateRule], [DaylightTransitionStartMonth], [DaylightTransitionStartDay], [DaylightTransitionStartWeek], [DaylightTransitionStartDayOfWeek], [DaylightTransitionStartTimeOfDay], [DaylightTransitionEndIsFixedDateRule], [DaylightTransitionEndMonth], [DaylightTransitionEndDay], [DaylightTransitionEndWeek], [DaylightTransitionEndDayOfWeek], [DaylightTransitionEndTimeOfDay], [DaylightDeltaSec]) VALUES (72, 22, 6, N'2012-01-01 00:00:00', N'2012-12-31 00:00:00', 0, 9, 1, 1, 6, N'23:59:59.999', 0, 4, 1, 5, 6, N'23:59:59.999', 3600)
INSERT INTO [DateTimeUtil].[TimezoneAdjustmentRule] ([Id], [TimezoneId], [RuleNo], [DateStart], [DateEnd], [DaylightTransitionStartIsFixedDateRule], [DaylightTransitionStartMonth], [DaylightTransitionStartDay], [DaylightTransitionStartWeek], [DaylightTransitionStartDayOfWeek], [DaylightTransitionStartTimeOfDay], [DaylightTransitionEndIsFixedDateRule], [DaylightTransitionEndMonth], [DaylightTransitionEndDay], [DaylightTransitionEndWeek], [DaylightTransitionEndDayOfWeek], [DaylightTransitionEndTimeOfDay], [DaylightDeltaSec]) VALUES (73, 22, 7, N'2013-01-01 00:00:00', N'2013-12-31 00:00:00', 0, 9, 1, 1, 6, N'23:59:59.999', 0, 4, 1, 5, 6, N'23:59:59.999', 3600)
INSERT INTO [DateTimeUtil].[TimezoneAdjustmentRule] ([Id], [TimezoneId], [RuleNo], [DateStart], [DateEnd], [DaylightTransitionStartIsFixedDateRule], [DaylightTransitionStartMonth], [DaylightTransitionStartDay], [DaylightTransitionStartWeek], [DaylightTransitionStartDayOfWeek], [DaylightTransitionStartTimeOfDay], [DaylightTransitionEndIsFixedDateRule], [DaylightTransitionEndMonth], [DaylightTransitionEndDay], [DaylightTransitionEndWeek], [DaylightTransitionEndDayOfWeek], [DaylightTransitionEndTimeOfDay], [DaylightDeltaSec]) VALUES (74, 22, 8, N'2014-01-01 00:00:00', N'9999-12-31 00:00:00', 0, 10, 1, 2, 6, N'23:59:59.999', 0, 3, 1, 2, 6, N'23:59:59.999', 3600)
INSERT INTO [DateTimeUtil].[TimezoneAdjustmentRule] ([Id], [TimezoneId], [RuleNo], [DateStart], [DateEnd], [DaylightTransitionStartIsFixedDateRule], [DaylightTransitionStartMonth], [DaylightTransitionStartDay], [DaylightTransitionStartWeek], [DaylightTransitionStartDayOfWeek], [DaylightTransitionStartTimeOfDay], [DaylightTransitionEndIsFixedDateRule], [DaylightTransitionEndMonth], [DaylightTransitionEndDay], [DaylightTransitionEndWeek], [DaylightTransitionEndDayOfWeek], [DaylightTransitionEndTimeOfDay], [DaylightDeltaSec]) VALUES (75, 23, 1, N'0001-01-01 00:00:00', N'2006-12-31 00:00:00', 0, 4, 1, 1, 0, N'00:01:00', 0, 10, 1, 5, 0, N'00:01:00', 3600)
INSERT INTO [DateTimeUtil].[TimezoneAdjustmentRule] ([Id], [TimezoneId], [RuleNo], [DateStart], [DateEnd], [DaylightTransitionStartIsFixedDateRule], [DaylightTransitionStartMonth], [DaylightTransitionStartDay], [DaylightTransitionStartWeek], [DaylightTransitionStartDayOfWeek], [DaylightTransitionStartTimeOfDay], [DaylightTransitionEndIsFixedDateRule], [DaylightTransitionEndMonth], [DaylightTransitionEndDay], [DaylightTransitionEndWeek], [DaylightTransitionEndDayOfWeek], [DaylightTransitionEndTimeOfDay], [DaylightDeltaSec]) VALUES (76, 23, 2, N'2007-01-01 00:00:00', N'2007-12-31 00:00:00', 0, 3, 1, 2, 0, N'00:01:00', 0, 11, 1, 1, 0, N'00:01:00', 3600)
INSERT INTO [DateTimeUtil].[TimezoneAdjustmentRule] ([Id], [TimezoneId], [RuleNo], [DateStart], [DateEnd], [DaylightTransitionStartIsFixedDateRule], [DaylightTransitionStartMonth], [DaylightTransitionStartDay], [DaylightTransitionStartWeek], [DaylightTransitionStartDayOfWeek], [DaylightTransitionStartTimeOfDay], [DaylightTransitionEndIsFixedDateRule], [DaylightTransitionEndMonth], [DaylightTransitionEndDay], [DaylightTransitionEndWeek], [DaylightTransitionEndDayOfWeek], [DaylightTransitionEndTimeOfDay], [DaylightDeltaSec]) VALUES (77, 23, 3, N'2008-01-01 00:00:00', N'2008-12-31 00:00:00', 0, 3, 1, 2, 0, N'00:01:00', 0, 11, 1, 1, 0, N'00:01:00', 3600)
INSERT INTO [DateTimeUtil].[TimezoneAdjustmentRule] ([Id], [TimezoneId], [RuleNo], [DateStart], [DateEnd], [DaylightTransitionStartIsFixedDateRule], [DaylightTransitionStartMonth], [DaylightTransitionStartDay], [DaylightTransitionStartWeek], [DaylightTransitionStartDayOfWeek], [DaylightTransitionStartTimeOfDay], [DaylightTransitionEndIsFixedDateRule], [DaylightTransitionEndMonth], [DaylightTransitionEndDay], [DaylightTransitionEndWeek], [DaylightTransitionEndDayOfWeek], [DaylightTransitionEndTimeOfDay], [DaylightDeltaSec]) VALUES (78, 23, 4, N'2009-01-01 00:00:00', N'2009-12-31 00:00:00', 0, 3, 1, 2, 0, N'00:01:00', 0, 11, 1, 1, 0, N'00:01:00', 3600)
INSERT INTO [DateTimeUtil].[TimezoneAdjustmentRule] ([Id], [TimezoneId], [RuleNo], [DateStart], [DateEnd], [DaylightTransitionStartIsFixedDateRule], [DaylightTransitionStartMonth], [DaylightTransitionStartDay], [DaylightTransitionStartWeek], [DaylightTransitionStartDayOfWeek], [DaylightTransitionStartTimeOfDay], [DaylightTransitionEndIsFixedDateRule], [DaylightTransitionEndMonth], [DaylightTransitionEndDay], [DaylightTransitionEndWeek], [DaylightTransitionEndDayOfWeek], [DaylightTransitionEndTimeOfDay], [DaylightDeltaSec]) VALUES (79, 23, 5, N'2010-01-01 00:00:00', N'2010-12-31 00:00:00', 0, 3, 1, 2, 0, N'00:01:00', 0, 11, 1, 1, 0, N'00:01:00', 3600)
INSERT INTO [DateTimeUtil].[TimezoneAdjustmentRule] ([Id], [TimezoneId], [RuleNo], [DateStart], [DateEnd], [DaylightTransitionStartIsFixedDateRule], [DaylightTransitionStartMonth], [DaylightTransitionStartDay], [DaylightTransitionStartWeek], [DaylightTransitionStartDayOfWeek], [DaylightTransitionStartTimeOfDay], [DaylightTransitionEndIsFixedDateRule], [DaylightTransitionEndMonth], [DaylightTransitionEndDay], [DaylightTransitionEndWeek], [DaylightTransitionEndDayOfWeek], [DaylightTransitionEndTimeOfDay], [DaylightDeltaSec]) VALUES (80, 23, 6, N'2011-01-01 00:00:00', N'2011-12-31 00:00:00', 0, 3, 1, 2, 0, N'00:01:00', 0, 11, 1, 1, 0, N'02:00:00', 3600)
INSERT INTO [DateTimeUtil].[TimezoneAdjustmentRule] ([Id], [TimezoneId], [RuleNo], [DateStart], [DateEnd], [DaylightTransitionStartIsFixedDateRule], [DaylightTransitionStartMonth], [DaylightTransitionStartDay], [DaylightTransitionStartWeek], [DaylightTransitionStartDayOfWeek], [DaylightTransitionStartTimeOfDay], [DaylightTransitionEndIsFixedDateRule], [DaylightTransitionEndMonth], [DaylightTransitionEndDay], [DaylightTransitionEndWeek], [DaylightTransitionEndDayOfWeek], [DaylightTransitionEndTimeOfDay], [DaylightDeltaSec]) VALUES (81, 23, 7, N'2012-01-01 00:00:00', N'9999-12-31 00:00:00', 0, 3, 1, 2, 0, N'02:00:00', 0, 11, 1, 1, 0, N'02:00:00', 3600)
INSERT INTO [DateTimeUtil].[TimezoneAdjustmentRule] ([Id], [TimezoneId], [RuleNo], [DateStart], [DateEnd], [DaylightTransitionStartIsFixedDateRule], [DaylightTransitionStartMonth], [DaylightTransitionStartDay], [DaylightTransitionStartWeek], [DaylightTransitionStartDayOfWeek], [DaylightTransitionStartTimeOfDay], [DaylightTransitionEndIsFixedDateRule], [DaylightTransitionEndMonth], [DaylightTransitionEndDay], [DaylightTransitionEndWeek], [DaylightTransitionEndDayOfWeek], [DaylightTransitionEndTimeOfDay], [DaylightDeltaSec]) VALUES (82, 24, 1, N'0001-01-01 00:00:00', N'2006-12-31 00:00:00', 0, 11, 1, 1, 0, N'00:00:00', 0, 2, 1, 2, 0, N'02:00:00', 3600)
INSERT INTO [DateTimeUtil].[TimezoneAdjustmentRule] ([Id], [TimezoneId], [RuleNo], [DateStart], [DateEnd], [DaylightTransitionStartIsFixedDateRule], [DaylightTransitionStartMonth], [DaylightTransitionStartDay], [DaylightTransitionStartWeek], [DaylightTransitionStartDayOfWeek], [DaylightTransitionStartTimeOfDay], [DaylightTransitionEndIsFixedDateRule], [DaylightTransitionEndMonth], [DaylightTransitionEndDay], [DaylightTransitionEndWeek], [DaylightTransitionEndDayOfWeek], [DaylightTransitionEndTimeOfDay], [DaylightDeltaSec]) VALUES (83, 24, 2, N'2007-01-01 00:00:00', N'2007-12-31 00:00:00', 0, 10, 1, 2, 0, N'00:00:00', 0, 2, 1, 5, 0, N'00:00:00', 3600)
INSERT INTO [DateTimeUtil].[TimezoneAdjustmentRule] ([Id], [TimezoneId], [RuleNo], [DateStart], [DateEnd], [DaylightTransitionStartIsFixedDateRule], [DaylightTransitionStartMonth], [DaylightTransitionStartDay], [DaylightTransitionStartWeek], [DaylightTransitionStartDayOfWeek], [DaylightTransitionStartTimeOfDay], [DaylightTransitionEndIsFixedDateRule], [DaylightTransitionEndMonth], [DaylightTransitionEndDay], [DaylightTransitionEndWeek], [DaylightTransitionEndDayOfWeek], [DaylightTransitionEndTimeOfDay], [DaylightDeltaSec]) VALUES (84, 24, 3, N'2008-01-01 00:00:00', N'2008-12-31 00:00:00', 0, 10, 1, 3, 6, N'23:59:59.999', 0, 2, 1, 3, 0, N'00:00:00', 3600)
INSERT INTO [DateTimeUtil].[TimezoneAdjustmentRule] ([Id], [TimezoneId], [RuleNo], [DateStart], [DateEnd], [DaylightTransitionStartIsFixedDateRule], [DaylightTransitionStartMonth], [DaylightTransitionStartDay], [DaylightTransitionStartWeek], [DaylightTransitionStartDayOfWeek], [DaylightTransitionStartTimeOfDay], [DaylightTransitionEndIsFixedDateRule], [DaylightTransitionEndMonth], [DaylightTransitionEndDay], [DaylightTransitionEndWeek], [DaylightTransitionEndDayOfWeek], [DaylightTransitionEndTimeOfDay], [DaylightDeltaSec]) VALUES (85, 24, 4, N'2009-01-01 00:00:00', N'2009-12-31 00:00:00', 0, 10, 1, 3, 6, N'23:59:59.999', 0, 2, 1, 2, 6, N'23:59:59.999', 3600)
INSERT INTO [DateTimeUtil].[TimezoneAdjustmentRule] ([Id], [TimezoneId], [RuleNo], [DateStart], [DateEnd], [DaylightTransitionStartIsFixedDateRule], [DaylightTransitionStartMonth], [DaylightTransitionStartDay], [DaylightTransitionStartWeek], [DaylightTransitionStartDayOfWeek], [DaylightTransitionStartTimeOfDay], [DaylightTransitionEndIsFixedDateRule], [DaylightTransitionEndMonth], [DaylightTransitionEndDay], [DaylightTransitionEndWeek], [DaylightTransitionEndDayOfWeek], [DaylightTransitionEndTimeOfDay], [DaylightDeltaSec]) VALUES (86, 24, 5, N'2010-01-01 00:00:00', N'2010-12-31 00:00:00', 0, 10, 1, 3, 6, N'23:59:59.999', 0, 2, 1, 3, 6, N'23:59:59.999', 3600)
INSERT INTO [DateTimeUtil].[TimezoneAdjustmentRule] ([Id], [TimezoneId], [RuleNo], [DateStart], [DateEnd], [DaylightTransitionStartIsFixedDateRule], [DaylightTransitionStartMonth], [DaylightTransitionStartDay], [DaylightTransitionStartWeek], [DaylightTransitionStartDayOfWeek], [DaylightTransitionStartTimeOfDay], [DaylightTransitionEndIsFixedDateRule], [DaylightTransitionEndMonth], [DaylightTransitionEndDay], [DaylightTransitionEndWeek], [DaylightTransitionEndDayOfWeek], [DaylightTransitionEndTimeOfDay], [DaylightDeltaSec]) VALUES (87, 24, 6, N'2011-01-01 00:00:00', N'2011-12-31 00:00:00', 0, 10, 1, 3, 6, N'23:59:59.999', 0, 2, 1, 3, 6, N'23:59:59.999', 3600)
INSERT INTO [DateTimeUtil].[TimezoneAdjustmentRule] ([Id], [TimezoneId], [RuleNo], [DateStart], [DateEnd], [DaylightTransitionStartIsFixedDateRule], [DaylightTransitionStartMonth], [DaylightTransitionStartDay], [DaylightTransitionStartWeek], [DaylightTransitionStartDayOfWeek], [DaylightTransitionStartTimeOfDay], [DaylightTransitionEndIsFixedDateRule], [DaylightTransitionEndMonth], [DaylightTransitionEndDay], [DaylightTransitionEndWeek], [DaylightTransitionEndDayOfWeek], [DaylightTransitionEndTimeOfDay], [DaylightDeltaSec]) VALUES (88, 24, 7, N'2012-01-01 00:00:00', N'2012-12-31 00:00:00', 0, 10, 1, 3, 6, N'23:59:59.999', 0, 2, 1, 4, 6, N'23:59:59.999', 3600)
INSERT INTO [DateTimeUtil].[TimezoneAdjustmentRule] ([Id], [TimezoneId], [RuleNo], [DateStart], [DateEnd], [DaylightTransitionStartIsFixedDateRule], [DaylightTransitionStartMonth], [DaylightTransitionStartDay], [DaylightTransitionStartWeek], [DaylightTransitionStartDayOfWeek], [DaylightTransitionStartTimeOfDay], [DaylightTransitionEndIsFixedDateRule], [DaylightTransitionEndMonth], [DaylightTransitionEndDay], [DaylightTransitionEndWeek], [DaylightTransitionEndDayOfWeek], [DaylightTransitionEndTimeOfDay], [DaylightDeltaSec]) VALUES (89, 24, 8, N'2013-01-01 00:00:00', N'2013-12-31 00:00:00', 0, 10, 1, 3, 6, N'23:59:59.999', 0, 2, 1, 3, 6, N'23:59:59.999', 3600)
INSERT INTO [DateTimeUtil].[TimezoneAdjustmentRule] ([Id], [TimezoneId], [RuleNo], [DateStart], [DateEnd], [DaylightTransitionStartIsFixedDateRule], [DaylightTransitionStartMonth], [DaylightTransitionStartDay], [DaylightTransitionStartWeek], [DaylightTransitionStartDayOfWeek], [DaylightTransitionStartTimeOfDay], [DaylightTransitionEndIsFixedDateRule], [DaylightTransitionEndMonth], [DaylightTransitionEndDay], [DaylightTransitionEndWeek], [DaylightTransitionEndDayOfWeek], [DaylightTransitionEndTimeOfDay], [DaylightDeltaSec]) VALUES (90, 24, 9, N'2014-01-01 00:00:00', N'2014-12-31 00:00:00', 0, 10, 1, 3, 6, N'23:59:59.999', 0, 2, 1, 3, 6, N'23:59:59.999', 3600)
INSERT INTO [DateTimeUtil].[TimezoneAdjustmentRule] ([Id], [TimezoneId], [RuleNo], [DateStart], [DateEnd], [DaylightTransitionStartIsFixedDateRule], [DaylightTransitionStartMonth], [DaylightTransitionStartDay], [DaylightTransitionStartWeek], [DaylightTransitionStartDayOfWeek], [DaylightTransitionStartTimeOfDay], [DaylightTransitionEndIsFixedDateRule], [DaylightTransitionEndMonth], [DaylightTransitionEndDay], [DaylightTransitionEndWeek], [DaylightTransitionEndDayOfWeek], [DaylightTransitionEndTimeOfDay], [DaylightDeltaSec]) VALUES (91, 24, 10, N'2015-01-01 00:00:00', N'2015-12-31 00:00:00', 0, 10, 1, 3, 6, N'23:59:59.999', 0, 2, 1, 3, 6, N'23:59:59.999', 3600)
INSERT INTO [DateTimeUtil].[TimezoneAdjustmentRule] ([Id], [TimezoneId], [RuleNo], [DateStart], [DateEnd], [DaylightTransitionStartIsFixedDateRule], [DaylightTransitionStartMonth], [DaylightTransitionStartDay], [DaylightTransitionStartWeek], [DaylightTransitionStartDayOfWeek], [DaylightTransitionStartTimeOfDay], [DaylightTransitionEndIsFixedDateRule], [DaylightTransitionEndMonth], [DaylightTransitionEndDay], [DaylightTransitionEndWeek], [DaylightTransitionEndDayOfWeek], [DaylightTransitionEndTimeOfDay], [DaylightDeltaSec]) VALUES (92, 24, 11, N'2016-01-01 00:00:00', N'2016-12-31 00:00:00', 0, 10, 1, 3, 6, N'23:59:59.999', 0, 2, 1, 3, 6, N'23:59:59.999', 3600)
INSERT INTO [DateTimeUtil].[TimezoneAdjustmentRule] ([Id], [TimezoneId], [RuleNo], [DateStart], [DateEnd], [DaylightTransitionStartIsFixedDateRule], [DaylightTransitionStartMonth], [DaylightTransitionStartDay], [DaylightTransitionStartWeek], [DaylightTransitionStartDayOfWeek], [DaylightTransitionStartTimeOfDay], [DaylightTransitionEndIsFixedDateRule], [DaylightTransitionEndMonth], [DaylightTransitionEndDay], [DaylightTransitionEndWeek], [DaylightTransitionEndDayOfWeek], [DaylightTransitionEndTimeOfDay], [DaylightDeltaSec]) VALUES (93, 24, 12, N'2017-01-01 00:00:00', N'2017-12-31 00:00:00', 0, 10, 1, 2, 6, N'23:59:59.999', 0, 2, 1, 3, 6, N'23:59:59.999', 3600)
INSERT INTO [DateTimeUtil].[TimezoneAdjustmentRule] ([Id], [TimezoneId], [RuleNo], [DateStart], [DateEnd], [DaylightTransitionStartIsFixedDateRule], [DaylightTransitionStartMonth], [DaylightTransitionStartDay], [DaylightTransitionStartWeek], [DaylightTransitionStartDayOfWeek], [DaylightTransitionStartTimeOfDay], [DaylightTransitionEndIsFixedDateRule], [DaylightTransitionEndMonth], [DaylightTransitionEndDay], [DaylightTransitionEndWeek], [DaylightTransitionEndDayOfWeek], [DaylightTransitionEndTimeOfDay], [DaylightDeltaSec]) VALUES (94, 24, 13, N'2018-01-01 00:00:00', N'2018-12-31 00:00:00', 0, 10, 1, 3, 6, N'23:59:59.999', 0, 2, 1, 3, 6, N'23:59:59.999', 3600)
INSERT INTO [DateTimeUtil].[TimezoneAdjustmentRule] ([Id], [TimezoneId], [RuleNo], [DateStart], [DateEnd], [DaylightTransitionStartIsFixedDateRule], [DaylightTransitionStartMonth], [DaylightTransitionStartDay], [DaylightTransitionStartWeek], [DaylightTransitionStartDayOfWeek], [DaylightTransitionStartTimeOfDay], [DaylightTransitionEndIsFixedDateRule], [DaylightTransitionEndMonth], [DaylightTransitionEndDay], [DaylightTransitionEndWeek], [DaylightTransitionEndDayOfWeek], [DaylightTransitionEndTimeOfDay], [DaylightDeltaSec]) VALUES (95, 24, 14, N'2019-01-01 00:00:00', N'2019-12-31 00:00:00', 0, 10, 1, 3, 6, N'23:59:59.999', 0, 2, 1, 3, 6, N'23:59:59.999', 3600)
INSERT INTO [DateTimeUtil].[TimezoneAdjustmentRule] ([Id], [TimezoneId], [RuleNo], [DateStart], [DateEnd], [DaylightTransitionStartIsFixedDateRule], [DaylightTransitionStartMonth], [DaylightTransitionStartDay], [DaylightTransitionStartWeek], [DaylightTransitionStartDayOfWeek], [DaylightTransitionStartTimeOfDay], [DaylightTransitionEndIsFixedDateRule], [DaylightTransitionEndMonth], [DaylightTransitionEndDay], [DaylightTransitionEndWeek], [DaylightTransitionEndDayOfWeek], [DaylightTransitionEndTimeOfDay], [DaylightDeltaSec]) VALUES (96, 24, 15, N'2020-01-01 00:00:00', N'2020-12-31 00:00:00', 0, 10, 1, 3, 6, N'23:59:59.999', 0, 2, 1, 3, 6, N'23:59:59.999', 3600)
INSERT INTO [DateTimeUtil].[TimezoneAdjustmentRule] ([Id], [TimezoneId], [RuleNo], [DateStart], [DateEnd], [DaylightTransitionStartIsFixedDateRule], [DaylightTransitionStartMonth], [DaylightTransitionStartDay], [DaylightTransitionStartWeek], [DaylightTransitionStartDayOfWeek], [DaylightTransitionStartTimeOfDay], [DaylightTransitionEndIsFixedDateRule], [DaylightTransitionEndMonth], [DaylightTransitionEndDay], [DaylightTransitionEndWeek], [DaylightTransitionEndDayOfWeek], [DaylightTransitionEndTimeOfDay], [DaylightDeltaSec]) VALUES (97, 24, 16, N'2021-01-01 00:00:00', N'2021-12-31 00:00:00', 0, 10, 1, 3, 6, N'23:59:59.999', 0, 2, 1, 3, 6, N'23:59:59.999', 3600)
INSERT INTO [DateTimeUtil].[TimezoneAdjustmentRule] ([Id], [TimezoneId], [RuleNo], [DateStart], [DateEnd], [DaylightTransitionStartIsFixedDateRule], [DaylightTransitionStartMonth], [DaylightTransitionStartDay], [DaylightTransitionStartWeek], [DaylightTransitionStartDayOfWeek], [DaylightTransitionStartTimeOfDay], [DaylightTransitionEndIsFixedDateRule], [DaylightTransitionEndMonth], [DaylightTransitionEndDay], [DaylightTransitionEndWeek], [DaylightTransitionEndDayOfWeek], [DaylightTransitionEndTimeOfDay], [DaylightDeltaSec]) VALUES (98, 24, 17, N'2022-01-01 00:00:00', N'2022-12-31 00:00:00', 0, 10, 1, 3, 6, N'23:59:59.999', 0, 2, 1, 3, 6, N'23:59:59.999', 3600)
INSERT INTO [DateTimeUtil].[TimezoneAdjustmentRule] ([Id], [TimezoneId], [RuleNo], [DateStart], [DateEnd], [DaylightTransitionStartIsFixedDateRule], [DaylightTransitionStartMonth], [DaylightTransitionStartDay], [DaylightTransitionStartWeek], [DaylightTransitionStartDayOfWeek], [DaylightTransitionStartTimeOfDay], [DaylightTransitionEndIsFixedDateRule], [DaylightTransitionEndMonth], [DaylightTransitionEndDay], [DaylightTransitionEndWeek], [DaylightTransitionEndDayOfWeek], [DaylightTransitionEndTimeOfDay], [DaylightDeltaSec]) VALUES (99, 24, 18, N'2023-01-01 00:00:00', N'2023-12-31 00:00:00', 0, 10, 1, 2, 6, N'23:59:59.999', 0, 2, 1, 4, 6, N'23:59:59.999', 3600)
INSERT INTO [DateTimeUtil].[TimezoneAdjustmentRule] ([Id], [TimezoneId], [RuleNo], [DateStart], [DateEnd], [DaylightTransitionStartIsFixedDateRule], [DaylightTransitionStartMonth], [DaylightTransitionStartDay], [DaylightTransitionStartWeek], [DaylightTransitionStartDayOfWeek], [DaylightTransitionStartTimeOfDay], [DaylightTransitionEndIsFixedDateRule], [DaylightTransitionEndMonth], [DaylightTransitionEndDay], [DaylightTransitionEndWeek], [DaylightTransitionEndDayOfWeek], [DaylightTransitionEndTimeOfDay], [DaylightDeltaSec]) VALUES (100, 24, 19, N'2024-01-01 00:00:00', N'2024-12-31 00:00:00', 0, 10, 1, 3, 6, N'23:59:59.999', 0, 2, 1, 3, 6, N'23:59:59.999', 3600)
INSERT INTO [DateTimeUtil].[TimezoneAdjustmentRule] ([Id], [TimezoneId], [RuleNo], [DateStart], [DateEnd], [DaylightTransitionStartIsFixedDateRule], [DaylightTransitionStartMonth], [DaylightTransitionStartDay], [DaylightTransitionStartWeek], [DaylightTransitionStartDayOfWeek], [DaylightTransitionStartTimeOfDay], [DaylightTransitionEndIsFixedDateRule], [DaylightTransitionEndMonth], [DaylightTransitionEndDay], [DaylightTransitionEndWeek], [DaylightTransitionEndDayOfWeek], [DaylightTransitionEndTimeOfDay], [DaylightDeltaSec]) VALUES (101, 24, 20, N'2025-01-01 00:00:00', N'2025-12-31 00:00:00', 0, 10, 1, 3, 6, N'23:59:59.999', 0, 2, 1, 3, 6, N'23:59:59.999', 3600)
INSERT INTO [DateTimeUtil].[TimezoneAdjustmentRule] ([Id], [TimezoneId], [RuleNo], [DateStart], [DateEnd], [DaylightTransitionStartIsFixedDateRule], [DaylightTransitionStartMonth], [DaylightTransitionStartDay], [DaylightTransitionStartWeek], [DaylightTransitionStartDayOfWeek], [DaylightTransitionStartTimeOfDay], [DaylightTransitionEndIsFixedDateRule], [DaylightTransitionEndMonth], [DaylightTransitionEndDay], [DaylightTransitionEndWeek], [DaylightTransitionEndDayOfWeek], [DaylightTransitionEndTimeOfDay], [DaylightDeltaSec]) VALUES (102, 24, 21, N'2026-01-01 00:00:00', N'2026-12-31 00:00:00', 0, 10, 1, 3, 6, N'23:59:59.999', 0, 2, 1, 3, 6, N'23:59:59.999', 3600)
INSERT INTO [DateTimeUtil].[TimezoneAdjustmentRule] ([Id], [TimezoneId], [RuleNo], [DateStart], [DateEnd], [DaylightTransitionStartIsFixedDateRule], [DaylightTransitionStartMonth], [DaylightTransitionStartDay], [DaylightTransitionStartWeek], [DaylightTransitionStartDayOfWeek], [DaylightTransitionStartTimeOfDay], [DaylightTransitionEndIsFixedDateRule], [DaylightTransitionEndMonth], [DaylightTransitionEndDay], [DaylightTransitionEndWeek], [DaylightTransitionEndDayOfWeek], [DaylightTransitionEndTimeOfDay], [DaylightDeltaSec]) VALUES (103, 24, 22, N'2027-01-01 00:00:00', N'2027-12-31 00:00:00', 0, 10, 1, 3, 6, N'23:59:59.999', 0, 2, 1, 3, 6, N'23:59:59.999', 3600)
INSERT INTO [DateTimeUtil].[TimezoneAdjustmentRule] ([Id], [TimezoneId], [RuleNo], [DateStart], [DateEnd], [DaylightTransitionStartIsFixedDateRule], [DaylightTransitionStartMonth], [DaylightTransitionStartDay], [DaylightTransitionStartWeek], [DaylightTransitionStartDayOfWeek], [DaylightTransitionStartTimeOfDay], [DaylightTransitionEndIsFixedDateRule], [DaylightTransitionEndMonth], [DaylightTransitionEndDay], [DaylightTransitionEndWeek], [DaylightTransitionEndDayOfWeek], [DaylightTransitionEndTimeOfDay], [DaylightDeltaSec]) VALUES (104, 24, 23, N'2028-01-01 00:00:00', N'2028-12-31 00:00:00', 0, 10, 1, 2, 6, N'23:59:59.999', 0, 2, 1, 3, 6, N'23:59:59.999', 3600)
INSERT INTO [DateTimeUtil].[TimezoneAdjustmentRule] ([Id], [TimezoneId], [RuleNo], [DateStart], [DateEnd], [DaylightTransitionStartIsFixedDateRule], [DaylightTransitionStartMonth], [DaylightTransitionStartDay], [DaylightTransitionStartWeek], [DaylightTransitionStartDayOfWeek], [DaylightTransitionStartTimeOfDay], [DaylightTransitionEndIsFixedDateRule], [DaylightTransitionEndMonth], [DaylightTransitionEndDay], [DaylightTransitionEndWeek], [DaylightTransitionEndDayOfWeek], [DaylightTransitionEndTimeOfDay], [DaylightDeltaSec]) VALUES (105, 24, 24, N'2029-01-01 00:00:00', N'2029-12-31 00:00:00', 0, 10, 1, 3, 6, N'23:59:59.999', 0, 2, 1, 3, 6, N'23:59:59.999', 3600)
INSERT INTO [DateTimeUtil].[TimezoneAdjustmentRule] ([Id], [TimezoneId], [RuleNo], [DateStart], [DateEnd], [DaylightTransitionStartIsFixedDateRule], [DaylightTransitionStartMonth], [DaylightTransitionStartDay], [DaylightTransitionStartWeek], [DaylightTransitionStartDayOfWeek], [DaylightTransitionStartTimeOfDay], [DaylightTransitionEndIsFixedDateRule], [DaylightTransitionEndMonth], [DaylightTransitionEndDay], [DaylightTransitionEndWeek], [DaylightTransitionEndDayOfWeek], [DaylightTransitionEndTimeOfDay], [DaylightDeltaSec]) VALUES (106, 24, 25, N'2030-01-01 00:00:00', N'2030-12-31 00:00:00', 0, 10, 1, 3, 6, N'23:59:59.999', 0, 2, 1, 3, 6, N'23:59:59.999', 3600)
INSERT INTO [DateTimeUtil].[TimezoneAdjustmentRule] ([Id], [TimezoneId], [RuleNo], [DateStart], [DateEnd], [DaylightTransitionStartIsFixedDateRule], [DaylightTransitionStartMonth], [DaylightTransitionStartDay], [DaylightTransitionStartWeek], [DaylightTransitionStartDayOfWeek], [DaylightTransitionStartTimeOfDay], [DaylightTransitionEndIsFixedDateRule], [DaylightTransitionEndMonth], [DaylightTransitionEndDay], [DaylightTransitionEndWeek], [DaylightTransitionEndDayOfWeek], [DaylightTransitionEndTimeOfDay], [DaylightDeltaSec]) VALUES (107, 24, 26, N'2031-01-01 00:00:00', N'2031-12-31 00:00:00', 0, 10, 1, 3, 6, N'23:59:59.999', 0, 2, 1, 3, 6, N'23:59:59.999', 3600)
INSERT INTO [DateTimeUtil].[TimezoneAdjustmentRule] ([Id], [TimezoneId], [RuleNo], [DateStart], [DateEnd], [DaylightTransitionStartIsFixedDateRule], [DaylightTransitionStartMonth], [DaylightTransitionStartDay], [DaylightTransitionStartWeek], [DaylightTransitionStartDayOfWeek], [DaylightTransitionStartTimeOfDay], [DaylightTransitionEndIsFixedDateRule], [DaylightTransitionEndMonth], [DaylightTransitionEndDay], [DaylightTransitionEndWeek], [DaylightTransitionEndDayOfWeek], [DaylightTransitionEndTimeOfDay], [DaylightDeltaSec]) VALUES (108, 24, 27, N'2032-01-01 00:00:00', N'2032-12-31 00:00:00', 0, 10, 1, 3, 6, N'23:59:59.999', 0, 2, 1, 2, 6, N'23:59:59.999', 3600)
INSERT INTO [DateTimeUtil].[TimezoneAdjustmentRule] ([Id], [TimezoneId], [RuleNo], [DateStart], [DateEnd], [DaylightTransitionStartIsFixedDateRule], [DaylightTransitionStartMonth], [DaylightTransitionStartDay], [DaylightTransitionStartWeek], [DaylightTransitionStartDayOfWeek], [DaylightTransitionStartTimeOfDay], [DaylightTransitionEndIsFixedDateRule], [DaylightTransitionEndMonth], [DaylightTransitionEndDay], [DaylightTransitionEndWeek], [DaylightTransitionEndDayOfWeek], [DaylightTransitionEndTimeOfDay], [DaylightDeltaSec]) VALUES (109, 24, 28, N'2033-01-01 00:00:00', N'2033-12-31 00:00:00', 0, 10, 1, 3, 6, N'23:59:59.999', 0, 2, 1, 3, 6, N'23:59:59.999', 3600)
INSERT INTO [DateTimeUtil].[TimezoneAdjustmentRule] ([Id], [TimezoneId], [RuleNo], [DateStart], [DateEnd], [DaylightTransitionStartIsFixedDateRule], [DaylightTransitionStartMonth], [DaylightTransitionStartDay], [DaylightTransitionStartWeek], [DaylightTransitionStartDayOfWeek], [DaylightTransitionStartTimeOfDay], [DaylightTransitionEndIsFixedDateRule], [DaylightTransitionEndMonth], [DaylightTransitionEndDay], [DaylightTransitionEndWeek], [DaylightTransitionEndDayOfWeek], [DaylightTransitionEndTimeOfDay], [DaylightDeltaSec]) VALUES (110, 24, 29, N'2034-01-01 00:00:00', N'2034-12-31 00:00:00', 0, 10, 1, 2, 6, N'23:59:59.999', 0, 2, 1, 4, 6, N'23:59:59.999', 3600)
INSERT INTO [DateTimeUtil].[TimezoneAdjustmentRule] ([Id], [TimezoneId], [RuleNo], [DateStart], [DateEnd], [DaylightTransitionStartIsFixedDateRule], [DaylightTransitionStartMonth], [DaylightTransitionStartDay], [DaylightTransitionStartWeek], [DaylightTransitionStartDayOfWeek], [DaylightTransitionStartTimeOfDay], [DaylightTransitionEndIsFixedDateRule], [DaylightTransitionEndMonth], [DaylightTransitionEndDay], [DaylightTransitionEndWeek], [DaylightTransitionEndDayOfWeek], [DaylightTransitionEndTimeOfDay], [DaylightDeltaSec]) VALUES (111, 24, 30, N'2035-01-01 00:00:00', N'2035-12-31 00:00:00', 0, 10, 1, 3, 6, N'23:59:59.999', 0, 2, 1, 3, 6, N'23:59:59.999', 3600)
INSERT INTO [DateTimeUtil].[TimezoneAdjustmentRule] ([Id], [TimezoneId], [RuleNo], [DateStart], [DateEnd], [DaylightTransitionStartIsFixedDateRule], [DaylightTransitionStartMonth], [DaylightTransitionStartDay], [DaylightTransitionStartWeek], [DaylightTransitionStartDayOfWeek], [DaylightTransitionStartTimeOfDay], [DaylightTransitionEndIsFixedDateRule], [DaylightTransitionEndMonth], [DaylightTransitionEndDay], [DaylightTransitionEndWeek], [DaylightTransitionEndDayOfWeek], [DaylightTransitionEndTimeOfDay], [DaylightDeltaSec]) VALUES (112, 24, 31, N'2036-01-01 00:00:00', N'2036-12-31 00:00:00', 0, 10, 1, 3, 6, N'23:59:59.999', 0, 2, 1, 3, 6, N'23:59:59.999', 3600)
INSERT INTO [DateTimeUtil].[TimezoneAdjustmentRule] ([Id], [TimezoneId], [RuleNo], [DateStart], [DateEnd], [DaylightTransitionStartIsFixedDateRule], [DaylightTransitionStartMonth], [DaylightTransitionStartDay], [DaylightTransitionStartWeek], [DaylightTransitionStartDayOfWeek], [DaylightTransitionStartTimeOfDay], [DaylightTransitionEndIsFixedDateRule], [DaylightTransitionEndMonth], [DaylightTransitionEndDay], [DaylightTransitionEndWeek], [DaylightTransitionEndDayOfWeek], [DaylightTransitionEndTimeOfDay], [DaylightDeltaSec]) VALUES (113, 24, 32, N'2037-01-01 00:00:00', N'2037-12-31 00:00:00', 0, 10, 1, 3, 6, N'23:59:59.999', 0, 2, 1, 3, 6, N'23:59:59.999', 3600)
INSERT INTO [DateTimeUtil].[TimezoneAdjustmentRule] ([Id], [TimezoneId], [RuleNo], [DateStart], [DateEnd], [DaylightTransitionStartIsFixedDateRule], [DaylightTransitionStartMonth], [DaylightTransitionStartDay], [DaylightTransitionStartWeek], [DaylightTransitionStartDayOfWeek], [DaylightTransitionStartTimeOfDay], [DaylightTransitionEndIsFixedDateRule], [DaylightTransitionEndMonth], [DaylightTransitionEndDay], [DaylightTransitionEndWeek], [DaylightTransitionEndDayOfWeek], [DaylightTransitionEndTimeOfDay], [DaylightDeltaSec]) VALUES (114, 24, 33, N'2038-01-01 00:00:00', N'2038-12-31 00:00:00', 0, 10, 1, 3, 6, N'23:59:59.999', 0, 2, 1, 3, 6, N'23:59:59.999', 3600)
INSERT INTO [DateTimeUtil].[TimezoneAdjustmentRule] ([Id], [TimezoneId], [RuleNo], [DateStart], [DateEnd], [DaylightTransitionStartIsFixedDateRule], [DaylightTransitionStartMonth], [DaylightTransitionStartDay], [DaylightTransitionStartWeek], [DaylightTransitionStartDayOfWeek], [DaylightTransitionStartTimeOfDay], [DaylightTransitionEndIsFixedDateRule], [DaylightTransitionEndMonth], [DaylightTransitionEndDay], [DaylightTransitionEndWeek], [DaylightTransitionEndDayOfWeek], [DaylightTransitionEndTimeOfDay], [DaylightDeltaSec]) VALUES (115, 24, 34, N'2039-01-01 00:00:00', N'2039-12-31 00:00:00', 0, 10, 1, 3, 6, N'23:59:59.999', 0, 2, 1, 4, 6, N'23:59:59.999', 3600)
INSERT INTO [DateTimeUtil].[TimezoneAdjustmentRule] ([Id], [TimezoneId], [RuleNo], [DateStart], [DateEnd], [DaylightTransitionStartIsFixedDateRule], [DaylightTransitionStartMonth], [DaylightTransitionStartDay], [DaylightTransitionStartWeek], [DaylightTransitionStartDayOfWeek], [DaylightTransitionStartTimeOfDay], [DaylightTransitionEndIsFixedDateRule], [DaylightTransitionEndMonth], [DaylightTransitionEndDay], [DaylightTransitionEndWeek], [DaylightTransitionEndDayOfWeek], [DaylightTransitionEndTimeOfDay], [DaylightDeltaSec]) VALUES (116, 24, 35, N'2040-01-01 00:00:00', N'9999-12-31 00:00:00', 0, 10, 1, 3, 6, N'23:59:59.999', 0, 2, 1, 3, 6, N'23:59:59.999', 3600)
INSERT INTO [DateTimeUtil].[TimezoneAdjustmentRule] ([Id], [TimezoneId], [RuleNo], [DateStart], [DateEnd], [DaylightTransitionStartIsFixedDateRule], [DaylightTransitionStartMonth], [DaylightTransitionStartDay], [DaylightTransitionStartWeek], [DaylightTransitionStartDayOfWeek], [DaylightTransitionStartTimeOfDay], [DaylightTransitionEndIsFixedDateRule], [DaylightTransitionEndMonth], [DaylightTransitionEndDay], [DaylightTransitionEndWeek], [DaylightTransitionEndDayOfWeek], [DaylightTransitionEndTimeOfDay], [DaylightDeltaSec]) VALUES (117, 25, 1, N'2007-01-01 00:00:00', N'2007-12-31 00:00:00', 0, 12, 1, 5, 0, N'00:00:00', 0, 1, 1, 1, 1, N'00:00:00', 3600)
INSERT INTO [DateTimeUtil].[TimezoneAdjustmentRule] ([Id], [TimezoneId], [RuleNo], [DateStart], [DateEnd], [DaylightTransitionStartIsFixedDateRule], [DaylightTransitionStartMonth], [DaylightTransitionStartDay], [DaylightTransitionStartWeek], [DaylightTransitionStartDayOfWeek], [DaylightTransitionStartTimeOfDay], [DaylightTransitionEndIsFixedDateRule], [DaylightTransitionEndMonth], [DaylightTransitionEndDay], [DaylightTransitionEndWeek], [DaylightTransitionEndDayOfWeek], [DaylightTransitionEndTimeOfDay], [DaylightDeltaSec]) VALUES (118, 25, 2, N'2008-01-01 00:00:00', N'2008-12-31 00:00:00', 0, 10, 1, 3, 6, N'23:59:59.999', 0, 3, 1, 3, 0, N'00:00:00', 3600)
INSERT INTO [DateTimeUtil].[TimezoneAdjustmentRule] ([Id], [TimezoneId], [RuleNo], [DateStart], [DateEnd], [DaylightTransitionStartIsFixedDateRule], [DaylightTransitionStartMonth], [DaylightTransitionStartDay], [DaylightTransitionStartWeek], [DaylightTransitionStartDayOfWeek], [DaylightTransitionStartTimeOfDay], [DaylightTransitionEndIsFixedDateRule], [DaylightTransitionEndMonth], [DaylightTransitionEndDay], [DaylightTransitionEndWeek], [DaylightTransitionEndDayOfWeek], [DaylightTransitionEndTimeOfDay], [DaylightDeltaSec]) VALUES (119, 25, 3, N'2009-01-01 00:00:00', N'2009-12-31 00:00:00', 0, 1, 1, 1, 4, N'00:00:00', 0, 3, 1, 2, 6, N'23:59:59.999', 3600)
INSERT INTO [DateTimeUtil].[TimezoneAdjustmentRule] ([Id], [TimezoneId], [RuleNo], [DateStart], [DateEnd], [DaylightTransitionStartIsFixedDateRule], [DaylightTransitionStartMonth], [DaylightTransitionStartDay], [DaylightTransitionStartWeek], [DaylightTransitionStartDayOfWeek], [DaylightTransitionStartTimeOfDay], [DaylightTransitionEndIsFixedDateRule], [DaylightTransitionEndMonth], [DaylightTransitionEndDay], [DaylightTransitionEndWeek], [DaylightTransitionEndDayOfWeek], [DaylightTransitionEndTimeOfDay], [DaylightDeltaSec]) VALUES (120, 27, 1, N'0001-01-01 00:00:00', N'2008-12-31 00:00:00', 0, 4, 1, 1, 0, N'02:00:00', 0, 10, 1, 5, 0, N'02:00:00', 3600)
INSERT INTO [DateTimeUtil].[TimezoneAdjustmentRule] ([Id], [TimezoneId], [RuleNo], [DateStart], [DateEnd], [DaylightTransitionStartIsFixedDateRule], [DaylightTransitionStartMonth], [DaylightTransitionStartDay], [DaylightTransitionStartWeek], [DaylightTransitionStartDayOfWeek], [DaylightTransitionStartTimeOfDay], [DaylightTransitionEndIsFixedDateRule], [DaylightTransitionEndMonth], [DaylightTransitionEndDay], [DaylightTransitionEndWeek], [DaylightTransitionEndDayOfWeek], [DaylightTransitionEndTimeOfDay], [DaylightDeltaSec]) VALUES (121, 27, 2, N'2009-01-01 00:00:00', N'2009-12-31 00:00:00', 0, 3, 1, 5, 6, N'22:00:00', 0, 10, 1, 4, 6, N'23:00:00', 3600)
INSERT INTO [DateTimeUtil].[TimezoneAdjustmentRule] ([Id], [TimezoneId], [RuleNo], [DateStart], [DateEnd], [DaylightTransitionStartIsFixedDateRule], [DaylightTransitionStartMonth], [DaylightTransitionStartDay], [DaylightTransitionStartWeek], [DaylightTransitionStartDayOfWeek], [DaylightTransitionStartTimeOfDay], [DaylightTransitionEndIsFixedDateRule], [DaylightTransitionEndMonth], [DaylightTransitionEndDay], [DaylightTransitionEndWeek], [DaylightTransitionEndDayOfWeek], [DaylightTransitionEndTimeOfDay], [DaylightDeltaSec]) VALUES (122, 27, 3, N'2010-01-01 00:00:00', N'2010-12-31 00:00:00', 0, 3, 1, 5, 6, N'22:00:00', 0, 10, 1, 5, 6, N'23:00:00', 3600)
INSERT INTO [DateTimeUtil].[TimezoneAdjustmentRule] ([Id], [TimezoneId], [RuleNo], [DateStart], [DateEnd], [DaylightTransitionStartIsFixedDateRule], [DaylightTransitionStartMonth], [DaylightTransitionStartDay], [DaylightTransitionStartWeek], [DaylightTransitionStartDayOfWeek], [DaylightTransitionStartTimeOfDay], [DaylightTransitionEndIsFixedDateRule], [DaylightTransitionEndMonth], [DaylightTransitionEndDay], [DaylightTransitionEndWeek], [DaylightTransitionEndDayOfWeek], [DaylightTransitionEndTimeOfDay], [DaylightDeltaSec]) VALUES (123, 27, 4, N'2011-01-01 00:00:00', N'2011-12-31 00:00:00', 0, 3, 1, 5, 6, N'22:00:00', 0, 10, 1, 5, 6, N'23:00:00', 3600)
INSERT INTO [DateTimeUtil].[TimezoneAdjustmentRule] ([Id], [TimezoneId], [RuleNo], [DateStart], [DateEnd], [DaylightTransitionStartIsFixedDateRule], [DaylightTransitionStartMonth], [DaylightTransitionStartDay], [DaylightTransitionStartWeek], [DaylightTransitionStartDayOfWeek], [DaylightTransitionStartTimeOfDay], [DaylightTransitionEndIsFixedDateRule], [DaylightTransitionEndMonth], [DaylightTransitionEndDay], [DaylightTransitionEndWeek], [DaylightTransitionEndDayOfWeek], [DaylightTransitionEndTimeOfDay], [DaylightDeltaSec]) VALUES (124, 27, 5, N'2012-01-01 00:00:00', N'2012-12-31 00:00:00', 0, 3, 1, 4, 6, N'22:00:00', 0, 10, 1, 5, 6, N'23:00:00', 3600)
INSERT INTO [DateTimeUtil].[TimezoneAdjustmentRule] ([Id], [TimezoneId], [RuleNo], [DateStart], [DateEnd], [DaylightTransitionStartIsFixedDateRule], [DaylightTransitionStartMonth], [DaylightTransitionStartDay], [DaylightTransitionStartWeek], [DaylightTransitionStartDayOfWeek], [DaylightTransitionStartTimeOfDay], [DaylightTransitionEndIsFixedDateRule], [DaylightTransitionEndMonth], [DaylightTransitionEndDay], [DaylightTransitionEndWeek], [DaylightTransitionEndDayOfWeek], [DaylightTransitionEndTimeOfDay], [DaylightDeltaSec]) VALUES (125, 27, 6, N'2013-01-01 00:00:00', N'2013-12-31 00:00:00', 0, 3, 1, 5, 6, N'22:00:00', 0, 10, 1, 5, 6, N'23:00:00', 3600)
INSERT INTO [DateTimeUtil].[TimezoneAdjustmentRule] ([Id], [TimezoneId], [RuleNo], [DateStart], [DateEnd], [DaylightTransitionStartIsFixedDateRule], [DaylightTransitionStartMonth], [DaylightTransitionStartDay], [DaylightTransitionStartWeek], [DaylightTransitionStartDayOfWeek], [DaylightTransitionStartTimeOfDay], [DaylightTransitionEndIsFixedDateRule], [DaylightTransitionEndMonth], [DaylightTransitionEndDay], [DaylightTransitionEndWeek], [DaylightTransitionEndDayOfWeek], [DaylightTransitionEndTimeOfDay], [DaylightDeltaSec]) VALUES (126, 27, 7, N'2014-01-01 00:00:00', N'2014-12-31 00:00:00', 0, 3, 1, 5, 6, N'22:00:00', 0, 10, 1, 5, 6, N'23:00:00', 3600)
INSERT INTO [DateTimeUtil].[TimezoneAdjustmentRule] ([Id], [TimezoneId], [RuleNo], [DateStart], [DateEnd], [DaylightTransitionStartIsFixedDateRule], [DaylightTransitionStartMonth], [DaylightTransitionStartDay], [DaylightTransitionStartWeek], [DaylightTransitionStartDayOfWeek], [DaylightTransitionStartTimeOfDay], [DaylightTransitionEndIsFixedDateRule], [DaylightTransitionEndMonth], [DaylightTransitionEndDay], [DaylightTransitionEndWeek], [DaylightTransitionEndDayOfWeek], [DaylightTransitionEndTimeOfDay], [DaylightDeltaSec]) VALUES (127, 27, 8, N'2015-01-01 00:00:00', N'2015-12-31 00:00:00', 0, 3, 1, 5, 6, N'22:00:00', 0, 10, 1, 4, 6, N'23:00:00', 3600)
INSERT INTO [DateTimeUtil].[TimezoneAdjustmentRule] ([Id], [TimezoneId], [RuleNo], [DateStart], [DateEnd], [DaylightTransitionStartIsFixedDateRule], [DaylightTransitionStartMonth], [DaylightTransitionStartDay], [DaylightTransitionStartWeek], [DaylightTransitionStartDayOfWeek], [DaylightTransitionStartTimeOfDay], [DaylightTransitionEndIsFixedDateRule], [DaylightTransitionEndMonth], [DaylightTransitionEndDay], [DaylightTransitionEndWeek], [DaylightTransitionEndDayOfWeek], [DaylightTransitionEndTimeOfDay], [DaylightDeltaSec]) VALUES (128, 27, 9, N'2016-01-01 00:00:00', N'2016-12-31 00:00:00', 0, 3, 1, 5, 6, N'22:00:00', 0, 10, 1, 5, 6, N'23:00:00', 3600)
INSERT INTO [DateTimeUtil].[TimezoneAdjustmentRule] ([Id], [TimezoneId], [RuleNo], [DateStart], [DateEnd], [DaylightTransitionStartIsFixedDateRule], [DaylightTransitionStartMonth], [DaylightTransitionStartDay], [DaylightTransitionStartWeek], [DaylightTransitionStartDayOfWeek], [DaylightTransitionStartTimeOfDay], [DaylightTransitionEndIsFixedDateRule], [DaylightTransitionEndMonth], [DaylightTransitionEndDay], [DaylightTransitionEndWeek], [DaylightTransitionEndDayOfWeek], [DaylightTransitionEndTimeOfDay], [DaylightDeltaSec]) VALUES (129, 27, 10, N'2017-01-01 00:00:00', N'2017-12-31 00:00:00', 0, 3, 1, 5, 6, N'22:00:00', 0, 10, 1, 5, 6, N'23:00:00', 3600)
INSERT INTO [DateTimeUtil].[TimezoneAdjustmentRule] ([Id], [TimezoneId], [RuleNo], [DateStart], [DateEnd], [DaylightTransitionStartIsFixedDateRule], [DaylightTransitionStartMonth], [DaylightTransitionStartDay], [DaylightTransitionStartWeek], [DaylightTransitionStartDayOfWeek], [DaylightTransitionStartTimeOfDay], [DaylightTransitionEndIsFixedDateRule], [DaylightTransitionEndMonth], [DaylightTransitionEndDay], [DaylightTransitionEndWeek], [DaylightTransitionEndDayOfWeek], [DaylightTransitionEndTimeOfDay], [DaylightDeltaSec]) VALUES (130, 27, 11, N'2018-01-01 00:00:00', N'2018-12-31 00:00:00', 0, 3, 1, 4, 6, N'22:00:00', 0, 10, 1, 5, 6, N'23:00:00', 3600)
INSERT INTO [DateTimeUtil].[TimezoneAdjustmentRule] ([Id], [TimezoneId], [RuleNo], [DateStart], [DateEnd], [DaylightTransitionStartIsFixedDateRule], [DaylightTransitionStartMonth], [DaylightTransitionStartDay], [DaylightTransitionStartWeek], [DaylightTransitionStartDayOfWeek], [DaylightTransitionStartTimeOfDay], [DaylightTransitionEndIsFixedDateRule], [DaylightTransitionEndMonth], [DaylightTransitionEndDay], [DaylightTransitionEndWeek], [DaylightTransitionEndDayOfWeek], [DaylightTransitionEndTimeOfDay], [DaylightDeltaSec]) VALUES (131, 27, 12, N'2019-01-01 00:00:00', N'2019-12-31 00:00:00', 0, 3, 1, 5, 6, N'22:00:00', 0, 10, 1, 5, 6, N'23:00:00', 3600)
INSERT INTO [DateTimeUtil].[TimezoneAdjustmentRule] ([Id], [TimezoneId], [RuleNo], [DateStart], [DateEnd], [DaylightTransitionStartIsFixedDateRule], [DaylightTransitionStartMonth], [DaylightTransitionStartDay], [DaylightTransitionStartWeek], [DaylightTransitionStartDayOfWeek], [DaylightTransitionStartTimeOfDay], [DaylightTransitionEndIsFixedDateRule], [DaylightTransitionEndMonth], [DaylightTransitionEndDay], [DaylightTransitionEndWeek], [DaylightTransitionEndDayOfWeek], [DaylightTransitionEndTimeOfDay], [DaylightDeltaSec]) VALUES (132, 27, 13, N'2020-01-01 00:00:00', N'2020-12-31 00:00:00', 0, 3, 1, 5, 6, N'22:00:00', 0, 10, 1, 4, 6, N'23:00:00', 3600)
INSERT INTO [DateTimeUtil].[TimezoneAdjustmentRule] ([Id], [TimezoneId], [RuleNo], [DateStart], [DateEnd], [DaylightTransitionStartIsFixedDateRule], [DaylightTransitionStartMonth], [DaylightTransitionStartDay], [DaylightTransitionStartWeek], [DaylightTransitionStartDayOfWeek], [DaylightTransitionStartTimeOfDay], [DaylightTransitionEndIsFixedDateRule], [DaylightTransitionEndMonth], [DaylightTransitionEndDay], [DaylightTransitionEndWeek], [DaylightTransitionEndDayOfWeek], [DaylightTransitionEndTimeOfDay], [DaylightDeltaSec]) VALUES (133, 27, 14, N'2021-01-01 00:00:00', N'9999-12-31 00:00:00', 0, 3, 1, 5, 6, N'22:00:00', 0, 10, 1, 5, 6, N'23:00:00', 3600)
INSERT INTO [DateTimeUtil].[TimezoneAdjustmentRule] ([Id], [TimezoneId], [RuleNo], [DateStart], [DateEnd], [DaylightTransitionStartIsFixedDateRule], [DaylightTransitionStartMonth], [DaylightTransitionStartDay], [DaylightTransitionStartWeek], [DaylightTransitionStartDayOfWeek], [DaylightTransitionStartTimeOfDay], [DaylightTransitionEndIsFixedDateRule], [DaylightTransitionEndMonth], [DaylightTransitionEndDay], [DaylightTransitionEndWeek], [DaylightTransitionEndDayOfWeek], [DaylightTransitionEndTimeOfDay], [DaylightDeltaSec]) VALUES (134, 28, 1, N'0001-01-01 00:00:00', N'2006-12-31 00:00:00', 0, 9, 1, 2, 0, N'02:00:00', 0, 3, 1, 2, 0, N'02:00:00', 3600)
INSERT INTO [DateTimeUtil].[TimezoneAdjustmentRule] ([Id], [TimezoneId], [RuleNo], [DateStart], [DateEnd], [DaylightTransitionStartIsFixedDateRule], [DaylightTransitionStartMonth], [DaylightTransitionStartDay], [DaylightTransitionStartWeek], [DaylightTransitionStartDayOfWeek], [DaylightTransitionStartTimeOfDay], [DaylightTransitionEndIsFixedDateRule], [DaylightTransitionEndMonth], [DaylightTransitionEndDay], [DaylightTransitionEndWeek], [DaylightTransitionEndDayOfWeek], [DaylightTransitionEndTimeOfDay], [DaylightDeltaSec]) VALUES (135, 28, 2, N'2007-01-01 00:00:00', N'9999-12-31 00:00:00', 0, 10, 1, 1, 0, N'02:00:00', 0, 3, 1, 2, 0, N'02:00:00', 3600)
INSERT INTO [DateTimeUtil].[TimezoneAdjustmentRule] ([Id], [TimezoneId], [RuleNo], [DateStart], [DateEnd], [DaylightTransitionStartIsFixedDateRule], [DaylightTransitionStartMonth], [DaylightTransitionStartDay], [DaylightTransitionStartWeek], [DaylightTransitionStartDayOfWeek], [DaylightTransitionStartTimeOfDay], [DaylightTransitionEndIsFixedDateRule], [DaylightTransitionEndMonth], [DaylightTransitionEndDay], [DaylightTransitionEndWeek], [DaylightTransitionEndDayOfWeek], [DaylightTransitionEndTimeOfDay], [DaylightDeltaSec]) VALUES (136, 29, 1, N'2011-01-01 00:00:00', N'2011-12-31 00:00:00', 0, 10, 1, 3, 6, N'23:59:59.999', 0, 1, 1, 1, 6, N'00:00:00', 3600)
INSERT INTO [DateTimeUtil].[TimezoneAdjustmentRule] ([Id], [TimezoneId], [RuleNo], [DateStart], [DateEnd], [DaylightTransitionStartIsFixedDateRule], [DaylightTransitionStartMonth], [DaylightTransitionStartDay], [DaylightTransitionStartWeek], [DaylightTransitionStartDayOfWeek], [DaylightTransitionStartTimeOfDay], [DaylightTransitionEndIsFixedDateRule], [DaylightTransitionEndMonth], [DaylightTransitionEndDay], [DaylightTransitionEndWeek], [DaylightTransitionEndDayOfWeek], [DaylightTransitionEndTimeOfDay], [DaylightDeltaSec]) VALUES (137, 29, 2, N'2012-01-01 00:00:00', N'2012-12-31 00:00:00', 0, 1, 1, 1, 0, N'00:00:00', 0, 2, 1, 4, 6, N'23:59:59.999', 3600)
INSERT INTO [DateTimeUtil].[TimezoneAdjustmentRule] ([Id], [TimezoneId], [RuleNo], [DateStart], [DateEnd], [DaylightTransitionStartIsFixedDateRule], [DaylightTransitionStartMonth], [DaylightTransitionStartDay], [DaylightTransitionStartWeek], [DaylightTransitionStartDayOfWeek], [DaylightTransitionStartTimeOfDay], [DaylightTransitionEndIsFixedDateRule], [DaylightTransitionEndMonth], [DaylightTransitionEndDay], [DaylightTransitionEndWeek], [DaylightTransitionEndDayOfWeek], [DaylightTransitionEndTimeOfDay], [DaylightDeltaSec]) VALUES (138, 31, 1, N'0001-01-01 00:00:00', N'9999-12-31 00:00:00', 0, 3, 1, 5, 0, N'02:00:00', 0, 9, 1, 5, 0, N'02:00:00', 3600)
INSERT INTO [DateTimeUtil].[TimezoneAdjustmentRule] ([Id], [TimezoneId], [RuleNo], [DateStart], [DateEnd], [DaylightTransitionStartIsFixedDateRule], [DaylightTransitionStartMonth], [DaylightTransitionStartDay], [DaylightTransitionStartWeek], [DaylightTransitionStartDayOfWeek], [DaylightTransitionStartTimeOfDay], [DaylightTransitionEndIsFixedDateRule], [DaylightTransitionEndMonth], [DaylightTransitionEndDay], [DaylightTransitionEndWeek], [DaylightTransitionEndDayOfWeek], [DaylightTransitionEndTimeOfDay], [DaylightDeltaSec]) VALUES (139, 32, 1, N'0001-01-01 00:00:00', N'2011-12-31 00:00:00', 0, 3, 1, 5, 0, N'02:00:00', 0, 10, 1, 5, 0, N'03:00:00', 3600)
INSERT INTO [DateTimeUtil].[TimezoneAdjustmentRule] ([Id], [TimezoneId], [RuleNo], [DateStart], [DateEnd], [DaylightTransitionStartIsFixedDateRule], [DaylightTransitionStartMonth], [DaylightTransitionStartDay], [DaylightTransitionStartWeek], [DaylightTransitionStartDayOfWeek], [DaylightTransitionStartTimeOfDay], [DaylightTransitionEndIsFixedDateRule], [DaylightTransitionEndMonth], [DaylightTransitionEndDay], [DaylightTransitionEndWeek], [DaylightTransitionEndDayOfWeek], [DaylightTransitionEndTimeOfDay], [DaylightDeltaSec]) VALUES (140, 32, 2, N'2012-01-01 00:00:00', N'2012-12-31 00:00:00', 0, 3, 1, 5, 0, N'02:00:00', 0, 10, 1, 5, 0, N'01:00:00', 3600)
INSERT INTO [DateTimeUtil].[TimezoneAdjustmentRule] ([Id], [TimezoneId], [RuleNo], [DateStart], [DateEnd], [DaylightTransitionStartIsFixedDateRule], [DaylightTransitionStartMonth], [DaylightTransitionStartDay], [DaylightTransitionStartWeek], [DaylightTransitionStartDayOfWeek], [DaylightTransitionStartTimeOfDay], [DaylightTransitionEndIsFixedDateRule], [DaylightTransitionEndMonth], [DaylightTransitionEndDay], [DaylightTransitionEndWeek], [DaylightTransitionEndDayOfWeek], [DaylightTransitionEndTimeOfDay], [DaylightDeltaSec]) VALUES (141, 32, 3, N'2013-01-01 00:00:00', N'9999-12-31 00:00:00', 0, 3, 1, 5, 0, N'00:00:00', 0, 10, 1, 5, 0, N'01:00:00', 3600)
INSERT INTO [DateTimeUtil].[TimezoneAdjustmentRule] ([Id], [TimezoneId], [RuleNo], [DateStart], [DateEnd], [DaylightTransitionStartIsFixedDateRule], [DaylightTransitionStartMonth], [DaylightTransitionStartDay], [DaylightTransitionStartWeek], [DaylightTransitionStartDayOfWeek], [DaylightTransitionStartTimeOfDay], [DaylightTransitionEndIsFixedDateRule], [DaylightTransitionEndMonth], [DaylightTransitionEndDay], [DaylightTransitionEndWeek], [DaylightTransitionEndDayOfWeek], [DaylightTransitionEndTimeOfDay], [DaylightDeltaSec]) VALUES (142, 34, 1, N'2008-01-01 00:00:00', N'2008-12-31 00:00:00', 0, 5, 1, 5, 6, N'23:59:59.999', 0, 8, 1, 5, 0, N'23:59:59.999', 3600)
INSERT INTO [DateTimeUtil].[TimezoneAdjustmentRule] ([Id], [TimezoneId], [RuleNo], [DateStart], [DateEnd], [DaylightTransitionStartIsFixedDateRule], [DaylightTransitionStartMonth], [DaylightTransitionStartDay], [DaylightTransitionStartWeek], [DaylightTransitionStartDayOfWeek], [DaylightTransitionStartTimeOfDay], [DaylightTransitionEndIsFixedDateRule], [DaylightTransitionEndMonth], [DaylightTransitionEndDay], [DaylightTransitionEndWeek], [DaylightTransitionEndDayOfWeek], [DaylightTransitionEndTimeOfDay], [DaylightDeltaSec]) VALUES (143, 34, 2, N'2009-01-01 00:00:00', N'2009-12-31 00:00:00', 0, 5, 1, 5, 0, N'23:59:59.999', 0, 8, 1, 3, 4, N'23:59:59.999', 3600)
INSERT INTO [DateTimeUtil].[TimezoneAdjustmentRule] ([Id], [TimezoneId], [RuleNo], [DateStart], [DateEnd], [DaylightTransitionStartIsFixedDateRule], [DaylightTransitionStartMonth], [DaylightTransitionStartDay], [DaylightTransitionStartWeek], [DaylightTransitionStartDayOfWeek], [DaylightTransitionStartTimeOfDay], [DaylightTransitionEndIsFixedDateRule], [DaylightTransitionEndMonth], [DaylightTransitionEndDay], [DaylightTransitionEndWeek], [DaylightTransitionEndDayOfWeek], [DaylightTransitionEndTimeOfDay], [DaylightDeltaSec]) VALUES (144, 34, 3, N'2010-01-01 00:00:00', N'2010-12-31 00:00:00', 0, 5, 1, 1, 6, N'23:59:59.999', 0, 8, 1, 1, 6, N'23:59:59.999', 3600)
INSERT INTO [DateTimeUtil].[TimezoneAdjustmentRule] ([Id], [TimezoneId], [RuleNo], [DateStart], [DateEnd], [DaylightTransitionStartIsFixedDateRule], [DaylightTransitionStartMonth], [DaylightTransitionStartDay], [DaylightTransitionStartWeek], [DaylightTransitionStartDayOfWeek], [DaylightTransitionStartTimeOfDay], [DaylightTransitionEndIsFixedDateRule], [DaylightTransitionEndMonth], [DaylightTransitionEndDay], [DaylightTransitionEndWeek], [DaylightTransitionEndDayOfWeek], [DaylightTransitionEndTimeOfDay], [DaylightDeltaSec]) VALUES (145, 34, 4, N'2011-01-01 00:00:00', N'2011-12-31 00:00:00', 0, 4, 1, 1, 6, N'23:59:59.999', 0, 7, 1, 5, 6, N'23:59:59.999', 3600)
INSERT INTO [DateTimeUtil].[TimezoneAdjustmentRule] ([Id], [TimezoneId], [RuleNo], [DateStart], [DateEnd], [DaylightTransitionStartIsFixedDateRule], [DaylightTransitionStartMonth], [DaylightTransitionStartDay], [DaylightTransitionStartWeek], [DaylightTransitionStartDayOfWeek], [DaylightTransitionStartTimeOfDay], [DaylightTransitionEndIsFixedDateRule], [DaylightTransitionEndMonth], [DaylightTransitionEndDay], [DaylightTransitionEndWeek], [DaylightTransitionEndDayOfWeek], [DaylightTransitionEndTimeOfDay], [DaylightDeltaSec]) VALUES (146, 34, 5, N'2012-01-01 00:00:00', N'2012-12-31 00:00:00', 0, 4, 1, 5, 0, N'02:00:00', 0, 9, 1, 5, 0, N'03:00:00', 3600)
INSERT INTO [DateTimeUtil].[TimezoneAdjustmentRule] ([Id], [TimezoneId], [RuleNo], [DateStart], [DateEnd], [DaylightTransitionStartIsFixedDateRule], [DaylightTransitionStartMonth], [DaylightTransitionStartDay], [DaylightTransitionStartWeek], [DaylightTransitionStartDayOfWeek], [DaylightTransitionStartTimeOfDay], [DaylightTransitionEndIsFixedDateRule], [DaylightTransitionEndMonth], [DaylightTransitionEndDay], [DaylightTransitionEndWeek], [DaylightTransitionEndDayOfWeek], [DaylightTransitionEndTimeOfDay], [DaylightDeltaSec]) VALUES (147, 34, 6, N'2013-01-01 00:00:00', N'2013-12-31 00:00:00', 0, 4, 1, 5, 0, N'02:00:00', 0, 10, 1, 5, 0, N'03:00:00', 3600)
INSERT INTO [DateTimeUtil].[TimezoneAdjustmentRule] ([Id], [TimezoneId], [RuleNo], [DateStart], [DateEnd], [DaylightTransitionStartIsFixedDateRule], [DaylightTransitionStartMonth], [DaylightTransitionStartDay], [DaylightTransitionStartWeek], [DaylightTransitionStartDayOfWeek], [DaylightTransitionStartTimeOfDay], [DaylightTransitionEndIsFixedDateRule], [DaylightTransitionEndMonth], [DaylightTransitionEndDay], [DaylightTransitionEndWeek], [DaylightTransitionEndDayOfWeek], [DaylightTransitionEndTimeOfDay], [DaylightDeltaSec]) VALUES (148, 34, 7, N'2014-01-01 00:00:00', N'9999-12-31 00:00:00', 0, 3, 1, 5, 0, N'02:00:00', 0, 6, 1, 5, 0, N'03:00:00', 3600)
INSERT INTO [DateTimeUtil].[TimezoneAdjustmentRule] ([Id], [TimezoneId], [RuleNo], [DateStart], [DateEnd], [DaylightTransitionStartIsFixedDateRule], [DaylightTransitionStartMonth], [DaylightTransitionStartDay], [DaylightTransitionStartWeek], [DaylightTransitionStartDayOfWeek], [DaylightTransitionStartTimeOfDay], [DaylightTransitionEndIsFixedDateRule], [DaylightTransitionEndMonth], [DaylightTransitionEndDay], [DaylightTransitionEndWeek], [DaylightTransitionEndDayOfWeek], [DaylightTransitionEndTimeOfDay], [DaylightDeltaSec]) VALUES (149, 35, 1, N'0001-01-01 00:00:00', N'9999-12-31 00:00:00', 0, 3, 1, 5, 0, N'01:00:00', 0, 10, 1, 5, 0, N'02:00:00', 3600)
INSERT INTO [DateTimeUtil].[TimezoneAdjustmentRule] ([Id], [TimezoneId], [RuleNo], [DateStart], [DateEnd], [DaylightTransitionStartIsFixedDateRule], [DaylightTransitionStartMonth], [DaylightTransitionStartDay], [DaylightTransitionStartWeek], [DaylightTransitionStartDayOfWeek], [DaylightTransitionStartTimeOfDay], [DaylightTransitionEndIsFixedDateRule], [DaylightTransitionEndMonth], [DaylightTransitionEndDay], [DaylightTransitionEndWeek], [DaylightTransitionEndDayOfWeek], [DaylightTransitionEndTimeOfDay], [DaylightDeltaSec]) VALUES (150, 38, 1, N'0001-01-01 00:00:00', N'9999-12-31 00:00:00', 0, 3, 1, 5, 0, N'02:00:00', 0, 10, 1, 5, 0, N'03:00:00', 3600)
INSERT INTO [DateTimeUtil].[TimezoneAdjustmentRule] ([Id], [TimezoneId], [RuleNo], [DateStart], [DateEnd], [DaylightTransitionStartIsFixedDateRule], [DaylightTransitionStartMonth], [DaylightTransitionStartDay], [DaylightTransitionStartWeek], [DaylightTransitionStartDayOfWeek], [DaylightTransitionStartTimeOfDay], [DaylightTransitionEndIsFixedDateRule], [DaylightTransitionEndMonth], [DaylightTransitionEndDay], [DaylightTransitionEndWeek], [DaylightTransitionEndDayOfWeek], [DaylightTransitionEndTimeOfDay], [DaylightDeltaSec]) VALUES (151, 39, 1, N'0001-01-01 00:00:00', N'9999-12-31 00:00:00', 0, 3, 1, 5, 0, N'02:00:00', 0, 10, 1, 5, 0, N'03:00:00', 3600)
INSERT INTO [DateTimeUtil].[TimezoneAdjustmentRule] ([Id], [TimezoneId], [RuleNo], [DateStart], [DateEnd], [DaylightTransitionStartIsFixedDateRule], [DaylightTransitionStartMonth], [DaylightTransitionStartDay], [DaylightTransitionStartWeek], [DaylightTransitionStartDayOfWeek], [DaylightTransitionStartTimeOfDay], [DaylightTransitionEndIsFixedDateRule], [DaylightTransitionEndMonth], [DaylightTransitionEndDay], [DaylightTransitionEndWeek], [DaylightTransitionEndDayOfWeek], [DaylightTransitionEndTimeOfDay], [DaylightDeltaSec]) VALUES (152, 40, 1, N'0001-01-01 00:00:00', N'9999-12-31 00:00:00', 0, 3, 1, 5, 0, N'02:00:00', 0, 10, 1, 5, 0, N'03:00:00', 3600)
INSERT INTO [DateTimeUtil].[TimezoneAdjustmentRule] ([Id], [TimezoneId], [RuleNo], [DateStart], [DateEnd], [DaylightTransitionStartIsFixedDateRule], [DaylightTransitionStartMonth], [DaylightTransitionStartDay], [DaylightTransitionStartWeek], [DaylightTransitionStartDayOfWeek], [DaylightTransitionStartTimeOfDay], [DaylightTransitionEndIsFixedDateRule], [DaylightTransitionEndMonth], [DaylightTransitionEndDay], [DaylightTransitionEndWeek], [DaylightTransitionEndDayOfWeek], [DaylightTransitionEndTimeOfDay], [DaylightDeltaSec]) VALUES (153, 41, 1, N'0001-01-01 00:00:00', N'9999-12-31 00:00:00', 0, 3, 1, 5, 0, N'02:00:00', 0, 10, 1, 5, 0, N'03:00:00', 3600)
INSERT INTO [DateTimeUtil].[TimezoneAdjustmentRule] ([Id], [TimezoneId], [RuleNo], [DateStart], [DateEnd], [DaylightTransitionStartIsFixedDateRule], [DaylightTransitionStartMonth], [DaylightTransitionStartDay], [DaylightTransitionStartWeek], [DaylightTransitionStartDayOfWeek], [DaylightTransitionStartTimeOfDay], [DaylightTransitionEndIsFixedDateRule], [DaylightTransitionEndMonth], [DaylightTransitionEndDay], [DaylightTransitionEndWeek], [DaylightTransitionEndDayOfWeek], [DaylightTransitionEndTimeOfDay], [DaylightDeltaSec]) VALUES (154, 43, 1, N'0001-01-01 00:00:00', N'2010-12-31 00:00:00', 0, 4, 1, 1, 0, N'02:00:00', 0, 9, 1, 1, 0, N'02:00:00', -3600)
INSERT INTO [DateTimeUtil].[TimezoneAdjustmentRule] ([Id], [TimezoneId], [RuleNo], [DateStart], [DateEnd], [DaylightTransitionStartIsFixedDateRule], [DaylightTransitionStartMonth], [DaylightTransitionStartDay], [DaylightTransitionStartWeek], [DaylightTransitionStartDayOfWeek], [DaylightTransitionStartTimeOfDay], [DaylightTransitionEndIsFixedDateRule], [DaylightTransitionEndMonth], [DaylightTransitionEndDay], [DaylightTransitionEndWeek], [DaylightTransitionEndDayOfWeek], [DaylightTransitionEndTimeOfDay], [DaylightDeltaSec]) VALUES (155, 43, 2, N'2011-01-01 00:00:00', N'9999-12-31 00:00:00', 0, 9, 1, 1, 0, N'02:00:00', 0, 4, 1, 1, 0, N'02:00:00', 3600)
INSERT INTO [DateTimeUtil].[TimezoneAdjustmentRule] ([Id], [TimezoneId], [RuleNo], [DateStart], [DateEnd], [DaylightTransitionStartIsFixedDateRule], [DaylightTransitionStartMonth], [DaylightTransitionStartDay], [DaylightTransitionStartWeek], [DaylightTransitionStartDayOfWeek], [DaylightTransitionStartTimeOfDay], [DaylightTransitionEndIsFixedDateRule], [DaylightTransitionEndMonth], [DaylightTransitionEndDay], [DaylightTransitionEndWeek], [DaylightTransitionEndDayOfWeek], [DaylightTransitionEndTimeOfDay], [DaylightDeltaSec]) VALUES (156, 44, 1, N'0001-01-01 00:00:00', N'2006-12-31 00:00:00', 0, 3, 1, 5, 4, N'00:00:00', 0, 9, 1, 5, 5, N'01:00:00', 3600)
INSERT INTO [DateTimeUtil].[TimezoneAdjustmentRule] ([Id], [TimezoneId], [RuleNo], [DateStart], [DateEnd], [DaylightTransitionStartIsFixedDateRule], [DaylightTransitionStartMonth], [DaylightTransitionStartDay], [DaylightTransitionStartWeek], [DaylightTransitionStartDayOfWeek], [DaylightTransitionStartTimeOfDay], [DaylightTransitionEndIsFixedDateRule], [DaylightTransitionEndMonth], [DaylightTransitionEndDay], [DaylightTransitionEndWeek], [DaylightTransitionEndDayOfWeek], [DaylightTransitionEndTimeOfDay], [DaylightDeltaSec]) VALUES (157, 44, 2, N'2007-01-01 00:00:00', N'2007-12-31 00:00:00', 0, 3, 1, 5, 4, N'23:59:59.999', 0, 10, 1, 5, 5, N'01:00:00', 3600)
INSERT INTO [DateTimeUtil].[TimezoneAdjustmentRule] ([Id], [TimezoneId], [RuleNo], [DateStart], [DateEnd], [DaylightTransitionStartIsFixedDateRule], [DaylightTransitionStartMonth], [DaylightTransitionStartDay], [DaylightTransitionStartWeek], [DaylightTransitionStartDayOfWeek], [DaylightTransitionStartTimeOfDay], [DaylightTransitionEndIsFixedDateRule], [DaylightTransitionEndMonth], [DaylightTransitionEndDay], [DaylightTransitionEndWeek], [DaylightTransitionEndDayOfWeek], [DaylightTransitionEndTimeOfDay], [DaylightDeltaSec]) VALUES (158, 44, 3, N'2008-01-01 00:00:00', N'2008-12-31 00:00:00', 0, 3, 1, 5, 4, N'23:59:59.999', 0, 10, 1, 5, 5, N'01:00:00', 3600)
INSERT INTO [DateTimeUtil].[TimezoneAdjustmentRule] ([Id], [TimezoneId], [RuleNo], [DateStart], [DateEnd], [DaylightTransitionStartIsFixedDateRule], [DaylightTransitionStartMonth], [DaylightTransitionStartDay], [DaylightTransitionStartWeek], [DaylightTransitionStartDayOfWeek], [DaylightTransitionStartTimeOfDay], [DaylightTransitionEndIsFixedDateRule], [DaylightTransitionEndMonth], [DaylightTransitionEndDay], [DaylightTransitionEndWeek], [DaylightTransitionEndDayOfWeek], [DaylightTransitionEndTimeOfDay], [DaylightDeltaSec]) VALUES (159, 44, 4, N'2009-01-01 00:00:00', N'2009-12-31 00:00:00', 0, 3, 1, 5, 4, N'23:59:59.999', 0, 10, 1, 5, 5, N'01:00:00', 3600)
INSERT INTO [DateTimeUtil].[TimezoneAdjustmentRule] ([Id], [TimezoneId], [RuleNo], [DateStart], [DateEnd], [DaylightTransitionStartIsFixedDateRule], [DaylightTransitionStartMonth], [DaylightTransitionStartDay], [DaylightTransitionStartWeek], [DaylightTransitionStartDayOfWeek], [DaylightTransitionStartTimeOfDay], [DaylightTransitionEndIsFixedDateRule], [DaylightTransitionEndMonth], [DaylightTransitionEndDay], [DaylightTransitionEndWeek], [DaylightTransitionEndDayOfWeek], [DaylightTransitionEndTimeOfDay], [DaylightDeltaSec]) VALUES (160, 44, 5, N'2010-01-01 00:00:00', N'2010-12-31 00:00:00', 0, 3, 1, 5, 4, N'23:59:59.999', 0, 10, 1, 5, 5, N'01:00:00', 3600)
INSERT INTO [DateTimeUtil].[TimezoneAdjustmentRule] ([Id], [TimezoneId], [RuleNo], [DateStart], [DateEnd], [DaylightTransitionStartIsFixedDateRule], [DaylightTransitionStartMonth], [DaylightTransitionStartDay], [DaylightTransitionStartWeek], [DaylightTransitionStartDayOfWeek], [DaylightTransitionStartTimeOfDay], [DaylightTransitionEndIsFixedDateRule], [DaylightTransitionEndMonth], [DaylightTransitionEndDay], [DaylightTransitionEndWeek], [DaylightTransitionEndDayOfWeek], [DaylightTransitionEndTimeOfDay], [DaylightDeltaSec]) VALUES (161, 44, 6, N'2011-01-01 00:00:00', N'2011-12-31 00:00:00', 0, 3, 1, 5, 4, N'23:59:59.999', 0, 10, 1, 5, 5, N'01:00:00', 3600)
INSERT INTO [DateTimeUtil].[TimezoneAdjustmentRule] ([Id], [TimezoneId], [RuleNo], [DateStart], [DateEnd], [DaylightTransitionStartIsFixedDateRule], [DaylightTransitionStartMonth], [DaylightTransitionStartDay], [DaylightTransitionStartWeek], [DaylightTransitionStartDayOfWeek], [DaylightTransitionStartTimeOfDay], [DaylightTransitionEndIsFixedDateRule], [DaylightTransitionEndMonth], [DaylightTransitionEndDay], [DaylightTransitionEndWeek], [DaylightTransitionEndDayOfWeek], [DaylightTransitionEndTimeOfDay], [DaylightDeltaSec]) VALUES (162, 44, 7, N'2012-01-01 00:00:00', N'2012-12-31 00:00:00', 0, 3, 1, 5, 4, N'23:59:59.999', 0, 1, 1, 1, 0, N'00:00:00', 3600)
INSERT INTO [DateTimeUtil].[TimezoneAdjustmentRule] ([Id], [TimezoneId], [RuleNo], [DateStart], [DateEnd], [DaylightTransitionStartIsFixedDateRule], [DaylightTransitionStartMonth], [DaylightTransitionStartDay], [DaylightTransitionStartWeek], [DaylightTransitionStartDayOfWeek], [DaylightTransitionStartTimeOfDay], [DaylightTransitionEndIsFixedDateRule], [DaylightTransitionEndMonth], [DaylightTransitionEndDay], [DaylightTransitionEndWeek], [DaylightTransitionEndDayOfWeek], [DaylightTransitionEndTimeOfDay], [DaylightDeltaSec]) VALUES (163, 44, 8, N'2013-01-01 00:00:00', N'2013-12-31 00:00:00', 0, 1, 1, 1, 2, N'00:00:00', 0, 12, 1, 3, 5, N'00:00:00', 3600)
INSERT INTO [DateTimeUtil].[TimezoneAdjustmentRule] ([Id], [TimezoneId], [RuleNo], [DateStart], [DateEnd], [DaylightTransitionStartIsFixedDateRule], [DaylightTransitionStartMonth], [DaylightTransitionStartDay], [DaylightTransitionStartWeek], [DaylightTransitionStartDayOfWeek], [DaylightTransitionStartTimeOfDay], [DaylightTransitionEndIsFixedDateRule], [DaylightTransitionEndMonth], [DaylightTransitionEndDay], [DaylightTransitionEndWeek], [DaylightTransitionEndDayOfWeek], [DaylightTransitionEndTimeOfDay], [DaylightDeltaSec]) VALUES (164, 44, 9, N'2014-01-01 00:00:00', N'9999-12-31 00:00:00', 0, 3, 1, 5, 4, N'23:59:59.999', 0, 10, 1, 5, 5, N'01:00:00', 3600)
INSERT INTO [DateTimeUtil].[TimezoneAdjustmentRule] ([Id], [TimezoneId], [RuleNo], [DateStart], [DateEnd], [DaylightTransitionStartIsFixedDateRule], [DaylightTransitionStartMonth], [DaylightTransitionStartDay], [DaylightTransitionStartWeek], [DaylightTransitionStartDayOfWeek], [DaylightTransitionStartTimeOfDay], [DaylightTransitionEndIsFixedDateRule], [DaylightTransitionEndMonth], [DaylightTransitionEndDay], [DaylightTransitionEndWeek], [DaylightTransitionEndDayOfWeek], [DaylightTransitionEndTimeOfDay], [DaylightDeltaSec]) VALUES (165, 45, 1, N'0001-01-01 00:00:00', N'9999-12-31 00:00:00', 0, 3, 1, 5, 0, N'03:00:00', 0, 10, 1, 5, 0, N'04:00:00', 3600)
INSERT INTO [DateTimeUtil].[TimezoneAdjustmentRule] ([Id], [TimezoneId], [RuleNo], [DateStart], [DateEnd], [DaylightTransitionStartIsFixedDateRule], [DaylightTransitionStartMonth], [DaylightTransitionStartDay], [DaylightTransitionStartWeek], [DaylightTransitionStartDayOfWeek], [DaylightTransitionStartTimeOfDay], [DaylightTransitionEndIsFixedDateRule], [DaylightTransitionEndMonth], [DaylightTransitionEndDay], [DaylightTransitionEndWeek], [DaylightTransitionEndDayOfWeek], [DaylightTransitionEndTimeOfDay], [DaylightDeltaSec]) VALUES (166, 46, 1, N'0001-01-01 00:00:00', N'2009-12-31 00:00:00', 0, 3, 1, 5, 0, N'00:00:00', 0, 10, 1, 5, 0, N'00:00:00', 3600)
INSERT INTO [DateTimeUtil].[TimezoneAdjustmentRule] ([Id], [TimezoneId], [RuleNo], [DateStart], [DateEnd], [DaylightTransitionStartIsFixedDateRule], [DaylightTransitionStartMonth], [DaylightTransitionStartDay], [DaylightTransitionStartWeek], [DaylightTransitionStartDayOfWeek], [DaylightTransitionStartTimeOfDay], [DaylightTransitionEndIsFixedDateRule], [DaylightTransitionEndMonth], [DaylightTransitionEndDay], [DaylightTransitionEndWeek], [DaylightTransitionEndDayOfWeek], [DaylightTransitionEndTimeOfDay], [DaylightDeltaSec]) VALUES (167, 46, 2, N'2010-01-01 00:00:00', N'2010-12-31 00:00:00', 0, 3, 1, 5, 6, N'23:59:59.999', 0, 10, 1, 5, 6, N'23:59:59.999', 3600)
INSERT INTO [DateTimeUtil].[TimezoneAdjustmentRule] ([Id], [TimezoneId], [RuleNo], [DateStart], [DateEnd], [DaylightTransitionStartIsFixedDateRule], [DaylightTransitionStartMonth], [DaylightTransitionStartDay], [DaylightTransitionStartWeek], [DaylightTransitionStartDayOfWeek], [DaylightTransitionStartTimeOfDay], [DaylightTransitionEndIsFixedDateRule], [DaylightTransitionEndMonth], [DaylightTransitionEndDay], [DaylightTransitionEndWeek], [DaylightTransitionEndDayOfWeek], [DaylightTransitionEndTimeOfDay], [DaylightDeltaSec]) VALUES (168, 46, 3, N'2011-01-01 00:00:00', N'2011-12-31 00:00:00', 0, 3, 1, 5, 6, N'23:59:59.999', 0, 10, 1, 5, 6, N'23:59:59.999', 3600)
INSERT INTO [DateTimeUtil].[TimezoneAdjustmentRule] ([Id], [TimezoneId], [RuleNo], [DateStart], [DateEnd], [DaylightTransitionStartIsFixedDateRule], [DaylightTransitionStartMonth], [DaylightTransitionStartDay], [DaylightTransitionStartWeek], [DaylightTransitionStartDayOfWeek], [DaylightTransitionStartTimeOfDay], [DaylightTransitionEndIsFixedDateRule], [DaylightTransitionEndMonth], [DaylightTransitionEndDay], [DaylightTransitionEndWeek], [DaylightTransitionEndDayOfWeek], [DaylightTransitionEndTimeOfDay], [DaylightDeltaSec]) VALUES (169, 46, 4, N'2012-01-01 00:00:00', N'2012-12-31 00:00:00', 0, 3, 1, 4, 6, N'23:59:59.999', 0, 10, 1, 5, 6, N'23:59:59.999', 3600)
INSERT INTO [DateTimeUtil].[TimezoneAdjustmentRule] ([Id], [TimezoneId], [RuleNo], [DateStart], [DateEnd], [DaylightTransitionStartIsFixedDateRule], [DaylightTransitionStartMonth], [DaylightTransitionStartDay], [DaylightTransitionStartWeek], [DaylightTransitionStartDayOfWeek], [DaylightTransitionStartTimeOfDay], [DaylightTransitionEndIsFixedDateRule], [DaylightTransitionEndMonth], [DaylightTransitionEndDay], [DaylightTransitionEndWeek], [DaylightTransitionEndDayOfWeek], [DaylightTransitionEndTimeOfDay], [DaylightDeltaSec]) VALUES (170, 46, 5, N'2013-01-01 00:00:00', N'2013-12-31 00:00:00', 0, 3, 1, 5, 6, N'23:59:59.999', 0, 10, 1, 5, 6, N'23:59:59.999', 3600)
INSERT INTO [DateTimeUtil].[TimezoneAdjustmentRule] ([Id], [TimezoneId], [RuleNo], [DateStart], [DateEnd], [DaylightTransitionStartIsFixedDateRule], [DaylightTransitionStartMonth], [DaylightTransitionStartDay], [DaylightTransitionStartWeek], [DaylightTransitionStartDayOfWeek], [DaylightTransitionStartTimeOfDay], [DaylightTransitionEndIsFixedDateRule], [DaylightTransitionEndMonth], [DaylightTransitionEndDay], [DaylightTransitionEndWeek], [DaylightTransitionEndDayOfWeek], [DaylightTransitionEndTimeOfDay], [DaylightDeltaSec]) VALUES (171, 46, 6, N'2014-01-01 00:00:00', N'2014-12-31 00:00:00', 0, 3, 1, 5, 6, N'23:59:59.999', 0, 10, 1, 5, 6, N'23:59:59.999', 3600)
INSERT INTO [DateTimeUtil].[TimezoneAdjustmentRule] ([Id], [TimezoneId], [RuleNo], [DateStart], [DateEnd], [DaylightTransitionStartIsFixedDateRule], [DaylightTransitionStartMonth], [DaylightTransitionStartDay], [DaylightTransitionStartWeek], [DaylightTransitionStartDayOfWeek], [DaylightTransitionStartTimeOfDay], [DaylightTransitionEndIsFixedDateRule], [DaylightTransitionEndMonth], [DaylightTransitionEndDay], [DaylightTransitionEndWeek], [DaylightTransitionEndDayOfWeek], [DaylightTransitionEndTimeOfDay], [DaylightDeltaSec]) VALUES (172, 46, 7, N'2015-01-01 00:00:00', N'2015-12-31 00:00:00', 0, 3, 1, 5, 6, N'23:59:59.999', 0, 10, 1, 4, 6, N'23:59:59.999', 3600)
INSERT INTO [DateTimeUtil].[TimezoneAdjustmentRule] ([Id], [TimezoneId], [RuleNo], [DateStart], [DateEnd], [DaylightTransitionStartIsFixedDateRule], [DaylightTransitionStartMonth], [DaylightTransitionStartDay], [DaylightTransitionStartWeek], [DaylightTransitionStartDayOfWeek], [DaylightTransitionStartTimeOfDay], [DaylightTransitionEndIsFixedDateRule], [DaylightTransitionEndMonth], [DaylightTransitionEndDay], [DaylightTransitionEndWeek], [DaylightTransitionEndDayOfWeek], [DaylightTransitionEndTimeOfDay], [DaylightDeltaSec]) VALUES (173, 46, 8, N'2016-01-01 00:00:00', N'2016-12-31 00:00:00', 0, 3, 1, 5, 6, N'23:59:59.999', 0, 10, 1, 5, 6, N'23:59:59.999', 3600)
INSERT INTO [DateTimeUtil].[TimezoneAdjustmentRule] ([Id], [TimezoneId], [RuleNo], [DateStart], [DateEnd], [DaylightTransitionStartIsFixedDateRule], [DaylightTransitionStartMonth], [DaylightTransitionStartDay], [DaylightTransitionStartWeek], [DaylightTransitionStartDayOfWeek], [DaylightTransitionStartTimeOfDay], [DaylightTransitionEndIsFixedDateRule], [DaylightTransitionEndMonth], [DaylightTransitionEndDay], [DaylightTransitionEndWeek], [DaylightTransitionEndDayOfWeek], [DaylightTransitionEndTimeOfDay], [DaylightDeltaSec]) VALUES (174, 46, 9, N'2017-01-01 00:00:00', N'2017-12-31 00:00:00', 0, 3, 1, 5, 6, N'23:59:59.999', 0, 10, 1, 5, 6, N'23:59:59.999', 3600)
INSERT INTO [DateTimeUtil].[TimezoneAdjustmentRule] ([Id], [TimezoneId], [RuleNo], [DateStart], [DateEnd], [DaylightTransitionStartIsFixedDateRule], [DaylightTransitionStartMonth], [DaylightTransitionStartDay], [DaylightTransitionStartWeek], [DaylightTransitionStartDayOfWeek], [DaylightTransitionStartTimeOfDay], [DaylightTransitionEndIsFixedDateRule], [DaylightTransitionEndMonth], [DaylightTransitionEndDay], [DaylightTransitionEndWeek], [DaylightTransitionEndDayOfWeek], [DaylightTransitionEndTimeOfDay], [DaylightDeltaSec]) VALUES (175, 46, 10, N'2018-01-01 00:00:00', N'2018-12-31 00:00:00', 0, 3, 1, 4, 6, N'23:59:59.999', 0, 10, 1, 5, 6, N'23:59:59.999', 3600)
INSERT INTO [DateTimeUtil].[TimezoneAdjustmentRule] ([Id], [TimezoneId], [RuleNo], [DateStart], [DateEnd], [DaylightTransitionStartIsFixedDateRule], [DaylightTransitionStartMonth], [DaylightTransitionStartDay], [DaylightTransitionStartWeek], [DaylightTransitionStartDayOfWeek], [DaylightTransitionStartTimeOfDay], [DaylightTransitionEndIsFixedDateRule], [DaylightTransitionEndMonth], [DaylightTransitionEndDay], [DaylightTransitionEndWeek], [DaylightTransitionEndDayOfWeek], [DaylightTransitionEndTimeOfDay], [DaylightDeltaSec]) VALUES (176, 46, 11, N'2019-01-01 00:00:00', N'2019-12-31 00:00:00', 0, 3, 1, 5, 6, N'23:59:59.999', 0, 10, 1, 5, 6, N'23:59:59.999', 3600)
INSERT INTO [DateTimeUtil].[TimezoneAdjustmentRule] ([Id], [TimezoneId], [RuleNo], [DateStart], [DateEnd], [DaylightTransitionStartIsFixedDateRule], [DaylightTransitionStartMonth], [DaylightTransitionStartDay], [DaylightTransitionStartWeek], [DaylightTransitionStartDayOfWeek], [DaylightTransitionStartTimeOfDay], [DaylightTransitionEndIsFixedDateRule], [DaylightTransitionEndMonth], [DaylightTransitionEndDay], [DaylightTransitionEndWeek], [DaylightTransitionEndDayOfWeek], [DaylightTransitionEndTimeOfDay], [DaylightDeltaSec]) VALUES (177, 46, 12, N'2020-01-01 00:00:00', N'2020-12-31 00:00:00', 0, 3, 1, 5, 6, N'23:59:59.999', 0, 10, 1, 4, 6, N'23:59:59.999', 3600)
INSERT INTO [DateTimeUtil].[TimezoneAdjustmentRule] ([Id], [TimezoneId], [RuleNo], [DateStart], [DateEnd], [DaylightTransitionStartIsFixedDateRule], [DaylightTransitionStartMonth], [DaylightTransitionStartDay], [DaylightTransitionStartWeek], [DaylightTransitionStartDayOfWeek], [DaylightTransitionStartTimeOfDay], [DaylightTransitionEndIsFixedDateRule], [DaylightTransitionEndMonth], [DaylightTransitionEndDay], [DaylightTransitionEndWeek], [DaylightTransitionEndDayOfWeek], [DaylightTransitionEndTimeOfDay], [DaylightDeltaSec]) VALUES (178, 46, 13, N'2021-01-01 00:00:00', N'9999-12-31 00:00:00', 0, 3, 1, 5, 6, N'23:59:59.999', 0, 10, 1, 5, 6, N'23:59:59.999', 3600)
INSERT INTO [DateTimeUtil].[TimezoneAdjustmentRule] ([Id], [TimezoneId], [RuleNo], [DateStart], [DateEnd], [DaylightTransitionStartIsFixedDateRule], [DaylightTransitionStartMonth], [DaylightTransitionStartDay], [DaylightTransitionStartWeek], [DaylightTransitionStartDayOfWeek], [DaylightTransitionStartTimeOfDay], [DaylightTransitionEndIsFixedDateRule], [DaylightTransitionEndMonth], [DaylightTransitionEndDay], [DaylightTransitionEndWeek], [DaylightTransitionEndDayOfWeek], [DaylightTransitionEndTimeOfDay], [DaylightDeltaSec]) VALUES (179, 47, 1, N'0001-01-01 00:00:00', N'2006-12-31 00:00:00', 0, 3, 1, 5, 5, N'23:59:59.999', 0, 9, 1, 3, 3, N'23:59:59.999', 3600)
INSERT INTO [DateTimeUtil].[TimezoneAdjustmentRule] ([Id], [TimezoneId], [RuleNo], [DateStart], [DateEnd], [DaylightTransitionStartIsFixedDateRule], [DaylightTransitionStartMonth], [DaylightTransitionStartDay], [DaylightTransitionStartWeek], [DaylightTransitionStartDayOfWeek], [DaylightTransitionStartTimeOfDay], [DaylightTransitionEndIsFixedDateRule], [DaylightTransitionEndMonth], [DaylightTransitionEndDay], [DaylightTransitionEndWeek], [DaylightTransitionEndDayOfWeek], [DaylightTransitionEndTimeOfDay], [DaylightDeltaSec]) VALUES (180, 47, 2, N'2007-01-01 00:00:00', N'2007-12-31 00:00:00', 0, 3, 1, 5, 4, N'23:59:59.999', 0, 11, 1, 1, 4, N'23:59:59.999', 3600)
INSERT INTO [DateTimeUtil].[TimezoneAdjustmentRule] ([Id], [TimezoneId], [RuleNo], [DateStart], [DateEnd], [DaylightTransitionStartIsFixedDateRule], [DaylightTransitionStartMonth], [DaylightTransitionStartDay], [DaylightTransitionStartWeek], [DaylightTransitionStartDayOfWeek], [DaylightTransitionStartTimeOfDay], [DaylightTransitionEndIsFixedDateRule], [DaylightTransitionEndMonth], [DaylightTransitionEndDay], [DaylightTransitionEndWeek], [DaylightTransitionEndDayOfWeek], [DaylightTransitionEndTimeOfDay], [DaylightDeltaSec]) VALUES (181, 47, 3, N'2008-01-01 00:00:00', N'2008-12-31 00:00:00', 0, 4, 1, 1, 4, N'23:59:59.999', 0, 10, 1, 5, 5, N'23:59:59.999', 3600)
INSERT INTO [DateTimeUtil].[TimezoneAdjustmentRule] ([Id], [TimezoneId], [RuleNo], [DateStart], [DateEnd], [DaylightTransitionStartIsFixedDateRule], [DaylightTransitionStartMonth], [DaylightTransitionStartDay], [DaylightTransitionStartWeek], [DaylightTransitionStartDayOfWeek], [DaylightTransitionStartTimeOfDay], [DaylightTransitionEndIsFixedDateRule], [DaylightTransitionEndMonth], [DaylightTransitionEndDay], [DaylightTransitionEndWeek], [DaylightTransitionEndDayOfWeek], [DaylightTransitionEndTimeOfDay], [DaylightDeltaSec]) VALUES (182, 47, 4, N'2009-01-01 00:00:00', N'2009-12-31 00:00:00', 0, 3, 1, 5, 4, N'23:59:59.999', 0, 10, 1, 5, 4, N'23:59:59.999', 3600)
INSERT INTO [DateTimeUtil].[TimezoneAdjustmentRule] ([Id], [TimezoneId], [RuleNo], [DateStart], [DateEnd], [DaylightTransitionStartIsFixedDateRule], [DaylightTransitionStartMonth], [DaylightTransitionStartDay], [DaylightTransitionStartWeek], [DaylightTransitionStartDayOfWeek], [DaylightTransitionStartTimeOfDay], [DaylightTransitionEndIsFixedDateRule], [DaylightTransitionEndMonth], [DaylightTransitionEndDay], [DaylightTransitionEndWeek], [DaylightTransitionEndDayOfWeek], [DaylightTransitionEndTimeOfDay], [DaylightDeltaSec]) VALUES (183, 47, 5, N'2010-01-01 00:00:00', N'2010-12-31 00:00:00', 0, 4, 1, 1, 4, N'23:59:59.999', 0, 10, 1, 5, 4, N'23:59:59.999', 3600)
INSERT INTO [DateTimeUtil].[TimezoneAdjustmentRule] ([Id], [TimezoneId], [RuleNo], [DateStart], [DateEnd], [DaylightTransitionStartIsFixedDateRule], [DaylightTransitionStartMonth], [DaylightTransitionStartDay], [DaylightTransitionStartWeek], [DaylightTransitionStartDayOfWeek], [DaylightTransitionStartTimeOfDay], [DaylightTransitionEndIsFixedDateRule], [DaylightTransitionEndMonth], [DaylightTransitionEndDay], [DaylightTransitionEndWeek], [DaylightTransitionEndDayOfWeek], [DaylightTransitionEndTimeOfDay], [DaylightDeltaSec]) VALUES (184, 47, 6, N'2011-01-01 00:00:00', N'2011-12-31 00:00:00', 0, 3, 1, 5, 4, N'23:59:59.999', 0, 10, 1, 5, 4, N'23:59:59.999', 3600)
INSERT INTO [DateTimeUtil].[TimezoneAdjustmentRule] ([Id], [TimezoneId], [RuleNo], [DateStart], [DateEnd], [DaylightTransitionStartIsFixedDateRule], [DaylightTransitionStartMonth], [DaylightTransitionStartDay], [DaylightTransitionStartWeek], [DaylightTransitionStartDayOfWeek], [DaylightTransitionStartTimeOfDay], [DaylightTransitionEndIsFixedDateRule], [DaylightTransitionEndMonth], [DaylightTransitionEndDay], [DaylightTransitionEndWeek], [DaylightTransitionEndDayOfWeek], [DaylightTransitionEndTimeOfDay], [DaylightDeltaSec]) VALUES (185, 47, 7, N'2012-01-01 00:00:00', N'2012-12-31 00:00:00', 0, 4, 1, 1, 4, N'23:59:59.999', 0, 10, 1, 5, 4, N'23:59:59.999', 3600)
INSERT INTO [DateTimeUtil].[TimezoneAdjustmentRule] ([Id], [TimezoneId], [RuleNo], [DateStart], [DateEnd], [DaylightTransitionStartIsFixedDateRule], [DaylightTransitionStartMonth], [DaylightTransitionStartDay], [DaylightTransitionStartWeek], [DaylightTransitionStartDayOfWeek], [DaylightTransitionStartTimeOfDay], [DaylightTransitionEndIsFixedDateRule], [DaylightTransitionEndMonth], [DaylightTransitionEndDay], [DaylightTransitionEndWeek], [DaylightTransitionEndDayOfWeek], [DaylightTransitionEndTimeOfDay], [DaylightDeltaSec]) VALUES (186, 47, 8, N'2013-01-01 00:00:00', N'2013-12-31 00:00:00', 0, 4, 1, 1, 4, N'23:59:59.999', 0, 10, 1, 5, 4, N'23:59:59.999', 3600)
INSERT INTO [DateTimeUtil].[TimezoneAdjustmentRule] ([Id], [TimezoneId], [RuleNo], [DateStart], [DateEnd], [DaylightTransitionStartIsFixedDateRule], [DaylightTransitionStartMonth], [DaylightTransitionStartDay], [DaylightTransitionStartWeek], [DaylightTransitionStartDayOfWeek], [DaylightTransitionStartTimeOfDay], [DaylightTransitionEndIsFixedDateRule], [DaylightTransitionEndMonth], [DaylightTransitionEndDay], [DaylightTransitionEndWeek], [DaylightTransitionEndDayOfWeek], [DaylightTransitionEndTimeOfDay], [DaylightDeltaSec]) VALUES (187, 47, 9, N'2014-01-01 00:00:00', N'2014-12-31 00:00:00', 0, 4, 1, 1, 4, N'23:59:59.999', 0, 10, 1, 5, 4, N'23:59:59.999', 3600)
INSERT INTO [DateTimeUtil].[TimezoneAdjustmentRule] ([Id], [TimezoneId], [RuleNo], [DateStart], [DateEnd], [DaylightTransitionStartIsFixedDateRule], [DaylightTransitionStartMonth], [DaylightTransitionStartDay], [DaylightTransitionStartWeek], [DaylightTransitionStartDayOfWeek], [DaylightTransitionStartTimeOfDay], [DaylightTransitionEndIsFixedDateRule], [DaylightTransitionEndMonth], [DaylightTransitionEndDay], [DaylightTransitionEndWeek], [DaylightTransitionEndDayOfWeek], [DaylightTransitionEndTimeOfDay], [DaylightDeltaSec]) VALUES (188, 47, 10, N'2015-01-01 00:00:00', N'2015-12-31 00:00:00', 0, 4, 1, 1, 4, N'23:59:59.999', 0, 10, 1, 5, 4, N'23:59:59.999', 3600)
INSERT INTO [DateTimeUtil].[TimezoneAdjustmentRule] ([Id], [TimezoneId], [RuleNo], [DateStart], [DateEnd], [DaylightTransitionStartIsFixedDateRule], [DaylightTransitionStartMonth], [DaylightTransitionStartDay], [DaylightTransitionStartWeek], [DaylightTransitionStartDayOfWeek], [DaylightTransitionStartTimeOfDay], [DaylightTransitionEndIsFixedDateRule], [DaylightTransitionEndMonth], [DaylightTransitionEndDay], [DaylightTransitionEndWeek], [DaylightTransitionEndDayOfWeek], [DaylightTransitionEndTimeOfDay], [DaylightDeltaSec]) VALUES (189, 47, 11, N'2016-01-01 00:00:00', N'2016-12-31 00:00:00', 0, 3, 1, 5, 4, N'23:59:59.999', 0, 10, 1, 5, 4, N'23:59:59.999', 3600)
INSERT INTO [DateTimeUtil].[TimezoneAdjustmentRule] ([Id], [TimezoneId], [RuleNo], [DateStart], [DateEnd], [DaylightTransitionStartIsFixedDateRule], [DaylightTransitionStartMonth], [DaylightTransitionStartDay], [DaylightTransitionStartWeek], [DaylightTransitionStartDayOfWeek], [DaylightTransitionStartTimeOfDay], [DaylightTransitionEndIsFixedDateRule], [DaylightTransitionEndMonth], [DaylightTransitionEndDay], [DaylightTransitionEndWeek], [DaylightTransitionEndDayOfWeek], [DaylightTransitionEndTimeOfDay], [DaylightDeltaSec]) VALUES (190, 47, 12, N'2017-01-01 00:00:00', N'9999-12-31 00:00:00', 0, 4, 1, 1, 4, N'23:59:59.999', 0, 10, 1, 5, 4, N'23:59:59.999', 3600)
INSERT INTO [DateTimeUtil].[TimezoneAdjustmentRule] ([Id], [TimezoneId], [RuleNo], [DateStart], [DateEnd], [DaylightTransitionStartIsFixedDateRule], [DaylightTransitionStartMonth], [DaylightTransitionStartDay], [DaylightTransitionStartWeek], [DaylightTransitionStartDayOfWeek], [DaylightTransitionStartTimeOfDay], [DaylightTransitionEndIsFixedDateRule], [DaylightTransitionEndMonth], [DaylightTransitionEndDay], [DaylightTransitionEndWeek], [DaylightTransitionEndDayOfWeek], [DaylightTransitionEndTimeOfDay], [DaylightDeltaSec]) VALUES (191, 49, 1, N'0001-01-01 00:00:00', N'9999-12-31 00:00:00', 0, 3, 1, 5, 0, N'03:00:00', 0, 10, 1, 5, 0, N'04:00:00', 3600)
INSERT INTO [DateTimeUtil].[TimezoneAdjustmentRule] ([Id], [TimezoneId], [RuleNo], [DateStart], [DateEnd], [DaylightTransitionStartIsFixedDateRule], [DaylightTransitionStartMonth], [DaylightTransitionStartDay], [DaylightTransitionStartWeek], [DaylightTransitionStartDayOfWeek], [DaylightTransitionStartTimeOfDay], [DaylightTransitionEndIsFixedDateRule], [DaylightTransitionEndMonth], [DaylightTransitionEndDay], [DaylightTransitionEndWeek], [DaylightTransitionEndDayOfWeek], [DaylightTransitionEndTimeOfDay], [DaylightDeltaSec]) VALUES (192, 50, 1, N'0001-01-01 00:00:00', N'2010-12-31 00:00:00', 0, 3, 1, 5, 0, N'03:00:00', 0, 10, 1, 5, 0, N'04:00:00', 3600)
INSERT INTO [DateTimeUtil].[TimezoneAdjustmentRule] ([Id], [TimezoneId], [RuleNo], [DateStart], [DateEnd], [DaylightTransitionStartIsFixedDateRule], [DaylightTransitionStartMonth], [DaylightTransitionStartDay], [DaylightTransitionStartWeek], [DaylightTransitionStartDayOfWeek], [DaylightTransitionStartTimeOfDay], [DaylightTransitionEndIsFixedDateRule], [DaylightTransitionEndMonth], [DaylightTransitionEndDay], [DaylightTransitionEndWeek], [DaylightTransitionEndDayOfWeek], [DaylightTransitionEndTimeOfDay], [DaylightDeltaSec]) VALUES (193, 50, 2, N'2011-01-01 00:00:00', N'2011-12-31 00:00:00', 0, 3, 1, 5, 1, N'03:00:00', 0, 10, 1, 5, 0, N'04:00:00', 3600)
INSERT INTO [DateTimeUtil].[TimezoneAdjustmentRule] ([Id], [TimezoneId], [RuleNo], [DateStart], [DateEnd], [DaylightTransitionStartIsFixedDateRule], [DaylightTransitionStartMonth], [DaylightTransitionStartDay], [DaylightTransitionStartWeek], [DaylightTransitionStartDayOfWeek], [DaylightTransitionStartTimeOfDay], [DaylightTransitionEndIsFixedDateRule], [DaylightTransitionEndMonth], [DaylightTransitionEndDay], [DaylightTransitionEndWeek], [DaylightTransitionEndDayOfWeek], [DaylightTransitionEndTimeOfDay], [DaylightDeltaSec]) VALUES (194, 50, 3, N'2012-01-01 00:00:00', N'9999-12-31 00:00:00', 0, 3, 1, 5, 0, N'03:00:00', 0, 10, 1, 5, 0, N'04:00:00', 3600)
INSERT INTO [DateTimeUtil].[TimezoneAdjustmentRule] ([Id], [TimezoneId], [RuleNo], [DateStart], [DateEnd], [DaylightTransitionStartIsFixedDateRule], [DaylightTransitionStartMonth], [DaylightTransitionStartDay], [DaylightTransitionStartWeek], [DaylightTransitionStartDayOfWeek], [DaylightTransitionStartTimeOfDay], [DaylightTransitionEndIsFixedDateRule], [DaylightTransitionEndMonth], [DaylightTransitionEndDay], [DaylightTransitionEndWeek], [DaylightTransitionEndDayOfWeek], [DaylightTransitionEndTimeOfDay], [DaylightDeltaSec]) VALUES (195, 51, 1, N'2005-01-01 00:00:00', N'2005-12-31 00:00:00', 0, 4, 1, 1, 5, N'02:00:00', 0, 10, 1, 2, 0, N'02:00:00', 3600)
INSERT INTO [DateTimeUtil].[TimezoneAdjustmentRule] ([Id], [TimezoneId], [RuleNo], [DateStart], [DateEnd], [DaylightTransitionStartIsFixedDateRule], [DaylightTransitionStartMonth], [DaylightTransitionStartDay], [DaylightTransitionStartWeek], [DaylightTransitionStartDayOfWeek], [DaylightTransitionStartTimeOfDay], [DaylightTransitionEndIsFixedDateRule], [DaylightTransitionEndMonth], [DaylightTransitionEndDay], [DaylightTransitionEndWeek], [DaylightTransitionEndDayOfWeek], [DaylightTransitionEndTimeOfDay], [DaylightDeltaSec]) VALUES (196, 51, 2, N'2006-01-01 00:00:00', N'2006-12-31 00:00:00', 0, 3, 1, 5, 5, N'02:00:00', 0, 10, 1, 1, 0, N'02:00:00', 3600)
INSERT INTO [DateTimeUtil].[TimezoneAdjustmentRule] ([Id], [TimezoneId], [RuleNo], [DateStart], [DateEnd], [DaylightTransitionStartIsFixedDateRule], [DaylightTransitionStartMonth], [DaylightTransitionStartDay], [DaylightTransitionStartWeek], [DaylightTransitionStartDayOfWeek], [DaylightTransitionStartTimeOfDay], [DaylightTransitionEndIsFixedDateRule], [DaylightTransitionEndMonth], [DaylightTransitionEndDay], [DaylightTransitionEndWeek], [DaylightTransitionEndDayOfWeek], [DaylightTransitionEndTimeOfDay], [DaylightDeltaSec]) VALUES (197, 51, 3, N'2007-01-01 00:00:00', N'2007-12-31 00:00:00', 0, 3, 1, 5, 5, N'02:00:00', 0, 9, 1, 3, 0, N'02:00:00', 3600)
INSERT INTO [DateTimeUtil].[TimezoneAdjustmentRule] ([Id], [TimezoneId], [RuleNo], [DateStart], [DateEnd], [DaylightTransitionStartIsFixedDateRule], [DaylightTransitionStartMonth], [DaylightTransitionStartDay], [DaylightTransitionStartWeek], [DaylightTransitionStartDayOfWeek], [DaylightTransitionStartTimeOfDay], [DaylightTransitionEndIsFixedDateRule], [DaylightTransitionEndMonth], [DaylightTransitionEndDay], [DaylightTransitionEndWeek], [DaylightTransitionEndDayOfWeek], [DaylightTransitionEndTimeOfDay], [DaylightDeltaSec]) VALUES (198, 51, 4, N'2008-01-01 00:00:00', N'2008-12-31 00:00:00', 0, 3, 1, 5, 5, N'02:00:00', 0, 10, 1, 1, 0, N'02:00:00', 3600)
INSERT INTO [DateTimeUtil].[TimezoneAdjustmentRule] ([Id], [TimezoneId], [RuleNo], [DateStart], [DateEnd], [DaylightTransitionStartIsFixedDateRule], [DaylightTransitionStartMonth], [DaylightTransitionStartDay], [DaylightTransitionStartWeek], [DaylightTransitionStartDayOfWeek], [DaylightTransitionStartTimeOfDay], [DaylightTransitionEndIsFixedDateRule], [DaylightTransitionEndMonth], [DaylightTransitionEndDay], [DaylightTransitionEndWeek], [DaylightTransitionEndDayOfWeek], [DaylightTransitionEndTimeOfDay], [DaylightDeltaSec]) VALUES (199, 51, 5, N'2009-01-01 00:00:00', N'2009-12-31 00:00:00', 0, 3, 1, 5, 5, N'02:00:00', 0, 9, 1, 5, 0, N'02:00:00', 3600)
INSERT INTO [DateTimeUtil].[TimezoneAdjustmentRule] ([Id], [TimezoneId], [RuleNo], [DateStart], [DateEnd], [DaylightTransitionStartIsFixedDateRule], [DaylightTransitionStartMonth], [DaylightTransitionStartDay], [DaylightTransitionStartWeek], [DaylightTransitionStartDayOfWeek], [DaylightTransitionStartTimeOfDay], [DaylightTransitionEndIsFixedDateRule], [DaylightTransitionEndMonth], [DaylightTransitionEndDay], [DaylightTransitionEndWeek], [DaylightTransitionEndDayOfWeek], [DaylightTransitionEndTimeOfDay], [DaylightDeltaSec]) VALUES (200, 51, 6, N'2010-01-01 00:00:00', N'2010-12-31 00:00:00', 0, 3, 1, 5, 5, N'02:00:00', 0, 9, 1, 2, 0, N'02:00:00', 3600)
INSERT INTO [DateTimeUtil].[TimezoneAdjustmentRule] ([Id], [TimezoneId], [RuleNo], [DateStart], [DateEnd], [DaylightTransitionStartIsFixedDateRule], [DaylightTransitionStartMonth], [DaylightTransitionStartDay], [DaylightTransitionStartWeek], [DaylightTransitionStartDayOfWeek], [DaylightTransitionStartTimeOfDay], [DaylightTransitionEndIsFixedDateRule], [DaylightTransitionEndMonth], [DaylightTransitionEndDay], [DaylightTransitionEndWeek], [DaylightTransitionEndDayOfWeek], [DaylightTransitionEndTimeOfDay], [DaylightDeltaSec]) VALUES (201, 51, 7, N'2011-01-01 00:00:00', N'2011-12-31 00:00:00', 0, 4, 1, 1, 5, N'02:00:00', 0, 10, 1, 1, 0, N'02:00:00', 3600)
INSERT INTO [DateTimeUtil].[TimezoneAdjustmentRule] ([Id], [TimezoneId], [RuleNo], [DateStart], [DateEnd], [DaylightTransitionStartIsFixedDateRule], [DaylightTransitionStartMonth], [DaylightTransitionStartDay], [DaylightTransitionStartWeek], [DaylightTransitionStartDayOfWeek], [DaylightTransitionStartTimeOfDay], [DaylightTransitionEndIsFixedDateRule], [DaylightTransitionEndMonth], [DaylightTransitionEndDay], [DaylightTransitionEndWeek], [DaylightTransitionEndDayOfWeek], [DaylightTransitionEndTimeOfDay], [DaylightDeltaSec]) VALUES (202, 51, 8, N'2012-01-01 00:00:00', N'2012-12-31 00:00:00', 0, 3, 1, 5, 5, N'02:00:00', 0, 9, 1, 4, 0, N'02:00:00', 3600)
INSERT INTO [DateTimeUtil].[TimezoneAdjustmentRule] ([Id], [TimezoneId], [RuleNo], [DateStart], [DateEnd], [DaylightTransitionStartIsFixedDateRule], [DaylightTransitionStartMonth], [DaylightTransitionStartDay], [DaylightTransitionStartWeek], [DaylightTransitionStartDayOfWeek], [DaylightTransitionStartTimeOfDay], [DaylightTransitionEndIsFixedDateRule], [DaylightTransitionEndMonth], [DaylightTransitionEndDay], [DaylightTransitionEndWeek], [DaylightTransitionEndDayOfWeek], [DaylightTransitionEndTimeOfDay], [DaylightDeltaSec]) VALUES (203, 51, 9, N'2013-01-01 00:00:00', N'2013-12-31 00:00:00', 0, 3, 1, 5, 5, N'02:00:00', 0, 10, 1, 5, 0, N'02:00:00', 3600)
INSERT INTO [DateTimeUtil].[TimezoneAdjustmentRule] ([Id], [TimezoneId], [RuleNo], [DateStart], [DateEnd], [DaylightTransitionStartIsFixedDateRule], [DaylightTransitionStartMonth], [DaylightTransitionStartDay], [DaylightTransitionStartWeek], [DaylightTransitionStartDayOfWeek], [DaylightTransitionStartTimeOfDay], [DaylightTransitionEndIsFixedDateRule], [DaylightTransitionEndMonth], [DaylightTransitionEndDay], [DaylightTransitionEndWeek], [DaylightTransitionEndDayOfWeek], [DaylightTransitionEndTimeOfDay], [DaylightDeltaSec]) VALUES (204, 51, 10, N'2014-01-01 00:00:00', N'2014-12-31 00:00:00', 0, 3, 1, 5, 5, N'02:00:00', 0, 10, 1, 5, 0, N'02:00:00', 3600)
INSERT INTO [DateTimeUtil].[TimezoneAdjustmentRule] ([Id], [TimezoneId], [RuleNo], [DateStart], [DateEnd], [DaylightTransitionStartIsFixedDateRule], [DaylightTransitionStartMonth], [DaylightTransitionStartDay], [DaylightTransitionStartWeek], [DaylightTransitionStartDayOfWeek], [DaylightTransitionStartTimeOfDay], [DaylightTransitionEndIsFixedDateRule], [DaylightTransitionEndMonth], [DaylightTransitionEndDay], [DaylightTransitionEndWeek], [DaylightTransitionEndDayOfWeek], [DaylightTransitionEndTimeOfDay], [DaylightDeltaSec]) VALUES (205, 51, 11, N'2015-01-01 00:00:00', N'2015-12-31 00:00:00', 0, 3, 1, 5, 5, N'02:00:00', 0, 10, 1, 5, 0, N'02:00:00', 3600)
INSERT INTO [DateTimeUtil].[TimezoneAdjustmentRule] ([Id], [TimezoneId], [RuleNo], [DateStart], [DateEnd], [DaylightTransitionStartIsFixedDateRule], [DaylightTransitionStartMonth], [DaylightTransitionStartDay], [DaylightTransitionStartWeek], [DaylightTransitionStartDayOfWeek], [DaylightTransitionStartTimeOfDay], [DaylightTransitionEndIsFixedDateRule], [DaylightTransitionEndMonth], [DaylightTransitionEndDay], [DaylightTransitionEndWeek], [DaylightTransitionEndDayOfWeek], [DaylightTransitionEndTimeOfDay], [DaylightDeltaSec]) VALUES (206, 51, 12, N'2016-01-01 00:00:00', N'2016-12-31 00:00:00', 0, 3, 1, 5, 5, N'02:00:00', 0, 10, 1, 5, 0, N'02:00:00', 3600)
INSERT INTO [DateTimeUtil].[TimezoneAdjustmentRule] ([Id], [TimezoneId], [RuleNo], [DateStart], [DateEnd], [DaylightTransitionStartIsFixedDateRule], [DaylightTransitionStartMonth], [DaylightTransitionStartDay], [DaylightTransitionStartWeek], [DaylightTransitionStartDayOfWeek], [DaylightTransitionStartTimeOfDay], [DaylightTransitionEndIsFixedDateRule], [DaylightTransitionEndMonth], [DaylightTransitionEndDay], [DaylightTransitionEndWeek], [DaylightTransitionEndDayOfWeek], [DaylightTransitionEndTimeOfDay], [DaylightDeltaSec]) VALUES (207, 51, 13, N'2017-01-01 00:00:00', N'2017-12-31 00:00:00', 0, 3, 1, 4, 5, N'02:00:00', 0, 10, 1, 5, 0, N'02:00:00', 3600)
INSERT INTO [DateTimeUtil].[TimezoneAdjustmentRule] ([Id], [TimezoneId], [RuleNo], [DateStart], [DateEnd], [DaylightTransitionStartIsFixedDateRule], [DaylightTransitionStartMonth], [DaylightTransitionStartDay], [DaylightTransitionStartWeek], [DaylightTransitionStartDayOfWeek], [DaylightTransitionStartTimeOfDay], [DaylightTransitionEndIsFixedDateRule], [DaylightTransitionEndMonth], [DaylightTransitionEndDay], [DaylightTransitionEndWeek], [DaylightTransitionEndDayOfWeek], [DaylightTransitionEndTimeOfDay], [DaylightDeltaSec]) VALUES (208, 51, 14, N'2018-01-01 00:00:00', N'2018-12-31 00:00:00', 0, 3, 1, 4, 5, N'02:00:00', 0, 10, 1, 5, 0, N'02:00:00', 3600)
INSERT INTO [DateTimeUtil].[TimezoneAdjustmentRule] ([Id], [TimezoneId], [RuleNo], [DateStart], [DateEnd], [DaylightTransitionStartIsFixedDateRule], [DaylightTransitionStartMonth], [DaylightTransitionStartDay], [DaylightTransitionStartWeek], [DaylightTransitionStartDayOfWeek], [DaylightTransitionStartTimeOfDay], [DaylightTransitionEndIsFixedDateRule], [DaylightTransitionEndMonth], [DaylightTransitionEndDay], [DaylightTransitionEndWeek], [DaylightTransitionEndDayOfWeek], [DaylightTransitionEndTimeOfDay], [DaylightDeltaSec]) VALUES (209, 51, 15, N'2019-01-01 00:00:00', N'2019-12-31 00:00:00', 0, 3, 1, 5, 5, N'02:00:00', 0, 10, 1, 5, 0, N'02:00:00', 3600)
INSERT INTO [DateTimeUtil].[TimezoneAdjustmentRule] ([Id], [TimezoneId], [RuleNo], [DateStart], [DateEnd], [DaylightTransitionStartIsFixedDateRule], [DaylightTransitionStartMonth], [DaylightTransitionStartDay], [DaylightTransitionStartWeek], [DaylightTransitionStartDayOfWeek], [DaylightTransitionStartTimeOfDay], [DaylightTransitionEndIsFixedDateRule], [DaylightTransitionEndMonth], [DaylightTransitionEndDay], [DaylightTransitionEndWeek], [DaylightTransitionEndDayOfWeek], [DaylightTransitionEndTimeOfDay], [DaylightDeltaSec]) VALUES (210, 51, 16, N'2020-01-01 00:00:00', N'2020-12-31 00:00:00', 0, 3, 1, 5, 5, N'02:00:00', 0, 10, 1, 5, 0, N'02:00:00', 3600)
INSERT INTO [DateTimeUtil].[TimezoneAdjustmentRule] ([Id], [TimezoneId], [RuleNo], [DateStart], [DateEnd], [DaylightTransitionStartIsFixedDateRule], [DaylightTransitionStartMonth], [DaylightTransitionStartDay], [DaylightTransitionStartWeek], [DaylightTransitionStartDayOfWeek], [DaylightTransitionStartTimeOfDay], [DaylightTransitionEndIsFixedDateRule], [DaylightTransitionEndMonth], [DaylightTransitionEndDay], [DaylightTransitionEndWeek], [DaylightTransitionEndDayOfWeek], [DaylightTransitionEndTimeOfDay], [DaylightDeltaSec]) VALUES (211, 51, 17, N'2021-01-01 00:00:00', N'2021-12-31 00:00:00', 0, 3, 1, 5, 5, N'02:00:00', 0, 10, 1, 5, 0, N'02:00:00', 3600)
INSERT INTO [DateTimeUtil].[TimezoneAdjustmentRule] ([Id], [TimezoneId], [RuleNo], [DateStart], [DateEnd], [DaylightTransitionStartIsFixedDateRule], [DaylightTransitionStartMonth], [DaylightTransitionStartDay], [DaylightTransitionStartWeek], [DaylightTransitionStartDayOfWeek], [DaylightTransitionStartTimeOfDay], [DaylightTransitionEndIsFixedDateRule], [DaylightTransitionEndMonth], [DaylightTransitionEndDay], [DaylightTransitionEndWeek], [DaylightTransitionEndDayOfWeek], [DaylightTransitionEndTimeOfDay], [DaylightDeltaSec]) VALUES (212, 51, 18, N'2022-01-01 00:00:00', N'2022-12-31 00:00:00', 0, 3, 1, 5, 5, N'02:00:00', 0, 10, 1, 5, 0, N'02:00:00', 3600)
INSERT INTO [DateTimeUtil].[TimezoneAdjustmentRule] ([Id], [TimezoneId], [RuleNo], [DateStart], [DateEnd], [DaylightTransitionStartIsFixedDateRule], [DaylightTransitionStartMonth], [DaylightTransitionStartDay], [DaylightTransitionStartWeek], [DaylightTransitionStartDayOfWeek], [DaylightTransitionStartTimeOfDay], [DaylightTransitionEndIsFixedDateRule], [DaylightTransitionEndMonth], [DaylightTransitionEndDay], [DaylightTransitionEndWeek], [DaylightTransitionEndDayOfWeek], [DaylightTransitionEndTimeOfDay], [DaylightDeltaSec]) VALUES (213, 51, 19, N'2023-01-01 00:00:00', N'9999-12-31 00:00:00', 0, 3, 1, 4, 5, N'02:00:00', 0, 10, 1, 5, 0, N'02:00:00', 3600)
INSERT INTO [DateTimeUtil].[TimezoneAdjustmentRule] ([Id], [TimezoneId], [RuleNo], [DateStart], [DateEnd], [DaylightTransitionStartIsFixedDateRule], [DaylightTransitionStartMonth], [DaylightTransitionStartDay], [DaylightTransitionStartWeek], [DaylightTransitionStartDayOfWeek], [DaylightTransitionStartTimeOfDay], [DaylightTransitionEndIsFixedDateRule], [DaylightTransitionEndMonth], [DaylightTransitionEndDay], [DaylightTransitionEndWeek], [DaylightTransitionEndDayOfWeek], [DaylightTransitionEndTimeOfDay], [DaylightDeltaSec]) VALUES (214, 52, 1, N'0001-01-01 00:00:00', N'2005-12-31 00:00:00', 0, 4, 1, 5, 5, N'00:00:00', 0, 9, 1, 5, 4, N'23:59:59.999', 3600)
INSERT INTO [DateTimeUtil].[TimezoneAdjustmentRule] ([Id], [TimezoneId], [RuleNo], [DateStart], [DateEnd], [DaylightTransitionStartIsFixedDateRule], [DaylightTransitionStartMonth], [DaylightTransitionStartDay], [DaylightTransitionStartWeek], [DaylightTransitionStartDayOfWeek], [DaylightTransitionStartTimeOfDay], [DaylightTransitionEndIsFixedDateRule], [DaylightTransitionEndMonth], [DaylightTransitionEndDay], [DaylightTransitionEndWeek], [DaylightTransitionEndDayOfWeek], [DaylightTransitionEndTimeOfDay], [DaylightDeltaSec]) VALUES (215, 52, 2, N'2006-01-01 00:00:00', N'2006-12-31 00:00:00', 0, 4, 1, 5, 5, N'00:00:00', 0, 9, 1, 3, 4, N'23:59:59.999', 3600)
INSERT INTO [DateTimeUtil].[TimezoneAdjustmentRule] ([Id], [TimezoneId], [RuleNo], [DateStart], [DateEnd], [DaylightTransitionStartIsFixedDateRule], [DaylightTransitionStartMonth], [DaylightTransitionStartDay], [DaylightTransitionStartWeek], [DaylightTransitionStartDayOfWeek], [DaylightTransitionStartTimeOfDay], [DaylightTransitionEndIsFixedDateRule], [DaylightTransitionEndMonth], [DaylightTransitionEndDay], [DaylightTransitionEndWeek], [DaylightTransitionEndDayOfWeek], [DaylightTransitionEndTimeOfDay], [DaylightDeltaSec]) VALUES (216, 52, 3, N'2007-01-01 00:00:00', N'2007-12-31 00:00:00', 0, 4, 1, 5, 4, N'23:59:59.999', 0, 9, 1, 1, 4, N'23:59:59.999', 3600)
INSERT INTO [DateTimeUtil].[TimezoneAdjustmentRule] ([Id], [TimezoneId], [RuleNo], [DateStart], [DateEnd], [DaylightTransitionStartIsFixedDateRule], [DaylightTransitionStartMonth], [DaylightTransitionStartDay], [DaylightTransitionStartWeek], [DaylightTransitionStartDayOfWeek], [DaylightTransitionStartTimeOfDay], [DaylightTransitionEndIsFixedDateRule], [DaylightTransitionEndMonth], [DaylightTransitionEndDay], [DaylightTransitionEndWeek], [DaylightTransitionEndDayOfWeek], [DaylightTransitionEndTimeOfDay], [DaylightDeltaSec]) VALUES (217, 52, 4, N'2008-01-01 00:00:00', N'2008-12-31 00:00:00', 0, 4, 1, 5, 4, N'23:59:59.999', 0, 8, 1, 5, 4, N'23:59:59.999', 3600)
INSERT INTO [DateTimeUtil].[TimezoneAdjustmentRule] ([Id], [TimezoneId], [RuleNo], [DateStart], [DateEnd], [DaylightTransitionStartIsFixedDateRule], [DaylightTransitionStartMonth], [DaylightTransitionStartDay], [DaylightTransitionStartWeek], [DaylightTransitionStartDayOfWeek], [DaylightTransitionStartTimeOfDay], [DaylightTransitionEndIsFixedDateRule], [DaylightTransitionEndMonth], [DaylightTransitionEndDay], [DaylightTransitionEndWeek], [DaylightTransitionEndDayOfWeek], [DaylightTransitionEndTimeOfDay], [DaylightDeltaSec]) VALUES (218, 52, 5, N'2009-01-01 00:00:00', N'2009-12-31 00:00:00', 0, 4, 1, 4, 4, N'23:59:59.999', 0, 8, 1, 3, 4, N'23:59:59.999', 3600)
INSERT INTO [DateTimeUtil].[TimezoneAdjustmentRule] ([Id], [TimezoneId], [RuleNo], [DateStart], [DateEnd], [DaylightTransitionStartIsFixedDateRule], [DaylightTransitionStartMonth], [DaylightTransitionStartDay], [DaylightTransitionStartWeek], [DaylightTransitionStartDayOfWeek], [DaylightTransitionStartTimeOfDay], [DaylightTransitionEndIsFixedDateRule], [DaylightTransitionEndMonth], [DaylightTransitionEndDay], [DaylightTransitionEndWeek], [DaylightTransitionEndDayOfWeek], [DaylightTransitionEndTimeOfDay], [DaylightDeltaSec]) VALUES (219, 52, 6, N'2010-01-01 00:00:00', N'2010-12-31 00:00:00', 0, 4, 1, 5, 5, N'00:00:00', 0, 8, 1, 2, 3, N'00:00:00', 3600)
INSERT INTO [DateTimeUtil].[TimezoneAdjustmentRule] ([Id], [TimezoneId], [RuleNo], [DateStart], [DateEnd], [DaylightTransitionStartIsFixedDateRule], [DaylightTransitionStartMonth], [DaylightTransitionStartDay], [DaylightTransitionStartWeek], [DaylightTransitionStartDayOfWeek], [DaylightTransitionStartTimeOfDay], [DaylightTransitionEndIsFixedDateRule], [DaylightTransitionEndMonth], [DaylightTransitionEndDay], [DaylightTransitionEndWeek], [DaylightTransitionEndDayOfWeek], [DaylightTransitionEndTimeOfDay], [DaylightDeltaSec]) VALUES (220, 53, 1, N'0001-01-01 00:00:00', N'9999-12-31 00:00:00', 0, 3, 1, 5, 0, N'02:00:00', 0, 10, 1, 5, 0, N'03:00:00', 3600)
INSERT INTO [DateTimeUtil].[TimezoneAdjustmentRule] ([Id], [TimezoneId], [RuleNo], [DateStart], [DateEnd], [DaylightTransitionStartIsFixedDateRule], [DaylightTransitionStartMonth], [DaylightTransitionStartDay], [DaylightTransitionStartWeek], [DaylightTransitionStartDayOfWeek], [DaylightTransitionStartTimeOfDay], [DaylightTransitionEndIsFixedDateRule], [DaylightTransitionEndMonth], [DaylightTransitionEndDay], [DaylightTransitionEndWeek], [DaylightTransitionEndDayOfWeek], [DaylightTransitionEndTimeOfDay], [DaylightDeltaSec]) VALUES (221, 54, 1, N'2012-01-01 00:00:00', N'2012-12-31 00:00:00', 0, 1, 1, 1, 0, N'00:00:00', 0, 11, 1, 2, 6, N'02:00:00', 3600)
INSERT INTO [DateTimeUtil].[TimezoneAdjustmentRule] ([Id], [TimezoneId], [RuleNo], [DateStart], [DateEnd], [DaylightTransitionStartIsFixedDateRule], [DaylightTransitionStartMonth], [DaylightTransitionStartDay], [DaylightTransitionStartWeek], [DaylightTransitionStartDayOfWeek], [DaylightTransitionStartTimeOfDay], [DaylightTransitionEndIsFixedDateRule], [DaylightTransitionEndMonth], [DaylightTransitionEndDay], [DaylightTransitionEndWeek], [DaylightTransitionEndDayOfWeek], [DaylightTransitionEndTimeOfDay], [DaylightDeltaSec]) VALUES (222, 54, 2, N'2013-01-01 00:00:00', N'2013-12-31 00:00:00', 0, 3, 1, 5, 5, N'01:00:00', 0, 1, 1, 1, 2, N'00:00:00', 3600)
INSERT INTO [DateTimeUtil].[TimezoneAdjustmentRule] ([Id], [TimezoneId], [RuleNo], [DateStart], [DateEnd], [DaylightTransitionStartIsFixedDateRule], [DaylightTransitionStartMonth], [DaylightTransitionStartDay], [DaylightTransitionStartWeek], [DaylightTransitionStartDayOfWeek], [DaylightTransitionStartTimeOfDay], [DaylightTransitionEndIsFixedDateRule], [DaylightTransitionEndMonth], [DaylightTransitionEndDay], [DaylightTransitionEndWeek], [DaylightTransitionEndDayOfWeek], [DaylightTransitionEndTimeOfDay], [DaylightDeltaSec]) VALUES (223, 55, 1, N'0001-01-01 00:00:00', N'2006-12-31 00:00:00', 0, 4, 1, 1, 0, N'03:00:00', 0, 10, 1, 1, 0, N'04:00:00', 3600)
INSERT INTO [DateTimeUtil].[TimezoneAdjustmentRule] ([Id], [TimezoneId], [RuleNo], [DateStart], [DateEnd], [DaylightTransitionStartIsFixedDateRule], [DaylightTransitionStartMonth], [DaylightTransitionStartDay], [DaylightTransitionStartWeek], [DaylightTransitionStartDayOfWeek], [DaylightTransitionStartTimeOfDay], [DaylightTransitionEndIsFixedDateRule], [DaylightTransitionEndMonth], [DaylightTransitionEndDay], [DaylightTransitionEndWeek], [DaylightTransitionEndDayOfWeek], [DaylightTransitionEndTimeOfDay], [DaylightDeltaSec]) VALUES (224, 55, 2, N'2007-01-01 00:00:00', N'2007-12-31 00:00:00', 0, 4, 1, 1, 0, N'03:00:00', 0, 10, 1, 1, 1, N'04:00:00', 3600)
INSERT INTO [DateTimeUtil].[TimezoneAdjustmentRule] ([Id], [TimezoneId], [RuleNo], [DateStart], [DateEnd], [DaylightTransitionStartIsFixedDateRule], [DaylightTransitionStartMonth], [DaylightTransitionStartDay], [DaylightTransitionStartWeek], [DaylightTransitionStartDayOfWeek], [DaylightTransitionStartTimeOfDay], [DaylightTransitionEndIsFixedDateRule], [DaylightTransitionEndMonth], [DaylightTransitionEndDay], [DaylightTransitionEndWeek], [DaylightTransitionEndDayOfWeek], [DaylightTransitionEndTimeOfDay], [DaylightDeltaSec]) VALUES (225, 56, 1, N'0001-01-01 00:00:00', N'2010-12-31 00:00:00', 0, 3, 1, 5, 0, N'02:00:00', 0, 10, 1, 5, 0, N'03:00:00', 3600)
INSERT INTO [DateTimeUtil].[TimezoneAdjustmentRule] ([Id], [TimezoneId], [RuleNo], [DateStart], [DateEnd], [DaylightTransitionStartIsFixedDateRule], [DaylightTransitionStartMonth], [DaylightTransitionStartDay], [DaylightTransitionStartWeek], [DaylightTransitionStartDayOfWeek], [DaylightTransitionStartTimeOfDay], [DaylightTransitionEndIsFixedDateRule], [DaylightTransitionEndMonth], [DaylightTransitionEndDay], [DaylightTransitionEndWeek], [DaylightTransitionEndDayOfWeek], [DaylightTransitionEndTimeOfDay], [DaylightDeltaSec]) VALUES (226, 56, 2, N'2011-01-01 00:00:00', N'2011-12-31 00:00:00', 0, 3, 1, 5, 0, N'02:00:00', 0, 1, 1, 1, 6, N'00:00:00', 3600)
INSERT INTO [DateTimeUtil].[TimezoneAdjustmentRule] ([Id], [TimezoneId], [RuleNo], [DateStart], [DateEnd], [DaylightTransitionStartIsFixedDateRule], [DaylightTransitionStartMonth], [DaylightTransitionStartDay], [DaylightTransitionStartWeek], [DaylightTransitionStartDayOfWeek], [DaylightTransitionStartTimeOfDay], [DaylightTransitionEndIsFixedDateRule], [DaylightTransitionEndMonth], [DaylightTransitionEndDay], [DaylightTransitionEndWeek], [DaylightTransitionEndDayOfWeek], [DaylightTransitionEndTimeOfDay], [DaylightDeltaSec]) VALUES (227, 59, 1, N'0001-01-01 00:00:00', N'2005-12-31 00:00:00', 0, 3, 1, 1, 0, N'02:00:00', 0, 9, 1, 4, 2, N'02:00:00', 3600)
INSERT INTO [DateTimeUtil].[TimezoneAdjustmentRule] ([Id], [TimezoneId], [RuleNo], [DateStart], [DateEnd], [DaylightTransitionStartIsFixedDateRule], [DaylightTransitionStartMonth], [DaylightTransitionStartDay], [DaylightTransitionStartWeek], [DaylightTransitionStartDayOfWeek], [DaylightTransitionStartTimeOfDay], [DaylightTransitionEndIsFixedDateRule], [DaylightTransitionEndMonth], [DaylightTransitionEndDay], [DaylightTransitionEndWeek], [DaylightTransitionEndDayOfWeek], [DaylightTransitionEndTimeOfDay], [DaylightDeltaSec]) VALUES (228, 59, 2, N'2008-01-01 00:00:00', N'2008-12-31 00:00:00', 0, 3, 1, 3, 4, N'23:59:59.999', 0, 9, 1, 3, 6, N'23:59:59.999', 3600)
INSERT INTO [DateTimeUtil].[TimezoneAdjustmentRule] ([Id], [TimezoneId], [RuleNo], [DateStart], [DateEnd], [DaylightTransitionStartIsFixedDateRule], [DaylightTransitionStartMonth], [DaylightTransitionStartDay], [DaylightTransitionStartWeek], [DaylightTransitionStartDayOfWeek], [DaylightTransitionStartTimeOfDay], [DaylightTransitionEndIsFixedDateRule], [DaylightTransitionEndMonth], [DaylightTransitionEndDay], [DaylightTransitionEndWeek], [DaylightTransitionEndDayOfWeek], [DaylightTransitionEndTimeOfDay], [DaylightDeltaSec]) VALUES (229, 59, 3, N'2009-01-01 00:00:00', N'9999-12-31 00:00:00', 0, 3, 1, 3, 6, N'23:59:59.999', 0, 9, 1, 3, 1, N'23:59:59.999', 3600)
INSERT INTO [DateTimeUtil].[TimezoneAdjustmentRule] ([Id], [TimezoneId], [RuleNo], [DateStart], [DateEnd], [DaylightTransitionStartIsFixedDateRule], [DaylightTransitionStartMonth], [DaylightTransitionStartDay], [DaylightTransitionStartWeek], [DaylightTransitionStartDayOfWeek], [DaylightTransitionStartTimeOfDay], [DaylightTransitionEndIsFixedDateRule], [DaylightTransitionEndMonth], [DaylightTransitionEndDay], [DaylightTransitionEndWeek], [DaylightTransitionEndDayOfWeek], [DaylightTransitionEndTimeOfDay], [DaylightDeltaSec]) VALUES (230, 61, 1, N'0001-01-01 00:00:00', N'9999-12-31 00:00:00', 0, 3, 1, 5, 0, N'04:00:00', 0, 10, 1, 5, 0, N'05:00:00', 3600)
INSERT INTO [DateTimeUtil].[TimezoneAdjustmentRule] ([Id], [TimezoneId], [RuleNo], [DateStart], [DateEnd], [DaylightTransitionStartIsFixedDateRule], [DaylightTransitionStartMonth], [DaylightTransitionStartDay], [DaylightTransitionStartWeek], [DaylightTransitionStartDayOfWeek], [DaylightTransitionStartTimeOfDay], [DaylightTransitionEndIsFixedDateRule], [DaylightTransitionEndMonth], [DaylightTransitionEndDay], [DaylightTransitionEndWeek], [DaylightTransitionEndDayOfWeek], [DaylightTransitionEndTimeOfDay], [DaylightDeltaSec]) VALUES (231, 62, 1, N'0001-01-01 00:00:00', N'2011-12-31 00:00:00', 0, 3, 1, 5, 0, N'02:00:00', 0, 10, 1, 5, 0, N'03:00:00', 3600)
INSERT INTO [DateTimeUtil].[TimezoneAdjustmentRule] ([Id], [TimezoneId], [RuleNo], [DateStart], [DateEnd], [DaylightTransitionStartIsFixedDateRule], [DaylightTransitionStartMonth], [DaylightTransitionStartDay], [DaylightTransitionStartWeek], [DaylightTransitionStartDayOfWeek], [DaylightTransitionStartTimeOfDay], [DaylightTransitionEndIsFixedDateRule], [DaylightTransitionEndMonth], [DaylightTransitionEndDay], [DaylightTransitionEndWeek], [DaylightTransitionEndDayOfWeek], [DaylightTransitionEndTimeOfDay], [DaylightDeltaSec]) VALUES (232, 63, 1, N'0001-01-01 00:00:00', N'2010-12-31 00:00:00', 0, 3, 1, 5, 0, N'02:00:00', 0, 10, 1, 5, 0, N'03:00:00', 3600)
INSERT INTO [DateTimeUtil].[TimezoneAdjustmentRule] ([Id], [TimezoneId], [RuleNo], [DateStart], [DateEnd], [DaylightTransitionStartIsFixedDateRule], [DaylightTransitionStartMonth], [DaylightTransitionStartDay], [DaylightTransitionStartWeek], [DaylightTransitionStartDayOfWeek], [DaylightTransitionStartTimeOfDay], [DaylightTransitionEndIsFixedDateRule], [DaylightTransitionEndMonth], [DaylightTransitionEndDay], [DaylightTransitionEndWeek], [DaylightTransitionEndDayOfWeek], [DaylightTransitionEndTimeOfDay], [DaylightDeltaSec]) VALUES (233, 63, 2, N'2011-01-01 00:00:00', N'2011-12-31 00:00:00', 0, 3, 1, 5, 0, N'02:00:00', 0, 1, 1, 1, 6, N'00:00:00', 3600)
INSERT INTO [DateTimeUtil].[TimezoneAdjustmentRule] ([Id], [TimezoneId], [RuleNo], [DateStart], [DateEnd], [DaylightTransitionStartIsFixedDateRule], [DaylightTransitionStartMonth], [DaylightTransitionStartDay], [DaylightTransitionStartWeek], [DaylightTransitionStartDayOfWeek], [DaylightTransitionStartTimeOfDay], [DaylightTransitionEndIsFixedDateRule], [DaylightTransitionEndMonth], [DaylightTransitionEndDay], [DaylightTransitionEndWeek], [DaylightTransitionEndDayOfWeek], [DaylightTransitionEndTimeOfDay], [DaylightDeltaSec]) VALUES (234, 64, 1, N'2008-01-01 00:00:00', N'2008-12-31 00:00:00', 0, 10, 1, 5, 0, N'02:00:00', 0, 1, 1, 1, 2, N'00:00:00', 3600)
INSERT INTO [DateTimeUtil].[TimezoneAdjustmentRule] ([Id], [TimezoneId], [RuleNo], [DateStart], [DateEnd], [DaylightTransitionStartIsFixedDateRule], [DaylightTransitionStartMonth], [DaylightTransitionStartDay], [DaylightTransitionStartWeek], [DaylightTransitionStartDayOfWeek], [DaylightTransitionStartTimeOfDay], [DaylightTransitionEndIsFixedDateRule], [DaylightTransitionEndMonth], [DaylightTransitionEndDay], [DaylightTransitionEndWeek], [DaylightTransitionEndDayOfWeek], [DaylightTransitionEndTimeOfDay], [DaylightDeltaSec]) VALUES (235, 64, 2, N'2009-01-01 00:00:00', N'2009-12-31 00:00:00', 0, 1, 1, 1, 4, N'00:00:00', 0, 3, 1, 5, 0, N'02:00:00', 3600)
INSERT INTO [DateTimeUtil].[TimezoneAdjustmentRule] ([Id], [TimezoneId], [RuleNo], [DateStart], [DateEnd], [DaylightTransitionStartIsFixedDateRule], [DaylightTransitionStartMonth], [DaylightTransitionStartDay], [DaylightTransitionStartWeek], [DaylightTransitionStartDayOfWeek], [DaylightTransitionStartTimeOfDay], [DaylightTransitionEndIsFixedDateRule], [DaylightTransitionEndMonth], [DaylightTransitionEndDay], [DaylightTransitionEndWeek], [DaylightTransitionEndDayOfWeek], [DaylightTransitionEndTimeOfDay], [DaylightDeltaSec]) VALUES (236, 68, 1, N'2008-01-01 00:00:00', N'2008-12-31 00:00:00', 0, 5, 1, 5, 6, N'23:59:59.999', 0, 10, 1, 5, 5, N'23:59:59.999', 3600)
INSERT INTO [DateTimeUtil].[TimezoneAdjustmentRule] ([Id], [TimezoneId], [RuleNo], [DateStart], [DateEnd], [DaylightTransitionStartIsFixedDateRule], [DaylightTransitionStartMonth], [DaylightTransitionStartDay], [DaylightTransitionStartWeek], [DaylightTransitionStartDayOfWeek], [DaylightTransitionStartTimeOfDay], [DaylightTransitionEndIsFixedDateRule], [DaylightTransitionEndMonth], [DaylightTransitionEndDay], [DaylightTransitionEndWeek], [DaylightTransitionEndDayOfWeek], [DaylightTransitionEndTimeOfDay], [DaylightDeltaSec]) VALUES (237, 68, 2, N'2009-01-01 00:00:00', N'2009-12-31 00:00:00', 0, 4, 1, 2, 2, N'23:59:59.999', 0, 10, 1, 5, 6, N'23:59:59.999', 3600)
INSERT INTO [DateTimeUtil].[TimezoneAdjustmentRule] ([Id], [TimezoneId], [RuleNo], [DateStart], [DateEnd], [DaylightTransitionStartIsFixedDateRule], [DaylightTransitionStartMonth], [DaylightTransitionStartDay], [DaylightTransitionStartWeek], [DaylightTransitionStartDayOfWeek], [DaylightTransitionStartTimeOfDay], [DaylightTransitionEndIsFixedDateRule], [DaylightTransitionEndMonth], [DaylightTransitionEndDay], [DaylightTransitionEndWeek], [DaylightTransitionEndDayOfWeek], [DaylightTransitionEndTimeOfDay], [DaylightDeltaSec]) VALUES (238, 73, 1, N'2009-01-01 00:00:00', N'2009-12-31 00:00:00', 0, 6, 1, 3, 5, N'23:00:00', 0, 12, 1, 5, 4, N'23:59:00', 3600)
INSERT INTO [DateTimeUtil].[TimezoneAdjustmentRule] ([Id], [TimezoneId], [RuleNo], [DateStart], [DateEnd], [DaylightTransitionStartIsFixedDateRule], [DaylightTransitionStartMonth], [DaylightTransitionStartDay], [DaylightTransitionStartWeek], [DaylightTransitionStartDayOfWeek], [DaylightTransitionStartTimeOfDay], [DaylightTransitionEndIsFixedDateRule], [DaylightTransitionEndMonth], [DaylightTransitionEndDay], [DaylightTransitionEndWeek], [DaylightTransitionEndDayOfWeek], [DaylightTransitionEndTimeOfDay], [DaylightDeltaSec]) VALUES (239, 74, 1, N'0001-01-01 00:00:00', N'2010-12-31 00:00:00', 0, 3, 1, 5, 0, N'02:00:00', 0, 10, 1, 5, 0, N'03:00:00', 3600)
INSERT INTO [DateTimeUtil].[TimezoneAdjustmentRule] ([Id], [TimezoneId], [RuleNo], [DateStart], [DateEnd], [DaylightTransitionStartIsFixedDateRule], [DaylightTransitionStartMonth], [DaylightTransitionStartDay], [DaylightTransitionStartWeek], [DaylightTransitionStartDayOfWeek], [DaylightTransitionStartTimeOfDay], [DaylightTransitionEndIsFixedDateRule], [DaylightTransitionEndMonth], [DaylightTransitionEndDay], [DaylightTransitionEndWeek], [DaylightTransitionEndDayOfWeek], [DaylightTransitionEndTimeOfDay], [DaylightDeltaSec]) VALUES (240, 74, 2, N'2011-01-01 00:00:00', N'2011-12-31 00:00:00', 0, 3, 1, 5, 0, N'02:00:00', 0, 1, 1, 1, 6, N'00:00:00', 3600)
INSERT INTO [DateTimeUtil].[TimezoneAdjustmentRule] ([Id], [TimezoneId], [RuleNo], [DateStart], [DateEnd], [DaylightTransitionStartIsFixedDateRule], [DaylightTransitionStartMonth], [DaylightTransitionStartDay], [DaylightTransitionStartWeek], [DaylightTransitionStartDayOfWeek], [DaylightTransitionStartTimeOfDay], [DaylightTransitionEndIsFixedDateRule], [DaylightTransitionEndMonth], [DaylightTransitionEndDay], [DaylightTransitionEndWeek], [DaylightTransitionEndDayOfWeek], [DaylightTransitionEndTimeOfDay], [DaylightDeltaSec]) VALUES (241, 77, 1, N'0001-01-01 00:00:00', N'2010-12-31 00:00:00', 0, 3, 1, 5, 0, N'02:00:00', 0, 10, 1, 5, 0, N'03:00:00', 3600)
INSERT INTO [DateTimeUtil].[TimezoneAdjustmentRule] ([Id], [TimezoneId], [RuleNo], [DateStart], [DateEnd], [DaylightTransitionStartIsFixedDateRule], [DaylightTransitionStartMonth], [DaylightTransitionStartDay], [DaylightTransitionStartWeek], [DaylightTransitionStartDayOfWeek], [DaylightTransitionStartTimeOfDay], [DaylightTransitionEndIsFixedDateRule], [DaylightTransitionEndMonth], [DaylightTransitionEndDay], [DaylightTransitionEndWeek], [DaylightTransitionEndDayOfWeek], [DaylightTransitionEndTimeOfDay], [DaylightDeltaSec]) VALUES (242, 77, 2, N'2011-01-01 00:00:00', N'2011-12-31 00:00:00', 0, 3, 1, 5, 0, N'02:00:00', 0, 1, 1, 1, 6, N'00:00:00', 3600)
INSERT INTO [DateTimeUtil].[TimezoneAdjustmentRule] ([Id], [TimezoneId], [RuleNo], [DateStart], [DateEnd], [DaylightTransitionStartIsFixedDateRule], [DaylightTransitionStartMonth], [DaylightTransitionStartDay], [DaylightTransitionStartWeek], [DaylightTransitionStartDayOfWeek], [DaylightTransitionStartTimeOfDay], [DaylightTransitionEndIsFixedDateRule], [DaylightTransitionEndMonth], [DaylightTransitionEndDay], [DaylightTransitionEndWeek], [DaylightTransitionEndDayOfWeek], [DaylightTransitionEndTimeOfDay], [DaylightDeltaSec]) VALUES (243, 78, 1, N'0001-01-01 00:00:00', N'2010-12-31 00:00:00', 0, 3, 1, 5, 0, N'02:00:00', 0, 10, 1, 5, 0, N'03:00:00', 3600)
INSERT INTO [DateTimeUtil].[TimezoneAdjustmentRule] ([Id], [TimezoneId], [RuleNo], [DateStart], [DateEnd], [DaylightTransitionStartIsFixedDateRule], [DaylightTransitionStartMonth], [DaylightTransitionStartDay], [DaylightTransitionStartWeek], [DaylightTransitionStartDayOfWeek], [DaylightTransitionStartTimeOfDay], [DaylightTransitionEndIsFixedDateRule], [DaylightTransitionEndMonth], [DaylightTransitionEndDay], [DaylightTransitionEndWeek], [DaylightTransitionEndDayOfWeek], [DaylightTransitionEndTimeOfDay], [DaylightDeltaSec]) VALUES (244, 78, 2, N'2011-01-01 00:00:00', N'2011-12-31 00:00:00', 0, 3, 1, 5, 0, N'02:00:00', 0, 1, 1, 1, 6, N'00:00:00', 3600)
INSERT INTO [DateTimeUtil].[TimezoneAdjustmentRule] ([Id], [TimezoneId], [RuleNo], [DateStart], [DateEnd], [DaylightTransitionStartIsFixedDateRule], [DaylightTransitionStartMonth], [DaylightTransitionStartDay], [DaylightTransitionStartWeek], [DaylightTransitionStartDayOfWeek], [DaylightTransitionStartTimeOfDay], [DaylightTransitionEndIsFixedDateRule], [DaylightTransitionEndMonth], [DaylightTransitionEndDay], [DaylightTransitionEndWeek], [DaylightTransitionEndDayOfWeek], [DaylightTransitionEndTimeOfDay], [DaylightDeltaSec]) VALUES (245, 81, 1, N'2006-01-01 00:00:00', N'2006-12-31 00:00:00', 0, 12, 1, 1, 0, N'02:00:00', 0, 1, 1, 1, 0, N'00:00:00', 3600)
INSERT INTO [DateTimeUtil].[TimezoneAdjustmentRule] ([Id], [TimezoneId], [RuleNo], [DateStart], [DateEnd], [DaylightTransitionStartIsFixedDateRule], [DaylightTransitionStartMonth], [DaylightTransitionStartDay], [DaylightTransitionStartWeek], [DaylightTransitionStartDayOfWeek], [DaylightTransitionStartTimeOfDay], [DaylightTransitionEndIsFixedDateRule], [DaylightTransitionEndMonth], [DaylightTransitionEndDay], [DaylightTransitionEndWeek], [DaylightTransitionEndDayOfWeek], [DaylightTransitionEndTimeOfDay], [DaylightDeltaSec]) VALUES (246, 81, 2, N'2007-01-01 00:00:00', N'2007-12-31 00:00:00', 0, 10, 1, 5, 0, N'02:00:00', 0, 3, 1, 5, 0, N'03:00:00', 3600)
INSERT INTO [DateTimeUtil].[TimezoneAdjustmentRule] ([Id], [TimezoneId], [RuleNo], [DateStart], [DateEnd], [DaylightTransitionStartIsFixedDateRule], [DaylightTransitionStartMonth], [DaylightTransitionStartDay], [DaylightTransitionStartWeek], [DaylightTransitionStartDayOfWeek], [DaylightTransitionStartTimeOfDay], [DaylightTransitionEndIsFixedDateRule], [DaylightTransitionEndMonth], [DaylightTransitionEndDay], [DaylightTransitionEndWeek], [DaylightTransitionEndDayOfWeek], [DaylightTransitionEndTimeOfDay], [DaylightDeltaSec]) VALUES (247, 81, 3, N'2008-01-01 00:00:00', N'2008-12-31 00:00:00', 0, 10, 1, 5, 0, N'02:00:00', 0, 3, 1, 5, 0, N'03:00:00', 3600)
INSERT INTO [DateTimeUtil].[TimezoneAdjustmentRule] ([Id], [TimezoneId], [RuleNo], [DateStart], [DateEnd], [DaylightTransitionStartIsFixedDateRule], [DaylightTransitionStartMonth], [DaylightTransitionStartDay], [DaylightTransitionStartWeek], [DaylightTransitionStartDayOfWeek], [DaylightTransitionStartTimeOfDay], [DaylightTransitionEndIsFixedDateRule], [DaylightTransitionEndMonth], [DaylightTransitionEndDay], [DaylightTransitionEndWeek], [DaylightTransitionEndDayOfWeek], [DaylightTransitionEndTimeOfDay], [DaylightDeltaSec]) VALUES (248, 81, 4, N'2009-01-01 00:00:00', N'2009-12-31 00:00:00', 0, 1, 1, 1, 4, N'00:00:00', 0, 3, 1, 5, 0, N'03:00:00', 3600)
INSERT INTO [DateTimeUtil].[TimezoneAdjustmentRule] ([Id], [TimezoneId], [RuleNo], [DateStart], [DateEnd], [DaylightTransitionStartIsFixedDateRule], [DaylightTransitionStartMonth], [DaylightTransitionStartDay], [DaylightTransitionStartWeek], [DaylightTransitionStartDayOfWeek], [DaylightTransitionStartTimeOfDay], [DaylightTransitionEndIsFixedDateRule], [DaylightTransitionEndMonth], [DaylightTransitionEndDay], [DaylightTransitionEndWeek], [DaylightTransitionEndDayOfWeek], [DaylightTransitionEndTimeOfDay], [DaylightDeltaSec]) VALUES (249, 84, 1, N'0001-01-01 00:00:00', N'2010-12-31 00:00:00', 0, 3, 1, 5, 0, N'02:00:00', 0, 10, 1, 5, 0, N'03:00:00', 3600)
INSERT INTO [DateTimeUtil].[TimezoneAdjustmentRule] ([Id], [TimezoneId], [RuleNo], [DateStart], [DateEnd], [DaylightTransitionStartIsFixedDateRule], [DaylightTransitionStartMonth], [DaylightTransitionStartDay], [DaylightTransitionStartWeek], [DaylightTransitionStartDayOfWeek], [DaylightTransitionStartTimeOfDay], [DaylightTransitionEndIsFixedDateRule], [DaylightTransitionEndMonth], [DaylightTransitionEndDay], [DaylightTransitionEndWeek], [DaylightTransitionEndDayOfWeek], [DaylightTransitionEndTimeOfDay], [DaylightDeltaSec]) VALUES (250, 84, 2, N'2011-01-01 00:00:00', N'2011-12-31 00:00:00', 0, 3, 1, 5, 0, N'02:00:00', 0, 1, 1, 1, 6, N'00:00:00', 3600)
INSERT INTO [DateTimeUtil].[TimezoneAdjustmentRule] ([Id], [TimezoneId], [RuleNo], [DateStart], [DateEnd], [DaylightTransitionStartIsFixedDateRule], [DaylightTransitionStartMonth], [DaylightTransitionStartDay], [DaylightTransitionStartWeek], [DaylightTransitionStartDayOfWeek], [DaylightTransitionStartTimeOfDay], [DaylightTransitionEndIsFixedDateRule], [DaylightTransitionEndMonth], [DaylightTransitionEndDay], [DaylightTransitionEndWeek], [DaylightTransitionEndDayOfWeek], [DaylightTransitionEndTimeOfDay], [DaylightDeltaSec]) VALUES (251, 87, 1, N'0001-01-01 00:00:00', N'2007-12-31 00:00:00', 0, 10, 1, 5, 0, N'02:00:00', 0, 3, 1, 5, 0, N'03:00:00', 3600)
INSERT INTO [DateTimeUtil].[TimezoneAdjustmentRule] ([Id], [TimezoneId], [RuleNo], [DateStart], [DateEnd], [DaylightTransitionStartIsFixedDateRule], [DaylightTransitionStartMonth], [DaylightTransitionStartDay], [DaylightTransitionStartWeek], [DaylightTransitionStartDayOfWeek], [DaylightTransitionStartTimeOfDay], [DaylightTransitionEndIsFixedDateRule], [DaylightTransitionEndMonth], [DaylightTransitionEndDay], [DaylightTransitionEndWeek], [DaylightTransitionEndDayOfWeek], [DaylightTransitionEndTimeOfDay], [DaylightDeltaSec]) VALUES (252, 87, 2, N'2008-01-01 00:00:00', N'9999-12-31 00:00:00', 0, 10, 1, 1, 0, N'02:00:00', 0, 4, 1, 1, 0, N'03:00:00', 3600)
INSERT INTO [DateTimeUtil].[TimezoneAdjustmentRule] ([Id], [TimezoneId], [RuleNo], [DateStart], [DateEnd], [DaylightTransitionStartIsFixedDateRule], [DaylightTransitionStartMonth], [DaylightTransitionStartDay], [DaylightTransitionStartWeek], [DaylightTransitionStartDayOfWeek], [DaylightTransitionStartTimeOfDay], [DaylightTransitionEndIsFixedDateRule], [DaylightTransitionEndMonth], [DaylightTransitionEndDay], [DaylightTransitionEndWeek], [DaylightTransitionEndDayOfWeek], [DaylightTransitionEndTimeOfDay], [DaylightDeltaSec]) VALUES (253, 90, 1, N'0001-01-01 00:00:00', N'2007-12-31 00:00:00', 0, 10, 1, 5, 0, N'02:00:00', 0, 3, 1, 5, 0, N'03:00:00', 3600)
INSERT INTO [DateTimeUtil].[TimezoneAdjustmentRule] ([Id], [TimezoneId], [RuleNo], [DateStart], [DateEnd], [DaylightTransitionStartIsFixedDateRule], [DaylightTransitionStartMonth], [DaylightTransitionStartDay], [DaylightTransitionStartWeek], [DaylightTransitionStartDayOfWeek], [DaylightTransitionStartTimeOfDay], [DaylightTransitionEndIsFixedDateRule], [DaylightTransitionEndMonth], [DaylightTransitionEndDay], [DaylightTransitionEndWeek], [DaylightTransitionEndDayOfWeek], [DaylightTransitionEndTimeOfDay], [DaylightDeltaSec]) VALUES (254, 90, 2, N'2008-01-01 00:00:00', N'9999-12-31 00:00:00', 0, 10, 1, 1, 0, N'02:00:00', 0, 4, 1, 1, 0, N'03:00:00', 3600)
INSERT INTO [DateTimeUtil].[TimezoneAdjustmentRule] ([Id], [TimezoneId], [RuleNo], [DateStart], [DateEnd], [DaylightTransitionStartIsFixedDateRule], [DaylightTransitionStartMonth], [DaylightTransitionStartDay], [DaylightTransitionStartWeek], [DaylightTransitionStartDayOfWeek], [DaylightTransitionStartTimeOfDay], [DaylightTransitionEndIsFixedDateRule], [DaylightTransitionEndMonth], [DaylightTransitionEndDay], [DaylightTransitionEndWeek], [DaylightTransitionEndDayOfWeek], [DaylightTransitionEndTimeOfDay], [DaylightDeltaSec]) VALUES (255, 92, 1, N'0001-01-01 00:00:00', N'2007-12-31 00:00:00', 0, 10, 1, 1, 0, N'02:00:00', 0, 3, 1, 5, 0, N'03:00:00', 3600)
INSERT INTO [DateTimeUtil].[TimezoneAdjustmentRule] ([Id], [TimezoneId], [RuleNo], [DateStart], [DateEnd], [DaylightTransitionStartIsFixedDateRule], [DaylightTransitionStartMonth], [DaylightTransitionStartDay], [DaylightTransitionStartWeek], [DaylightTransitionStartDayOfWeek], [DaylightTransitionStartTimeOfDay], [DaylightTransitionEndIsFixedDateRule], [DaylightTransitionEndMonth], [DaylightTransitionEndDay], [DaylightTransitionEndWeek], [DaylightTransitionEndDayOfWeek], [DaylightTransitionEndTimeOfDay], [DaylightDeltaSec]) VALUES (256, 92, 2, N'2008-01-01 00:00:00', N'9999-12-31 00:00:00', 0, 10, 1, 1, 0, N'02:00:00', 0, 4, 1, 1, 0, N'03:00:00', 3600)
INSERT INTO [DateTimeUtil].[TimezoneAdjustmentRule] ([Id], [TimezoneId], [RuleNo], [DateStart], [DateEnd], [DaylightTransitionStartIsFixedDateRule], [DaylightTransitionStartMonth], [DaylightTransitionStartDay], [DaylightTransitionStartWeek], [DaylightTransitionStartDayOfWeek], [DaylightTransitionStartTimeOfDay], [DaylightTransitionEndIsFixedDateRule], [DaylightTransitionEndMonth], [DaylightTransitionEndDay], [DaylightTransitionEndWeek], [DaylightTransitionEndDayOfWeek], [DaylightTransitionEndTimeOfDay], [DaylightDeltaSec]) VALUES (257, 93, 1, N'0001-01-01 00:00:00', N'2010-12-31 00:00:00', 0, 3, 1, 5, 0, N'02:00:00', 0, 10, 1, 5, 0, N'03:00:00', 3600)
INSERT INTO [DateTimeUtil].[TimezoneAdjustmentRule] ([Id], [TimezoneId], [RuleNo], [DateStart], [DateEnd], [DaylightTransitionStartIsFixedDateRule], [DaylightTransitionStartMonth], [DaylightTransitionStartDay], [DaylightTransitionStartWeek], [DaylightTransitionStartDayOfWeek], [DaylightTransitionStartTimeOfDay], [DaylightTransitionEndIsFixedDateRule], [DaylightTransitionEndMonth], [DaylightTransitionEndDay], [DaylightTransitionEndWeek], [DaylightTransitionEndDayOfWeek], [DaylightTransitionEndTimeOfDay], [DaylightDeltaSec]) VALUES (258, 93, 2, N'2011-01-01 00:00:00', N'2011-12-31 00:00:00', 0, 3, 1, 5, 0, N'02:00:00', 0, 1, 1, 1, 6, N'00:00:00', 3600)
INSERT INTO [DateTimeUtil].[TimezoneAdjustmentRule] ([Id], [TimezoneId], [RuleNo], [DateStart], [DateEnd], [DaylightTransitionStartIsFixedDateRule], [DaylightTransitionStartMonth], [DaylightTransitionStartDay], [DaylightTransitionStartWeek], [DaylightTransitionStartDayOfWeek], [DaylightTransitionStartTimeOfDay], [DaylightTransitionEndIsFixedDateRule], [DaylightTransitionEndMonth], [DaylightTransitionEndDay], [DaylightTransitionEndWeek], [DaylightTransitionEndDayOfWeek], [DaylightTransitionEndTimeOfDay], [DaylightDeltaSec]) VALUES (259, 95, 1, N'0001-01-01 00:00:00', N'2010-12-31 00:00:00', 0, 3, 1, 5, 0, N'02:00:00', 0, 10, 1, 5, 0, N'03:00:00', 3600)
INSERT INTO [DateTimeUtil].[TimezoneAdjustmentRule] ([Id], [TimezoneId], [RuleNo], [DateStart], [DateEnd], [DaylightTransitionStartIsFixedDateRule], [DaylightTransitionStartMonth], [DaylightTransitionStartDay], [DaylightTransitionStartWeek], [DaylightTransitionStartDayOfWeek], [DaylightTransitionStartTimeOfDay], [DaylightTransitionEndIsFixedDateRule], [DaylightTransitionEndMonth], [DaylightTransitionEndDay], [DaylightTransitionEndWeek], [DaylightTransitionEndDayOfWeek], [DaylightTransitionEndTimeOfDay], [DaylightDeltaSec]) VALUES (260, 95, 2, N'2011-01-01 00:00:00', N'2011-12-31 00:00:00', 0, 3, 1, 5, 0, N'02:00:00', 0, 1, 1, 1, 6, N'00:00:00', 3600)
INSERT INTO [DateTimeUtil].[TimezoneAdjustmentRule] ([Id], [TimezoneId], [RuleNo], [DateStart], [DateEnd], [DaylightTransitionStartIsFixedDateRule], [DaylightTransitionStartMonth], [DaylightTransitionStartDay], [DaylightTransitionStartWeek], [DaylightTransitionStartDayOfWeek], [DaylightTransitionStartTimeOfDay], [DaylightTransitionEndIsFixedDateRule], [DaylightTransitionEndMonth], [DaylightTransitionEndDay], [DaylightTransitionEndWeek], [DaylightTransitionEndDayOfWeek], [DaylightTransitionEndTimeOfDay], [DaylightDeltaSec]) VALUES (261, 96, 1, N'0001-01-01 00:00:00', N'2006-12-31 00:00:00', 0, 10, 1, 1, 0, N'02:00:00', 0, 3, 1, 3, 0, N'03:00:00', 3600)
INSERT INTO [DateTimeUtil].[TimezoneAdjustmentRule] ([Id], [TimezoneId], [RuleNo], [DateStart], [DateEnd], [DaylightTransitionStartIsFixedDateRule], [DaylightTransitionStartMonth], [DaylightTransitionStartDay], [DaylightTransitionStartWeek], [DaylightTransitionStartDayOfWeek], [DaylightTransitionStartTimeOfDay], [DaylightTransitionEndIsFixedDateRule], [DaylightTransitionEndMonth], [DaylightTransitionEndDay], [DaylightTransitionEndWeek], [DaylightTransitionEndDayOfWeek], [DaylightTransitionEndTimeOfDay], [DaylightDeltaSec]) VALUES (262, 96, 2, N'2007-01-01 00:00:00', N'2007-12-31 00:00:00', 0, 9, 1, 5, 0, N'02:00:00', 0, 3, 1, 3, 0, N'03:00:00', 3600)
INSERT INTO [DateTimeUtil].[TimezoneAdjustmentRule] ([Id], [TimezoneId], [RuleNo], [DateStart], [DateEnd], [DaylightTransitionStartIsFixedDateRule], [DaylightTransitionStartMonth], [DaylightTransitionStartDay], [DaylightTransitionStartWeek], [DaylightTransitionStartDayOfWeek], [DaylightTransitionStartTimeOfDay], [DaylightTransitionEndIsFixedDateRule], [DaylightTransitionEndMonth], [DaylightTransitionEndDay], [DaylightTransitionEndWeek], [DaylightTransitionEndDayOfWeek], [DaylightTransitionEndTimeOfDay], [DaylightDeltaSec]) VALUES (263, 96, 3, N'2008-01-01 00:00:00', N'9999-12-31 00:00:00', 0, 9, 1, 5, 0, N'02:00:00', 0, 4, 1, 1, 0, N'03:00:00', 3600)
INSERT INTO [DateTimeUtil].[TimezoneAdjustmentRule] ([Id], [TimezoneId], [RuleNo], [DateStart], [DateEnd], [DaylightTransitionStartIsFixedDateRule], [DaylightTransitionStartMonth], [DaylightTransitionStartDay], [DaylightTransitionStartWeek], [DaylightTransitionStartDayOfWeek], [DaylightTransitionStartTimeOfDay], [DaylightTransitionEndIsFixedDateRule], [DaylightTransitionEndMonth], [DaylightTransitionEndDay], [DaylightTransitionEndWeek], [DaylightTransitionEndDayOfWeek], [DaylightTransitionEndTimeOfDay], [DaylightDeltaSec]) VALUES (264, 97, 1, N'2009-01-01 00:00:00', N'2009-12-31 00:00:00', 0, 11, 1, 5, 0, N'02:00:00', 0, 1, 1, 1, 4, N'00:00:00', 3600)
INSERT INTO [DateTimeUtil].[TimezoneAdjustmentRule] ([Id], [TimezoneId], [RuleNo], [DateStart], [DateEnd], [DaylightTransitionStartIsFixedDateRule], [DaylightTransitionStartMonth], [DaylightTransitionStartDay], [DaylightTransitionStartWeek], [DaylightTransitionStartDayOfWeek], [DaylightTransitionStartTimeOfDay], [DaylightTransitionEndIsFixedDateRule], [DaylightTransitionEndMonth], [DaylightTransitionEndDay], [DaylightTransitionEndWeek], [DaylightTransitionEndDayOfWeek], [DaylightTransitionEndTimeOfDay], [DaylightDeltaSec]) VALUES (265, 97, 2, N'2010-01-01 00:00:00', N'2010-12-31 00:00:00', 0, 10, 1, 4, 0, N'02:00:00', 0, 3, 1, 5, 0, N'03:00:00', 3600)
INSERT INTO [DateTimeUtil].[TimezoneAdjustmentRule] ([Id], [TimezoneId], [RuleNo], [DateStart], [DateEnd], [DaylightTransitionStartIsFixedDateRule], [DaylightTransitionStartMonth], [DaylightTransitionStartDay], [DaylightTransitionStartWeek], [DaylightTransitionStartDayOfWeek], [DaylightTransitionStartTimeOfDay], [DaylightTransitionEndIsFixedDateRule], [DaylightTransitionEndMonth], [DaylightTransitionEndDay], [DaylightTransitionEndWeek], [DaylightTransitionEndDayOfWeek], [DaylightTransitionEndTimeOfDay], [DaylightDeltaSec]) VALUES (266, 97, 3, N'2011-01-01 00:00:00', N'2011-12-31 00:00:00', 0, 10, 1, 4, 0, N'02:00:00', 0, 3, 1, 1, 0, N'03:00:00', 3600)
INSERT INTO [DateTimeUtil].[TimezoneAdjustmentRule] ([Id], [TimezoneId], [RuleNo], [DateStart], [DateEnd], [DaylightTransitionStartIsFixedDateRule], [DaylightTransitionStartMonth], [DaylightTransitionStartDay], [DaylightTransitionStartWeek], [DaylightTransitionStartDayOfWeek], [DaylightTransitionStartTimeOfDay], [DaylightTransitionEndIsFixedDateRule], [DaylightTransitionEndMonth], [DaylightTransitionEndDay], [DaylightTransitionEndWeek], [DaylightTransitionEndDayOfWeek], [DaylightTransitionEndTimeOfDay], [DaylightDeltaSec]) VALUES (267, 97, 4, N'2012-01-01 00:00:00', N'2012-12-31 00:00:00', 0, 10, 1, 3, 0, N'02:00:00', 0, 1, 1, 4, 0, N'03:00:00', 3600)
INSERT INTO [DateTimeUtil].[TimezoneAdjustmentRule] ([Id], [TimezoneId], [RuleNo], [DateStart], [DateEnd], [DaylightTransitionStartIsFixedDateRule], [DaylightTransitionStartMonth], [DaylightTransitionStartDay], [DaylightTransitionStartWeek], [DaylightTransitionStartDayOfWeek], [DaylightTransitionStartTimeOfDay], [DaylightTransitionEndIsFixedDateRule], [DaylightTransitionEndMonth], [DaylightTransitionEndDay], [DaylightTransitionEndWeek], [DaylightTransitionEndDayOfWeek], [DaylightTransitionEndTimeOfDay], [DaylightDeltaSec]) VALUES (268, 97, 5, N'2013-01-01 00:00:00', N'2013-12-31 00:00:00', 0, 10, 1, 4, 0, N'02:00:00', 0, 1, 1, 3, 0, N'03:00:00', 3600)
INSERT INTO [DateTimeUtil].[TimezoneAdjustmentRule] ([Id], [TimezoneId], [RuleNo], [DateStart], [DateEnd], [DaylightTransitionStartIsFixedDateRule], [DaylightTransitionStartMonth], [DaylightTransitionStartDay], [DaylightTransitionStartWeek], [DaylightTransitionStartDayOfWeek], [DaylightTransitionStartTimeOfDay], [DaylightTransitionEndIsFixedDateRule], [DaylightTransitionEndMonth], [DaylightTransitionEndDay], [DaylightTransitionEndWeek], [DaylightTransitionEndDayOfWeek], [DaylightTransitionEndTimeOfDay], [DaylightDeltaSec]) VALUES (269, 97, 6, N'2014-01-01 00:00:00', N'2014-12-31 00:00:00', 0, 10, 1, 4, 0, N'02:00:00', 0, 1, 1, 3, 0, N'02:00:00', 3600)
INSERT INTO [DateTimeUtil].[TimezoneAdjustmentRule] ([Id], [TimezoneId], [RuleNo], [DateStart], [DateEnd], [DaylightTransitionStartIsFixedDateRule], [DaylightTransitionStartMonth], [DaylightTransitionStartDay], [DaylightTransitionStartWeek], [DaylightTransitionStartDayOfWeek], [DaylightTransitionStartTimeOfDay], [DaylightTransitionEndIsFixedDateRule], [DaylightTransitionEndMonth], [DaylightTransitionEndDay], [DaylightTransitionEndWeek], [DaylightTransitionEndDayOfWeek], [DaylightTransitionEndTimeOfDay], [DaylightDeltaSec]) VALUES (270, 97, 7, N'2015-01-01 00:00:00', N'9999-12-31 00:00:00', 0, 10, 1, 4, 0, N'02:00:00', 0, 1, 1, 4, 0, N'03:00:00', 3600)
INSERT INTO [DateTimeUtil].[TimezoneAdjustmentRule] ([Id], [TimezoneId], [RuleNo], [DateStart], [DateEnd], [DaylightTransitionStartIsFixedDateRule], [DaylightTransitionStartMonth], [DaylightTransitionStartDay], [DaylightTransitionStartWeek], [DaylightTransitionStartDayOfWeek], [DaylightTransitionStartTimeOfDay], [DaylightTransitionEndIsFixedDateRule], [DaylightTransitionEndMonth], [DaylightTransitionEndDay], [DaylightTransitionEndWeek], [DaylightTransitionEndDayOfWeek], [DaylightTransitionEndTimeOfDay], [DaylightDeltaSec]) VALUES (271, 99, 1, N'0001-01-01 00:00:00', N'2010-12-31 00:00:00', 0, 3, 1, 5, 0, N'02:00:00', 0, 10, 1, 5, 0, N'03:00:00', 3600)
INSERT INTO [DateTimeUtil].[TimezoneAdjustmentRule] ([Id], [TimezoneId], [RuleNo], [DateStart], [DateEnd], [DaylightTransitionStartIsFixedDateRule], [DaylightTransitionStartMonth], [DaylightTransitionStartDay], [DaylightTransitionStartWeek], [DaylightTransitionStartDayOfWeek], [DaylightTransitionStartTimeOfDay], [DaylightTransitionEndIsFixedDateRule], [DaylightTransitionEndMonth], [DaylightTransitionEndDay], [DaylightTransitionEndWeek], [DaylightTransitionEndDayOfWeek], [DaylightTransitionEndTimeOfDay], [DaylightDeltaSec]) VALUES (272, 99, 2, N'2011-01-01 00:00:00', N'2011-12-31 00:00:00', 0, 3, 1, 5, 0, N'02:00:00', 0, 1, 1, 1, 6, N'00:00:00', 3600)
INSERT INTO [DateTimeUtil].[TimezoneAdjustmentRule] ([Id], [TimezoneId], [RuleNo], [DateStart], [DateEnd], [DaylightTransitionStartIsFixedDateRule], [DaylightTransitionStartMonth], [DaylightTransitionStartDay], [DaylightTransitionStartWeek], [DaylightTransitionStartDayOfWeek], [DaylightTransitionStartTimeOfDay], [DaylightTransitionEndIsFixedDateRule], [DaylightTransitionEndMonth], [DaylightTransitionEndDay], [DaylightTransitionEndWeek], [DaylightTransitionEndDayOfWeek], [DaylightTransitionEndTimeOfDay], [DaylightDeltaSec]) VALUES (273, 100, 1, N'0001-01-01 00:00:00', N'9999-12-31 00:00:00', 0, 3, 1, 5, 0, N'02:00:00', 0, 10, 1, 5, 0, N'03:00:00', 3600)
INSERT INTO [DateTimeUtil].[TimezoneAdjustmentRule] ([Id], [TimezoneId], [RuleNo], [DateStart], [DateEnd], [DaylightTransitionStartIsFixedDateRule], [DaylightTransitionStartMonth], [DaylightTransitionStartDay], [DaylightTransitionStartWeek], [DaylightTransitionStartDayOfWeek], [DaylightTransitionStartTimeOfDay], [DaylightTransitionEndIsFixedDateRule], [DaylightTransitionEndMonth], [DaylightTransitionEndDay], [DaylightTransitionEndWeek], [DaylightTransitionEndDayOfWeek], [DaylightTransitionEndTimeOfDay], [DaylightDeltaSec]) VALUES (274, 102, 1, N'2010-01-01 00:00:00', N'2010-12-31 00:00:00', 0, 9, 1, 5, 6, N'23:59:59.999', 0, 1, 1, 1, 5, N'00:00:00', 3600)
INSERT INTO [DateTimeUtil].[TimezoneAdjustmentRule] ([Id], [TimezoneId], [RuleNo], [DateStart], [DateEnd], [DaylightTransitionStartIsFixedDateRule], [DaylightTransitionStartMonth], [DaylightTransitionStartDay], [DaylightTransitionStartWeek], [DaylightTransitionStartDayOfWeek], [DaylightTransitionStartTimeOfDay], [DaylightTransitionEndIsFixedDateRule], [DaylightTransitionEndMonth], [DaylightTransitionEndDay], [DaylightTransitionEndWeek], [DaylightTransitionEndDayOfWeek], [DaylightTransitionEndTimeOfDay], [DaylightDeltaSec]) VALUES (275, 102, 2, N'2011-01-01 00:00:00', N'2011-12-31 00:00:00', 0, 9, 1, 4, 6, N'03:00:00', 0, 4, 1, 1, 6, N'04:00:00', 3600)
INSERT INTO [DateTimeUtil].[TimezoneAdjustmentRule] ([Id], [TimezoneId], [RuleNo], [DateStart], [DateEnd], [DaylightTransitionStartIsFixedDateRule], [DaylightTransitionStartMonth], [DaylightTransitionStartDay], [DaylightTransitionStartWeek], [DaylightTransitionStartDayOfWeek], [DaylightTransitionStartTimeOfDay], [DaylightTransitionEndIsFixedDateRule], [DaylightTransitionEndMonth], [DaylightTransitionEndDay], [DaylightTransitionEndWeek], [DaylightTransitionEndDayOfWeek], [DaylightTransitionEndTimeOfDay], [DaylightDeltaSec]) VALUES (276, 102, 3, N'2012-01-01 00:00:00', N'9999-12-31 00:00:00', 0, 9, 1, 5, 0, N'00:00:00', 0, 4, 1, 1, 0, N'01:00:00', 3600)
GO

/*
=============================================
Author:         Degen, Andreas
Copyright:      All rights reserved
Create date:    2014-04-26
Description:    Converts the given date to UTC applying the given timezone

Parameters:
    
    @OriginalTimezoneId INT: The unique ID of your original timezone (supported timezone IDs see table "DateTimeUtil.Timezone" column "Id")
    @TargetTimezoneId INT: The unique ID of your target timezone (supported timezone IDs see table "DateTimeUtil.Timezone" column "Id")
    @LocalDate DATETIME2: The datetime value in your original timezone which you want to convert to the corresponding datetime value in your target timezone

Return value:
    
    @Result DATETIME2: The converted datetime value in your target timezone

Remarks:


Samples:

    SELECT [DateTimeUtil].[UDF_ConvertLocalToLocalByTimezoneId] (38, 46, GETDATE())
    SELECT [DateTimeUtil].[UDF_ConvertLocalToLocalByTimezoneId] (38, 46, '2014-03-30 01:55:00')
    SELECT [DateTimeUtil].[UDF_ConvertLocalToLocalByTimezoneId] (38, 46, '2014-03-30 03:05:00')
    SELECT [DateTimeUtil].[UDF_ConvertLocalToLocalByTimezoneId] (38, 46, '2014-10-26 02:05:00')
    SELECT [DateTimeUtil].[UDF_ConvertLocalToLocalByTimezoneId] (38, 46, '2014-10-26 03:05:00')

Change log:
    
=============================================
*/
CREATE FUNCTION [DateTimeUtil].[UDF_ConvertLocalToLocalByTimezoneId]
(
    @OriginalTimezoneId INT
    ,@TargetTimezoneId INT
    ,@LocalDate DATETIME2
)
RETURNS DATETIME2
AS
BEGIN
	DECLARE
        @Result DATETIME2 = NULL
    ;

    SELECT @Result = [DateTimeUtil].[UDF_ConvertUtcToLocalByTimezoneId] (
        @TargetTimezoneId
        ,[DateTimeUtil].[UDF_ConvertLocalToUtcByTimezoneId] (
            @OriginalTimezoneId
            ,@LocalDate
        )
    );

	RETURN @Result;

END
GO

/*
=============================================
Author:         Degen, Andreas
Copyright:      All rights reserved
Create date:    2014-04-26
Description:    Converts the given date to UTC applying the given timezone

Parameters:
    
    @OriginalTimezoneIdentifier NVARCHAR(100): The unique identifier of your original timezone (supported timezone identifiers see table "DateTimeUtil.Timezone" column "Identifier")
    @TargetTimezoneIdentifier NVARCHAR(100): The unique identifier of your target timezone (supported timezone identifiers see table "DateTimeUtil.Timezone" column "Identifier")
    @LocalDate DATETIME2: The datetime value in your original timezone which you want to convert to the corresponding datetime value in your target timezone

Return value:
    
    @Result DATETIME2: The converted datetime value in your target timezone

Remarks:


Samples:

    SELECT [DateTimeUtil].[UDF_ConvertLocalToLocalByTimezoneIdentifier] ('W. Europe Standard Time', 'Middle East Standard Time', GETDATE())
    SELECT [DateTimeUtil].[UDF_ConvertLocalToLocalByTimezoneIdentifier] ('W. Europe Standard Time', 'Middle East Standard Time', '2014-03-30 01:55:00')
    SELECT [DateTimeUtil].[UDF_ConvertLocalToLocalByTimezoneIdentifier] ('W. Europe Standard Time', 'Middle East Standard Time', '2014-03-30 03:05:00')
    SELECT [DateTimeUtil].[UDF_ConvertLocalToLocalByTimezoneIdentifier] ('W. Europe Standard Time', 'Middle East Standard Time', '2014-10-26 02:05:00')
    SELECT [DateTimeUtil].[UDF_ConvertLocalToLocalByTimezoneIdentifier] ('W. Europe Standard Time', 'Middle East Standard Time', '2014-10-26 03:05:00')

Change log:
    
=============================================
*/
CREATE FUNCTION [DateTimeUtil].[UDF_ConvertLocalToLocalByTimezoneIdentifier]
(
    @OriginalTimezoneIdentifier NVARCHAR(100)
    ,@TargetTimezoneIdentifier NVARCHAR(100)
    ,@LocalDate DATETIME2
)
RETURNS DATETIME2
AS
BEGIN
	DECLARE
        @Result DATETIME2 = NULL
    ;

    SELECT @Result = [DateTimeUtil].[UDF_ConvertUtcToLocalByTimezoneIdentifier] (
        @TargetTimezoneIdentifier
        ,[DateTimeUtil].[UDF_ConvertLocalToUtcByTimezoneIdentifier] (
            @OriginalTimezoneIdentifier
            ,@LocalDate
        )
    );

	RETURN @Result;

END
GO

/*
=============================================
Author:         Degen, Andreas
Copyright:      All rights reserved
Create date:    2014-04-26
Description:    Converts the given date to UTC applying the given timezone

Parameters:
    
    @OriginalTimezoneId INT: The unique ID of your original timezone (supported timezone IDs see table "DateTimeUtil.Timezone" column "Id")
    @LocalDate DATETIME2: The original local datetime value which you want to convert to UTC datetime

Return value:
    
    @Result DATETIME2: The converted datetime value in UTC datetime

Remarks:


Samples:

    SELECT [DateTimeUtil].[UDF_ConvertLocalToUtcByTimezoneId] (38, GETDATE())

    -- northern hemisphere (+3600)
    SELECT [DateTimeUtil].[UDF_ConvertLocalToUtcByTimezoneId] (38, '2014-03-30 01:55:00')
    SELECT [DateTimeUtil].[UDF_ConvertLocalToUtcByTimezoneId] (38, '2014-03-30 03:05:00')
    SELECT [DateTimeUtil].[UDF_ConvertLocalToUtcByTimezoneId] (38, '2014-10-26 02:05:00')
    SELECT [DateTimeUtil].[UDF_ConvertLocalToUtcByTimezoneId] (38, '2014-10-26 03:05:00')

    -- southern hemisphere (+3600)
    SELECT [DateTimeUtil].[UDF_ConvertLocalToUtcByTimezoneId] (43, '2014-04-06 01:55:00')
    SELECT [DateTimeUtil].[UDF_ConvertLocalToUtcByTimezoneId] (43, '2014-04-06 02:05:00')
    SELECT [DateTimeUtil].[UDF_ConvertLocalToUtcByTimezoneId] (43, '2014-09-07 01:55:00')
    SELECT [DateTimeUtil].[UDF_ConvertLocalToUtcByTimezoneId] (43, '2014-09-07 03:05:00')

Change log:
    
=============================================
*/
CREATE FUNCTION [DateTimeUtil].[UDF_ConvertLocalToUtcByTimezoneId]
(
    @OriginalTimezoneId INT
    ,@LocalDate DATETIME2
)
RETURNS DATETIME2
AS
BEGIN
	DECLARE
        @Result DATETIME2 = NULL
    ;

    SELECT @Result = 
        DATEADD(SECOND, -([tz].[BaseUtcOffsetSec] + COALESCE([ar].[DaylightDeltaSec], 0)), @LocalDate)
    FROM
        [DateTimeUtil].[Timezone] AS [tz] WITH (READUNCOMMITTED)
        LEFT JOIN [DateTimeUtil].[TimezoneAdjustmentRule] AS [ar] WITH (READUNCOMMITTED)
            ON 1 = 1
            AND [ar].[TimezoneId] = [tz].[Id]
            AND @LocalDate BETWEEN [ar].[DateStart] AND [ar].[DateEnd]
            AND ( 1 = 0
                OR ( 1 = 1
                    -- southern hemisphere
                    AND [ar].[DaylightTransitionStartMonth] > [ar].[DaylightTransitionEndMonth]
                    AND NOT @LocalDate
                        BETWEEN
                            CASE
                                WHEN [ar].[DaylightTransitionEndIsFixedDateRule] = 1 THEN CONVERT(DATETIME2, RIGHT('0000' + CONVERT(NVARCHAR, YEAR(@LocalDate)), 4) + '-' + RIGHT('00' + CONVERT(NVARCHAR, [ar].[DaylightTransitionEndMonth]), 2) + '-' + RIGHT('00' + CONVERT(NVARCHAR, [ar].[DaylightTransitionEndDay]), 2) + ' ' + CONVERT(NVARCHAR, [ar].[DaylightTransitionEndTimeOfDay]), 121)
                                ELSE 
                                    CASE
                                        WHEN [ar].[DaylightTransitionEndWeek] = 5 THEN CONVERT(DATETIME2, CONVERT(NVARCHAR, [DateTimeUtil].[UDF_GetLastWeekdayInMonth] ([ar].[DaylightTransitionEndDayOfWeek], CONVERT(DATETIME2, RIGHT('0000' + CONVERT(NVARCHAR, YEAR(@LocalDate)), 4) + '-' + RIGHT('00' + CONVERT(NVARCHAR, [ar].[DaylightTransitionEndMonth]), 2) + '-' + RIGHT('00' + CONVERT(NVARCHAR, [ar].[DaylightTransitionEndDay]), 2), 121)), 121)  + ' ' + CONVERT(NVARCHAR, [ar].[DaylightTransitionEndTimeOfDay]), 121)
                                        ELSE CONVERT(DATETIME2, CONVERT(NVARCHAR, CONVERT(DATE, DATEADD(DAY, ([ar].[DaylightTransitionEndWeek] - 1) * 7, [DateTimeUtil].[UDF_GetFirstWeekdayInMonth] ([ar].[DaylightTransitionEndDayOfWeek], CONVERT(DATETIME2, RIGHT('0000' + CONVERT(NVARCHAR, YEAR(@LocalDate)), 4) + '-' + RIGHT('00' + CONVERT(NVARCHAR, [ar].[DaylightTransitionEndMonth]), 2) + '-' + RIGHT('00' + CONVERT(NVARCHAR, [ar].[DaylightTransitionEndDay]), 2), 121)))), 121)  + ' ' + CONVERT(NVARCHAR, [ar].[DaylightTransitionEndTimeOfDay]), 121)
                                    END
                            END
                        AND
                            CASE
                                WHEN [ar].[DaylightTransitionStartIsFixedDateRule] = 1 THEN CONVERT(DATETIME2, RIGHT('0000' + CONVERT(NVARCHAR, YEAR(@LocalDate)), 4) + '-' + RIGHT('00' + CONVERT(NVARCHAR, [ar].[DaylightTransitionStartMonth]), 2) + '-' + RIGHT('00' + CONVERT(NVARCHAR, [ar].[DaylightTransitionStartDay]), 2) + ' ' + CONVERT(NVARCHAR, [ar].[DaylightTransitionStartTimeOfDay]), 121)
                                ELSE 
                                    CASE
                                        WHEN [ar].[DaylightTransitionStartWeek] = 5 THEN CONVERT(DATETIME2, CONVERT(NVARCHAR, [DateTimeUtil].[UDF_GetLastWeekdayInMonth] ([ar].[DaylightTransitionStartDayOfWeek], CONVERT(DATETIME2, RIGHT('0000' + CONVERT(NVARCHAR, YEAR(@LocalDate)), 4) + '-' + RIGHT('00' + CONVERT(NVARCHAR, [ar].[DaylightTransitionStartMonth]), 2) + '-' + RIGHT('00' + CONVERT(NVARCHAR, [ar].[DaylightTransitionStartDay]), 2), 121)), 121)  + ' ' + CONVERT(NVARCHAR, [ar].[DaylightTransitionStartTimeOfDay]), 121)
                                        ELSE CONVERT(DATETIME2, CONVERT(NVARCHAR, CONVERT(DATE, DATEADD(DAY, ([ar].[DaylightTransitionStartWeek] - 1) * 7, [DateTimeUtil].[UDF_GetFirstWeekdayInMonth] ([ar].[DaylightTransitionStartDayOfWeek], CONVERT(DATETIME2, RIGHT('0000' + CONVERT(NVARCHAR, YEAR(@LocalDate)), 4) + '-' + RIGHT('00' + CONVERT(NVARCHAR, [ar].[DaylightTransitionStartMonth]), 2) + '-' + RIGHT('00' + CONVERT(NVARCHAR, [ar].[DaylightTransitionStartDay]), 2), 121)))), 121)  + ' ' + CONVERT(NVARCHAR, [ar].[DaylightTransitionStartTimeOfDay]), 121)
                                    END
                            END                
                ) OR
                ( 1 = 1
                    -- northern hemisphere
                    AND [ar].[DaylightTransitionStartMonth] <= [ar].[DaylightTransitionEndMonth]
                    AND @LocalDate
                        BETWEEN
                            CASE
                                WHEN [ar].[DaylightTransitionStartIsFixedDateRule] = 1 THEN CONVERT(DATETIME2, RIGHT('0000' + CONVERT(NVARCHAR, YEAR(@LocalDate)), 4) + '-' + RIGHT('00' + CONVERT(NVARCHAR, [ar].[DaylightTransitionStartMonth]), 2) + '-' + RIGHT('00' + CONVERT(NVARCHAR, [ar].[DaylightTransitionStartDay]), 2) + ' ' + CONVERT(NVARCHAR, [ar].[DaylightTransitionStartTimeOfDay]), 121)
                                ELSE 
                                    CASE
                                        WHEN [ar].[DaylightTransitionStartWeek] = 5 THEN CONVERT(DATETIME2, CONVERT(NVARCHAR, [DateTimeUtil].[UDF_GetLastWeekdayInMonth] ([ar].[DaylightTransitionStartDayOfWeek], CONVERT(DATETIME2, RIGHT('0000' + CONVERT(NVARCHAR, YEAR(@LocalDate)), 4) + '-' + RIGHT('00' + CONVERT(NVARCHAR, [ar].[DaylightTransitionStartMonth]), 2) + '-' + RIGHT('00' + CONVERT(NVARCHAR, [ar].[DaylightTransitionStartDay]), 2), 121)), 121)  + ' ' + CONVERT(NVARCHAR, [ar].[DaylightTransitionStartTimeOfDay]), 121)
                                        ELSE CONVERT(DATETIME2, CONVERT(NVARCHAR, CONVERT(DATE, DATEADD(DAY, ([ar].[DaylightTransitionStartWeek] - 1) * 7, [DateTimeUtil].[UDF_GetFirstWeekdayInMonth] ([ar].[DaylightTransitionStartDayOfWeek], CONVERT(DATETIME2, RIGHT('0000' + CONVERT(NVARCHAR, YEAR(@LocalDate)), 4) + '-' + RIGHT('00' + CONVERT(NVARCHAR, [ar].[DaylightTransitionStartMonth]), 2) + '-' + RIGHT('00' + CONVERT(NVARCHAR, [ar].[DaylightTransitionStartDay]), 2), 121)))), 121)  + ' ' + CONVERT(NVARCHAR, [ar].[DaylightTransitionStartTimeOfDay]), 121)
                                    END
                            END
                        AND
                            CASE
                                WHEN [ar].[DaylightTransitionEndIsFixedDateRule] = 1 THEN CONVERT(DATETIME2, RIGHT('0000' + CONVERT(NVARCHAR, YEAR(@LocalDate)), 4) + '-' + RIGHT('00' + CONVERT(NVARCHAR, [ar].[DaylightTransitionEndMonth]), 2) + '-' + RIGHT('00' + CONVERT(NVARCHAR, [ar].[DaylightTransitionEndDay]), 2) + ' ' + CONVERT(NVARCHAR, [ar].[DaylightTransitionEndTimeOfDay]), 121)
                                ELSE 
                                    CASE
                                        WHEN [ar].[DaylightTransitionEndWeek] = 5 THEN CONVERT(DATETIME2, CONVERT(NVARCHAR, [DateTimeUtil].[UDF_GetLastWeekdayInMonth] ([ar].[DaylightTransitionEndDayOfWeek], CONVERT(DATETIME2, RIGHT('0000' + CONVERT(NVARCHAR, YEAR(@LocalDate)), 4) + '-' + RIGHT('00' + CONVERT(NVARCHAR, [ar].[DaylightTransitionEndMonth]), 2) + '-' + RIGHT('00' + CONVERT(NVARCHAR, [ar].[DaylightTransitionEndDay]), 2), 121)), 121)  + ' ' + CONVERT(NVARCHAR, [ar].[DaylightTransitionEndTimeOfDay]), 121)
                                        ELSE CONVERT(DATETIME2, CONVERT(NVARCHAR, CONVERT(DATE, DATEADD(DAY, ([ar].[DaylightTransitionEndWeek] - 1) * 7, [DateTimeUtil].[UDF_GetFirstWeekdayInMonth] ([ar].[DaylightTransitionEndDayOfWeek], CONVERT(DATETIME2, RIGHT('0000' + CONVERT(NVARCHAR, YEAR(@LocalDate)), 4) + '-' + RIGHT('00' + CONVERT(NVARCHAR, [ar].[DaylightTransitionEndMonth]), 2) + '-' + RIGHT('00' + CONVERT(NVARCHAR, [ar].[DaylightTransitionEndDay]), 2), 121)))), 121)  + ' ' + CONVERT(NVARCHAR, [ar].[DaylightTransitionEndTimeOfDay]), 121)
                                    END
                            END
                )                          
            )
    WHERE 1 = 1
        AND [tz].[Id] = @OriginalTimezoneId
    ;

	RETURN @Result;

END
GO

/*
=============================================
Author:         Degen, Andreas
Copyright:      All rights reserved
Create date:    2014-04-26
Description:    Converts the given date to UTC applying the given timezone

Parameters:
    
    @OriginalTimezoneIdentifier NVARCHAR(100): The unique identifier of your original timezone (supported timezone identifiers see table "DateTimeUtil.Timezone" column "Identifier")
    @LocalDate DATETIME2: The original local datetime value which you want to convert to UTC datetime

Return value:
    
    @Result DATETIME2: The converted datetime value in UTC datetime

Remarks:


Samples:

    SELECT [DateTimeUtil].[UDF_ConvertLocalToUtcByTimezoneIdentifier] ('W. Europe Standard Time', GETDATE())
    
    -- northern hemisphere (+3600)
    SELECT [DateTimeUtil].[UDF_ConvertLocalToUtcByTimezoneIdentifier] ('W. Europe Standard Time', '2014-03-30 01:55:00')
    SELECT [DateTimeUtil].[UDF_ConvertLocalToUtcByTimezoneIdentifier] ('W. Europe Standard Time', '2014-03-30 03:05:00')
    SELECT [DateTimeUtil].[UDF_ConvertLocalToUtcByTimezoneIdentifier] ('W. Europe Standard Time', '2014-10-26 02:05:00')
    SELECT [DateTimeUtil].[UDF_ConvertLocalToUtcByTimezoneIdentifier] ('W. Europe Standard Time', '2014-10-26 03:05:00')

    -- southern hemisphere (+3600)
    SELECT [DateTimeUtil].[UDF_ConvertLocalToUtcByTimezoneIdentifier] ('Namibia Standard Time', '2014-04-06 01:55:00')
    SELECT [DateTimeUtil].[UDF_ConvertLocalToUtcByTimezoneIdentifier] ('Namibia Standard Time', '2014-04-06 02:05:00')
    SELECT [DateTimeUtil].[UDF_ConvertLocalToUtcByTimezoneIdentifier] ('Namibia Standard Time', '2014-09-07 01:55:00')
    SELECT [DateTimeUtil].[UDF_ConvertLocalToUtcByTimezoneIdentifier] ('Namibia Standard Time', '2014-09-07 03:05:00')

Change log:
    
=============================================
*/
CREATE FUNCTION [DateTimeUtil].[UDF_ConvertLocalToUtcByTimezoneIdentifier]
(
    @OriginalTimezoneIdentifier NVARCHAR(100)
    ,@LocalDate DATETIME2
)
RETURNS DATETIME2
AS
BEGIN
	DECLARE
        @Result DATETIME2 = NULL
    ;

    SELECT @Result = 
        DATEADD(SECOND, -([tz].[BaseUtcOffsetSec] + COALESCE([ar].[DaylightDeltaSec], 0)), @LocalDate)
    FROM
        [DateTimeUtil].[Timezone] AS [tz] WITH (READUNCOMMITTED)
        LEFT JOIN [DateTimeUtil].[TimezoneAdjustmentRule] AS [ar] WITH (READUNCOMMITTED)
            ON 1 = 1
            AND [ar].[TimezoneId] = [tz].[Id]
            AND @LocalDate BETWEEN [ar].[DateStart] AND [ar].[DateEnd]
            AND ( 1 = 0
                OR ( 1 = 1
                    -- southern hemisphere
                    AND [ar].[DaylightTransitionStartMonth] > [ar].[DaylightTransitionEndMonth]
                    AND NOT @LocalDate
                        BETWEEN
                            CASE
                                WHEN [ar].[DaylightTransitionEndIsFixedDateRule] = 1 THEN CONVERT(DATETIME2, RIGHT('0000' + CONVERT(NVARCHAR, YEAR(@LocalDate)), 4) + '-' + RIGHT('00' + CONVERT(NVARCHAR, [ar].[DaylightTransitionEndMonth]), 2) + '-' + RIGHT('00' + CONVERT(NVARCHAR, [ar].[DaylightTransitionEndDay]), 2) + ' ' + CONVERT(NVARCHAR, [ar].[DaylightTransitionEndTimeOfDay]), 121)
                                ELSE 
                                    CASE
                                        WHEN [ar].[DaylightTransitionEndWeek] = 5 THEN CONVERT(DATETIME2, CONVERT(NVARCHAR, [DateTimeUtil].[UDF_GetLastWeekdayInMonth] ([ar].[DaylightTransitionEndDayOfWeek], CONVERT(DATETIME2, RIGHT('0000' + CONVERT(NVARCHAR, YEAR(@LocalDate)), 4) + '-' + RIGHT('00' + CONVERT(NVARCHAR, [ar].[DaylightTransitionEndMonth]), 2) + '-' + RIGHT('00' + CONVERT(NVARCHAR, [ar].[DaylightTransitionEndDay]), 2), 121)), 121)  + ' ' + CONVERT(NVARCHAR, [ar].[DaylightTransitionEndTimeOfDay]), 121)
                                        ELSE CONVERT(DATETIME2, CONVERT(NVARCHAR, CONVERT(DATE, DATEADD(DAY, ([ar].[DaylightTransitionEndWeek] - 1) * 7, [DateTimeUtil].[UDF_GetFirstWeekdayInMonth] ([ar].[DaylightTransitionEndDayOfWeek], CONVERT(DATETIME2, RIGHT('0000' + CONVERT(NVARCHAR, YEAR(@LocalDate)), 4) + '-' + RIGHT('00' + CONVERT(NVARCHAR, [ar].[DaylightTransitionEndMonth]), 2) + '-' + RIGHT('00' + CONVERT(NVARCHAR, [ar].[DaylightTransitionEndDay]), 2), 121)))), 121)  + ' ' + CONVERT(NVARCHAR, [ar].[DaylightTransitionEndTimeOfDay]), 121)
                                    END
                            END
                        AND
                            CASE
                                WHEN [ar].[DaylightTransitionStartIsFixedDateRule] = 1 THEN CONVERT(DATETIME2, RIGHT('0000' + CONVERT(NVARCHAR, YEAR(@LocalDate)), 4) + '-' + RIGHT('00' + CONVERT(NVARCHAR, [ar].[DaylightTransitionStartMonth]), 2) + '-' + RIGHT('00' + CONVERT(NVARCHAR, [ar].[DaylightTransitionStartDay]), 2) + ' ' + CONVERT(NVARCHAR, [ar].[DaylightTransitionStartTimeOfDay]), 121)
                                ELSE 
                                    CASE
                                        WHEN [ar].[DaylightTransitionStartWeek] = 5 THEN CONVERT(DATETIME2, CONVERT(NVARCHAR, [DateTimeUtil].[UDF_GetLastWeekdayInMonth] ([ar].[DaylightTransitionStartDayOfWeek], CONVERT(DATETIME2, RIGHT('0000' + CONVERT(NVARCHAR, YEAR(@LocalDate)), 4) + '-' + RIGHT('00' + CONVERT(NVARCHAR, [ar].[DaylightTransitionStartMonth]), 2) + '-' + RIGHT('00' + CONVERT(NVARCHAR, [ar].[DaylightTransitionStartDay]), 2), 121)), 121)  + ' ' + CONVERT(NVARCHAR, [ar].[DaylightTransitionStartTimeOfDay]), 121)
                                        ELSE CONVERT(DATETIME2, CONVERT(NVARCHAR, CONVERT(DATE, DATEADD(DAY, ([ar].[DaylightTransitionStartWeek] - 1) * 7, [DateTimeUtil].[UDF_GetFirstWeekdayInMonth] ([ar].[DaylightTransitionStartDayOfWeek], CONVERT(DATETIME2, RIGHT('0000' + CONVERT(NVARCHAR, YEAR(@LocalDate)), 4) + '-' + RIGHT('00' + CONVERT(NVARCHAR, [ar].[DaylightTransitionStartMonth]), 2) + '-' + RIGHT('00' + CONVERT(NVARCHAR, [ar].[DaylightTransitionStartDay]), 2), 121)))), 121)  + ' ' + CONVERT(NVARCHAR, [ar].[DaylightTransitionStartTimeOfDay]), 121)
                                    END
                            END                
                ) OR
                ( 1 = 1
                    -- northern hemisphere
                    AND [ar].[DaylightTransitionStartMonth] <= [ar].[DaylightTransitionEndMonth]
                    AND @LocalDate
                        BETWEEN
                            CASE
                                WHEN [ar].[DaylightTransitionStartIsFixedDateRule] = 1 THEN CONVERT(DATETIME2, RIGHT('0000' + CONVERT(NVARCHAR, YEAR(@LocalDate)), 4) + '-' + RIGHT('00' + CONVERT(NVARCHAR, [ar].[DaylightTransitionStartMonth]), 2) + '-' + RIGHT('00' + CONVERT(NVARCHAR, [ar].[DaylightTransitionStartDay]), 2) + ' ' + CONVERT(NVARCHAR, [ar].[DaylightTransitionStartTimeOfDay]), 121)
                                ELSE 
                                    CASE
                                        WHEN [ar].[DaylightTransitionStartWeek] = 5 THEN CONVERT(DATETIME2, CONVERT(NVARCHAR, [DateTimeUtil].[UDF_GetLastWeekdayInMonth] ([ar].[DaylightTransitionStartDayOfWeek], CONVERT(DATETIME2, RIGHT('0000' + CONVERT(NVARCHAR, YEAR(@LocalDate)), 4) + '-' + RIGHT('00' + CONVERT(NVARCHAR, [ar].[DaylightTransitionStartMonth]), 2) + '-' + RIGHT('00' + CONVERT(NVARCHAR, [ar].[DaylightTransitionStartDay]), 2), 121)), 121)  + ' ' + CONVERT(NVARCHAR, [ar].[DaylightTransitionStartTimeOfDay]), 121)
                                        ELSE CONVERT(DATETIME2, CONVERT(NVARCHAR, CONVERT(DATE, DATEADD(DAY, ([ar].[DaylightTransitionStartWeek] - 1) * 7, [DateTimeUtil].[UDF_GetFirstWeekdayInMonth] ([ar].[DaylightTransitionStartDayOfWeek], CONVERT(DATETIME2, RIGHT('0000' + CONVERT(NVARCHAR, YEAR(@LocalDate)), 4) + '-' + RIGHT('00' + CONVERT(NVARCHAR, [ar].[DaylightTransitionStartMonth]), 2) + '-' + RIGHT('00' + CONVERT(NVARCHAR, [ar].[DaylightTransitionStartDay]), 2), 121)))), 121)  + ' ' + CONVERT(NVARCHAR, [ar].[DaylightTransitionStartTimeOfDay]), 121)
                                    END
                            END
                        AND
                            CASE
                                WHEN [ar].[DaylightTransitionEndIsFixedDateRule] = 1 THEN CONVERT(DATETIME2, RIGHT('0000' + CONVERT(NVARCHAR, YEAR(@LocalDate)), 4) + '-' + RIGHT('00' + CONVERT(NVARCHAR, [ar].[DaylightTransitionEndMonth]), 2) + '-' + RIGHT('00' + CONVERT(NVARCHAR, [ar].[DaylightTransitionEndDay]), 2) + ' ' + CONVERT(NVARCHAR, [ar].[DaylightTransitionEndTimeOfDay]), 121)
                                ELSE 
                                    CASE
                                        WHEN [ar].[DaylightTransitionEndWeek] = 5 THEN CONVERT(DATETIME2, CONVERT(NVARCHAR, [DateTimeUtil].[UDF_GetLastWeekdayInMonth] ([ar].[DaylightTransitionEndDayOfWeek], CONVERT(DATETIME2, RIGHT('0000' + CONVERT(NVARCHAR, YEAR(@LocalDate)), 4) + '-' + RIGHT('00' + CONVERT(NVARCHAR, [ar].[DaylightTransitionEndMonth]), 2) + '-' + RIGHT('00' + CONVERT(NVARCHAR, [ar].[DaylightTransitionEndDay]), 2), 121)), 121)  + ' ' + CONVERT(NVARCHAR, [ar].[DaylightTransitionEndTimeOfDay]), 121)
                                        ELSE CONVERT(DATETIME2, CONVERT(NVARCHAR, CONVERT(DATE, DATEADD(DAY, ([ar].[DaylightTransitionEndWeek] - 1) * 7, [DateTimeUtil].[UDF_GetFirstWeekdayInMonth] ([ar].[DaylightTransitionEndDayOfWeek], CONVERT(DATETIME2, RIGHT('0000' + CONVERT(NVARCHAR, YEAR(@LocalDate)), 4) + '-' + RIGHT('00' + CONVERT(NVARCHAR, [ar].[DaylightTransitionEndMonth]), 2) + '-' + RIGHT('00' + CONVERT(NVARCHAR, [ar].[DaylightTransitionEndDay]), 2), 121)))), 121)  + ' ' + CONVERT(NVARCHAR, [ar].[DaylightTransitionEndTimeOfDay]), 121)
                                    END
                            END
                )                          
            )
    WHERE 1 = 1
        AND [tz].[Identifier] = @OriginalTimezoneIdentifier
    ;

	RETURN @Result;

END
GO

/*
=============================================
Author:         Degen, Andreas
Copyright:      All rights reserved
Create date:    2014-04-26
Description:    Converts the given UTC date to the given timezone

Parameters:
    
    @TargetTimezoneId INT: The unique ID of your target timezone (supported timezone IDs see table "DateTimeUtil.Timezone" column "Id")
    @UtcDate DATETIME2: The original UTC datetime value which you want to convert to local datetime

Return value:
    
    @Result DATETIME2: The converted datetime value in local datetime

Remarks:


Samples:

    SELECT [DateTimeUtil].[UDF_ConvertUtcToLocalByTimezoneId] (38, GETUTCDATE())

    -- northern hemisphere (+3600)
    SELECT [DateTimeUtil].[UDF_ConvertUtcToLocalByTimezoneId] (38, '2014-03-30 00:55:00')
    SELECT [DateTimeUtil].[UDF_ConvertUtcToLocalByTimezoneId] (38, '2014-03-30 01:05:00')
    SELECT [DateTimeUtil].[UDF_ConvertUtcToLocalByTimezoneId] (38, '2014-10-26 00:05:00')
    SELECT [DateTimeUtil].[UDF_ConvertUtcToLocalByTimezoneId] (38, '2014-10-26 01:05:00')
    SELECT [DateTimeUtil].[UDF_ConvertUtcToLocalByTimezoneId] (38, '2014-10-26 02:05:00')

    -- southern hemisphere (+3600)
    SELECT [DateTimeUtil].[UDF_ConvertUtcToLocalByTimezoneId] (43, '2014-04-05 23:55:00')
    SELECT [DateTimeUtil].[UDF_ConvertUtcToLocalByTimezoneId] (43, '2014-04-06 00:55:00')
    SELECT [DateTimeUtil].[UDF_ConvertUtcToLocalByTimezoneId] (43, '2014-04-06 01:55:00')
    SELECT [DateTimeUtil].[UDF_ConvertUtcToLocalByTimezoneId] (43, '2014-09-07 00:55:00')
    SELECT [DateTimeUtil].[UDF_ConvertUtcToLocalByTimezoneId] (43, '2014-09-07 01:05:00')

Change log:
    
=============================================
*/
CREATE FUNCTION [DateTimeUtil].[UDF_ConvertUtcToLocalByTimezoneId]
(
    @TargetTimezoneId INT
    ,@UtcDate DATETIME2
)
RETURNS DATETIME2
AS
BEGIN
	DECLARE
        @Result DATETIME2 = NULL
    ;

    SELECT @Result = 
        DATEADD(SECOND, [tz].[BaseUtcOffsetSec] + COALESCE([ar].[DaylightDeltaSec], 0), @UtcDate)
    FROM
        [DateTimeUtil].[Timezone] AS [tz] WITH (READUNCOMMITTED)
        LEFT JOIN [DateTimeUtil].[TimezoneAdjustmentRule] AS [ar] WITH (READUNCOMMITTED)
            ON 1 = 1
            AND [ar].[TimezoneId] = [tz].[Id]
            AND CONVERT(DATE,
                    CASE
                        -- southern hemisphere
                        WHEN [ar].[DaylightTransitionStartMonth] > [ar].[DaylightTransitionEndMonth] THEN DATEADD(SECOND, [tz].[BaseUtcOffsetSec] + [ar].[DaylightDeltaSec], @UtcDate)
                        -- northern hemisphere
                        ELSE DATEADD(SECOND, [tz].[BaseUtcOffsetSec], @UtcDate)
                    END
                ) BETWEEN [ar].[DateStart] AND [ar].[DateEnd]
            AND ( 1 = 0
                OR ( 1 = 1
                    -- southern hemisphere
                    AND [ar].[DaylightTransitionStartMonth] > [ar].[DaylightTransitionEndMonth]
                    AND NOT ( 1 = 1
                        AND DATEADD(SECOND, [tz].[BaseUtcOffsetSec] + [ar].[DaylightDeltaSec], @UtcDate) >=
                            CASE
                                WHEN [ar].[DaylightTransitionEndIsFixedDateRule] = 1 THEN CONVERT(DATETIME2, RIGHT('0000' + CONVERT(NVARCHAR, YEAR(@UtcDate)), 4) + '-' + RIGHT('00' + CONVERT(NVARCHAR, [ar].[DaylightTransitionEndMonth]), 2) + '-' + RIGHT('00' + CONVERT(NVARCHAR, [ar].[DaylightTransitionEndDay]), 2) + ' ' + CONVERT(NVARCHAR, [ar].[DaylightTransitionEndTimeOfDay]), 121)
                                ELSE 
                                    CASE
                                        WHEN [ar].[DaylightTransitionEndWeek] = 5 THEN CONVERT(DATETIME2, CONVERT(NVARCHAR, [DateTimeUtil].[UDF_GetLastWeekdayInMonth] ([ar].[DaylightTransitionEndDayOfWeek], CONVERT(DATETIME2, RIGHT('0000' + CONVERT(NVARCHAR, YEAR(@UtcDate)), 4) + '-' + RIGHT('00' + CONVERT(NVARCHAR, [ar].[DaylightTransitionEndMonth]), 2) + '-' + RIGHT('00' + CONVERT(NVARCHAR, [ar].[DaylightTransitionEndDay]), 2), 121)), 121)  + ' ' + CONVERT(NVARCHAR, [ar].[DaylightTransitionEndTimeOfDay]), 121)
                                        ELSE CONVERT(DATETIME2, CONVERT(NVARCHAR, CONVERT(DATE, DATEADD(DAY, ([ar].[DaylightTransitionEndWeek] - 1) * 7, [DateTimeUtil].[UDF_GetFirstWeekdayInMonth] ([ar].[DaylightTransitionEndDayOfWeek], CONVERT(DATETIME2, RIGHT('0000' + CONVERT(NVARCHAR, YEAR(@UtcDate)), 4) + '-' + RIGHT('00' + CONVERT(NVARCHAR, [ar].[DaylightTransitionEndMonth]), 2) + '-' + RIGHT('00' + CONVERT(NVARCHAR, [ar].[DaylightTransitionEndDay]), 2), 121)))), 121)  + ' ' + CONVERT(NVARCHAR, [ar].[DaylightTransitionEndTimeOfDay]), 121)
                                    END
                            END
                        AND DATEADD(SECOND, [tz].[BaseUtcOffsetSec], @UtcDate) <=
                            CASE
                                WHEN [ar].[DaylightTransitionStartIsFixedDateRule] = 1 THEN CONVERT(DATETIME2, RIGHT('0000' + CONVERT(NVARCHAR, YEAR(@UtcDate)), 4) + '-' + RIGHT('00' + CONVERT(NVARCHAR, [ar].[DaylightTransitionStartMonth]), 2) + '-' + RIGHT('00' + CONVERT(NVARCHAR, [ar].[DaylightTransitionStartDay]), 2) + ' ' + CONVERT(NVARCHAR, [ar].[DaylightTransitionStartTimeOfDay]), 121)
                                ELSE 
                                    CASE
                                        WHEN [ar].[DaylightTransitionStartWeek] = 5 THEN CONVERT(DATETIME2, CONVERT(NVARCHAR, [DateTimeUtil].[UDF_GetLastWeekdayInMonth] ([ar].[DaylightTransitionStartDayOfWeek], CONVERT(DATETIME2, RIGHT('0000' + CONVERT(NVARCHAR, YEAR(@UtcDate)), 4) + '-' + RIGHT('00' + CONVERT(NVARCHAR, [ar].[DaylightTransitionStartMonth]), 2) + '-' + RIGHT('00' + CONVERT(NVARCHAR, [ar].[DaylightTransitionStartDay]), 2), 121)), 121)  + ' ' + CONVERT(NVARCHAR, [ar].[DaylightTransitionStartTimeOfDay]), 121)
                                        ELSE CONVERT(DATETIME2, CONVERT(NVARCHAR, CONVERT(DATE, DATEADD(DAY, ([ar].[DaylightTransitionStartWeek] - 1) * 7, [DateTimeUtil].[UDF_GetFirstWeekdayInMonth] ([ar].[DaylightTransitionStartDayOfWeek], CONVERT(DATETIME2, RIGHT('0000' + CONVERT(NVARCHAR, YEAR(@UtcDate)), 4) + '-' + RIGHT('00' + CONVERT(NVARCHAR, [ar].[DaylightTransitionStartMonth]), 2) + '-' + RIGHT('00' + CONVERT(NVARCHAR, [ar].[DaylightTransitionStartDay]), 2), 121)))), 121)  + ' ' + CONVERT(NVARCHAR, [ar].[DaylightTransitionStartTimeOfDay]), 121)
                                    END
                            END
                    )                          
                ) OR
                ( 1 = 1
                    -- northern hemisphere
                    AND [ar].[DaylightTransitionStartMonth] <= [ar].[DaylightTransitionEndMonth]
                    AND DATEADD(SECOND, [tz].[BaseUtcOffsetSec], @UtcDate) >=
                        CASE
                            WHEN [ar].[DaylightTransitionStartIsFixedDateRule] = 1 THEN CONVERT(DATETIME2, RIGHT('0000' + CONVERT(NVARCHAR, YEAR(@UtcDate)), 4) + '-' + RIGHT('00' + CONVERT(NVARCHAR, [ar].[DaylightTransitionStartMonth]), 2) + '-' + RIGHT('00' + CONVERT(NVARCHAR, [ar].[DaylightTransitionStartDay]), 2) + ' ' + CONVERT(NVARCHAR, [ar].[DaylightTransitionStartTimeOfDay]), 121)
                            ELSE 
                                CASE
                                    WHEN [ar].[DaylightTransitionStartWeek] = 5 THEN CONVERT(DATETIME2, CONVERT(NVARCHAR, [DateTimeUtil].[UDF_GetLastWeekdayInMonth] ([ar].[DaylightTransitionStartDayOfWeek], CONVERT(DATETIME2, RIGHT('0000' + CONVERT(NVARCHAR, YEAR(@UtcDate)), 4) + '-' + RIGHT('00' + CONVERT(NVARCHAR, [ar].[DaylightTransitionStartMonth]), 2) + '-' + RIGHT('00' + CONVERT(NVARCHAR, [ar].[DaylightTransitionStartDay]), 2), 121)), 121)  + ' ' + CONVERT(NVARCHAR, [ar].[DaylightTransitionStartTimeOfDay]), 121)
                                    ELSE CONVERT(DATETIME2, CONVERT(NVARCHAR, CONVERT(DATE, DATEADD(DAY, ([ar].[DaylightTransitionStartWeek] - 1) * 7, [DateTimeUtil].[UDF_GetFirstWeekdayInMonth] ([ar].[DaylightTransitionStartDayOfWeek], CONVERT(DATETIME2, RIGHT('0000' + CONVERT(NVARCHAR, YEAR(@UtcDate)), 4) + '-' + RIGHT('00' + CONVERT(NVARCHAR, [ar].[DaylightTransitionStartMonth]), 2) + '-' + RIGHT('00' + CONVERT(NVARCHAR, [ar].[DaylightTransitionStartDay]), 2), 121)))), 121)  + ' ' + CONVERT(NVARCHAR, [ar].[DaylightTransitionStartTimeOfDay]), 121)
                                END
                        END
                    AND DATEADD(SECOND, [tz].[BaseUtcOffsetSec] + [ar].[DaylightDeltaSec], @UtcDate) <=
                        CASE
                            WHEN [ar].[DaylightTransitionEndIsFixedDateRule] = 1 THEN CONVERT(DATETIME2, RIGHT('0000' + CONVERT(NVARCHAR, YEAR(@UtcDate)), 4) + '-' + RIGHT('00' + CONVERT(NVARCHAR, [ar].[DaylightTransitionEndMonth]), 2) + '-' + RIGHT('00' + CONVERT(NVARCHAR, [ar].[DaylightTransitionEndDay]), 2) + ' ' + CONVERT(NVARCHAR, [ar].[DaylightTransitionEndTimeOfDay]), 121)
                            ELSE 
                                CASE
                                    WHEN [ar].[DaylightTransitionEndWeek] = 5 THEN CONVERT(DATETIME2, CONVERT(NVARCHAR, [DateTimeUtil].[UDF_GetLastWeekdayInMonth] ([ar].[DaylightTransitionEndDayOfWeek], CONVERT(DATETIME2, RIGHT('0000' + CONVERT(NVARCHAR, YEAR(@UtcDate)), 4) + '-' + RIGHT('00' + CONVERT(NVARCHAR, [ar].[DaylightTransitionEndMonth]), 2) + '-' + RIGHT('00' + CONVERT(NVARCHAR, [ar].[DaylightTransitionEndDay]), 2), 121)), 121)  + ' ' + CONVERT(NVARCHAR, [ar].[DaylightTransitionEndTimeOfDay]), 121)
                                    ELSE CONVERT(DATETIME2, CONVERT(NVARCHAR, CONVERT(DATE, DATEADD(DAY, ([ar].[DaylightTransitionEndWeek] - 1) * 7, [DateTimeUtil].[UDF_GetFirstWeekdayInMonth] ([ar].[DaylightTransitionEndDayOfWeek], CONVERT(DATETIME2, RIGHT('0000' + CONVERT(NVARCHAR, YEAR(@UtcDate)), 4) + '-' + RIGHT('00' + CONVERT(NVARCHAR, [ar].[DaylightTransitionEndMonth]), 2) + '-' + RIGHT('00' + CONVERT(NVARCHAR, [ar].[DaylightTransitionEndDay]), 2), 121)))), 121)  + ' ' + CONVERT(NVARCHAR, [ar].[DaylightTransitionEndTimeOfDay]), 121)
                                END
                        END
                )              
            )
    WHERE 1 = 1
        AND [tz].[Id] = @TargetTimezoneId
    ;

	RETURN @Result;

END
GO

/*
=============================================
Author:         Degen, Andreas
Copyright:      All rights reserved
Create date:    2014-04-26
Description:    Converts the given UTC date to the given timezone

Parameters:
    
    @TargetTimezoneIdentifier NVARCHAR(100): The unique identifier of your target timezone (supported timezone identifiers see table "DateTimeUtil.Timezone" column "Identifier")
    @UtcDate DATETIME2: The original UTC datetime value which you want to convert to local datetime

Return value:
    
    @Result DATETIME2: The converted datetime value in local datetime

Remarks:


Samples:

    SELECT [DateTimeUtil].[UDF_ConvertUtcToLocalByTimezoneIdentifier] ('W. Europe Standard Time', GETUTCDATE())

    -- northern hemisphere (+3600)
    SELECT [DateTimeUtil].[UDF_ConvertUtcToLocalByTimezoneIdentifier] ('W. Europe Standard Time', '2014-03-30 00:55:00')
    SELECT [DateTimeUtil].[UDF_ConvertUtcToLocalByTimezoneIdentifier] ('W. Europe Standard Time', '2014-03-30 01:05:00')
    SELECT [DateTimeUtil].[UDF_ConvertUtcToLocalByTimezoneIdentifier] ('W. Europe Standard Time', '2014-10-26 00:05:00')
    SELECT [DateTimeUtil].[UDF_ConvertUtcToLocalByTimezoneIdentifier] ('W. Europe Standard Time', '2014-10-26 01:05:00')
    SELECT [DateTimeUtil].[UDF_ConvertUtcToLocalByTimezoneIdentifier] ('W. Europe Standard Time', '2014-10-26 02:05:00')

    -- southern hemisphere (+3600)
    SELECT [DateTimeUtil].[UDF_ConvertUtcToLocalByTimezoneIdentifier] ('Namibia Standard Time', '2014-04-05 23:55:00')
    SELECT [DateTimeUtil].[UDF_ConvertUtcToLocalByTimezoneIdentifier] ('Namibia Standard Time', '2014-04-06 00:55:00')
    SELECT [DateTimeUtil].[UDF_ConvertUtcToLocalByTimezoneIdentifier] ('Namibia Standard Time', '2014-04-06 01:55:00')
    SELECT [DateTimeUtil].[UDF_ConvertUtcToLocalByTimezoneIdentifier] ('Namibia Standard Time', '2014-09-07 00:55:00')
    SELECT [DateTimeUtil].[UDF_ConvertUtcToLocalByTimezoneIdentifier] ('Namibia Standard Time', '2014-09-07 01:05:00')

Change log:
    
=============================================
*/
CREATE FUNCTION [DateTimeUtil].[UDF_ConvertUtcToLocalByTimezoneIdentifier]
(
    @TargetTimezoneIdentifier NVARCHAR(100)
    ,@UtcDate DATETIME2
)
RETURNS DATETIME2
AS
BEGIN
	DECLARE
        @Result DATETIME2 = NULL
    ;

    SELECT @Result = 
        DATEADD(SECOND, [tz].[BaseUtcOffsetSec] + COALESCE([ar].[DaylightDeltaSec], 0), @UtcDate)
    FROM
        [DateTimeUtil].[Timezone] AS [tz] WITH (READUNCOMMITTED)
        LEFT JOIN [DateTimeUtil].[TimezoneAdjustmentRule] AS [ar] WITH (READUNCOMMITTED)
            ON 1 = 1
            AND [ar].[TimezoneId] = [tz].[Id]
            AND CONVERT(DATE,
                    CASE
                        -- southern hemisphere
                        WHEN [ar].[DaylightTransitionStartMonth] > [ar].[DaylightTransitionEndMonth] THEN DATEADD(SECOND, [tz].[BaseUtcOffsetSec] + [ar].[DaylightDeltaSec], @UtcDate)
                        -- northern hemisphere
                        ELSE DATEADD(SECOND, [tz].[BaseUtcOffsetSec], @UtcDate)
                    END
                ) BETWEEN [ar].[DateStart] AND [ar].[DateEnd]
            AND ( 1 = 0
                OR ( 1 = 1
                    -- southern hemisphere
                    AND [ar].[DaylightTransitionStartMonth] > [ar].[DaylightTransitionEndMonth]
                    AND NOT ( 1 = 1
                        AND DATEADD(SECOND, [tz].[BaseUtcOffsetSec] + [ar].[DaylightDeltaSec], @UtcDate) >=
                            CASE
                                WHEN [ar].[DaylightTransitionEndIsFixedDateRule] = 1 THEN CONVERT(DATETIME2, RIGHT('0000' + CONVERT(NVARCHAR, YEAR(@UtcDate)), 4) + '-' + RIGHT('00' + CONVERT(NVARCHAR, [ar].[DaylightTransitionEndMonth]), 2) + '-' + RIGHT('00' + CONVERT(NVARCHAR, [ar].[DaylightTransitionEndDay]), 2) + ' ' + CONVERT(NVARCHAR, [ar].[DaylightTransitionEndTimeOfDay]), 121)
                                ELSE 
                                    CASE
                                        WHEN [ar].[DaylightTransitionEndWeek] = 5 THEN CONVERT(DATETIME2, CONVERT(NVARCHAR, [DateTimeUtil].[UDF_GetLastWeekdayInMonth] ([ar].[DaylightTransitionEndDayOfWeek], CONVERT(DATETIME2, RIGHT('0000' + CONVERT(NVARCHAR, YEAR(@UtcDate)), 4) + '-' + RIGHT('00' + CONVERT(NVARCHAR, [ar].[DaylightTransitionEndMonth]), 2) + '-' + RIGHT('00' + CONVERT(NVARCHAR, [ar].[DaylightTransitionEndDay]), 2), 121)), 121)  + ' ' + CONVERT(NVARCHAR, [ar].[DaylightTransitionEndTimeOfDay]), 121)
                                        ELSE CONVERT(DATETIME2, CONVERT(NVARCHAR, CONVERT(DATE, DATEADD(DAY, ([ar].[DaylightTransitionEndWeek] - 1) * 7, [DateTimeUtil].[UDF_GetFirstWeekdayInMonth] ([ar].[DaylightTransitionEndDayOfWeek], CONVERT(DATETIME2, RIGHT('0000' + CONVERT(NVARCHAR, YEAR(@UtcDate)), 4) + '-' + RIGHT('00' + CONVERT(NVARCHAR, [ar].[DaylightTransitionEndMonth]), 2) + '-' + RIGHT('00' + CONVERT(NVARCHAR, [ar].[DaylightTransitionEndDay]), 2), 121)))), 121)  + ' ' + CONVERT(NVARCHAR, [ar].[DaylightTransitionEndTimeOfDay]), 121)
                                    END
                            END
                        AND DATEADD(SECOND, [tz].[BaseUtcOffsetSec], @UtcDate) <=
                            CASE
                                WHEN [ar].[DaylightTransitionStartIsFixedDateRule] = 1 THEN CONVERT(DATETIME2, RIGHT('0000' + CONVERT(NVARCHAR, YEAR(@UtcDate)), 4) + '-' + RIGHT('00' + CONVERT(NVARCHAR, [ar].[DaylightTransitionStartMonth]), 2) + '-' + RIGHT('00' + CONVERT(NVARCHAR, [ar].[DaylightTransitionStartDay]), 2) + ' ' + CONVERT(NVARCHAR, [ar].[DaylightTransitionStartTimeOfDay]), 121)
                                ELSE 
                                    CASE
                                        WHEN [ar].[DaylightTransitionStartWeek] = 5 THEN CONVERT(DATETIME2, CONVERT(NVARCHAR, [DateTimeUtil].[UDF_GetLastWeekdayInMonth] ([ar].[DaylightTransitionStartDayOfWeek], CONVERT(DATETIME2, RIGHT('0000' + CONVERT(NVARCHAR, YEAR(@UtcDate)), 4) + '-' + RIGHT('00' + CONVERT(NVARCHAR, [ar].[DaylightTransitionStartMonth]), 2) + '-' + RIGHT('00' + CONVERT(NVARCHAR, [ar].[DaylightTransitionStartDay]), 2), 121)), 121)  + ' ' + CONVERT(NVARCHAR, [ar].[DaylightTransitionStartTimeOfDay]), 121)
                                        ELSE CONVERT(DATETIME2, CONVERT(NVARCHAR, CONVERT(DATE, DATEADD(DAY, ([ar].[DaylightTransitionStartWeek] - 1) * 7, [DateTimeUtil].[UDF_GetFirstWeekdayInMonth] ([ar].[DaylightTransitionStartDayOfWeek], CONVERT(DATETIME2, RIGHT('0000' + CONVERT(NVARCHAR, YEAR(@UtcDate)), 4) + '-' + RIGHT('00' + CONVERT(NVARCHAR, [ar].[DaylightTransitionStartMonth]), 2) + '-' + RIGHT('00' + CONVERT(NVARCHAR, [ar].[DaylightTransitionStartDay]), 2), 121)))), 121)  + ' ' + CONVERT(NVARCHAR, [ar].[DaylightTransitionStartTimeOfDay]), 121)
                                    END
                            END
                    )                          
                ) OR
                ( 1 = 1
                    -- northern hemisphere
                    AND [ar].[DaylightTransitionStartMonth] <= [ar].[DaylightTransitionEndMonth]
                    AND DATEADD(SECOND, [tz].[BaseUtcOffsetSec], @UtcDate) >=
                        CASE
                            WHEN [ar].[DaylightTransitionStartIsFixedDateRule] = 1 THEN CONVERT(DATETIME2, RIGHT('0000' + CONVERT(NVARCHAR, YEAR(@UtcDate)), 4) + '-' + RIGHT('00' + CONVERT(NVARCHAR, [ar].[DaylightTransitionStartMonth]), 2) + '-' + RIGHT('00' + CONVERT(NVARCHAR, [ar].[DaylightTransitionStartDay]), 2) + ' ' + CONVERT(NVARCHAR, [ar].[DaylightTransitionStartTimeOfDay]), 121)
                            ELSE 
                                CASE
                                    WHEN [ar].[DaylightTransitionStartWeek] = 5 THEN CONVERT(DATETIME2, CONVERT(NVARCHAR, [DateTimeUtil].[UDF_GetLastWeekdayInMonth] ([ar].[DaylightTransitionStartDayOfWeek], CONVERT(DATETIME2, RIGHT('0000' + CONVERT(NVARCHAR, YEAR(@UtcDate)), 4) + '-' + RIGHT('00' + CONVERT(NVARCHAR, [ar].[DaylightTransitionStartMonth]), 2) + '-' + RIGHT('00' + CONVERT(NVARCHAR, [ar].[DaylightTransitionStartDay]), 2), 121)), 121)  + ' ' + CONVERT(NVARCHAR, [ar].[DaylightTransitionStartTimeOfDay]), 121)
                                    ELSE CONVERT(DATETIME2, CONVERT(NVARCHAR, CONVERT(DATE, DATEADD(DAY, ([ar].[DaylightTransitionStartWeek] - 1) * 7, [DateTimeUtil].[UDF_GetFirstWeekdayInMonth] ([ar].[DaylightTransitionStartDayOfWeek], CONVERT(DATETIME2, RIGHT('0000' + CONVERT(NVARCHAR, YEAR(@UtcDate)), 4) + '-' + RIGHT('00' + CONVERT(NVARCHAR, [ar].[DaylightTransitionStartMonth]), 2) + '-' + RIGHT('00' + CONVERT(NVARCHAR, [ar].[DaylightTransitionStartDay]), 2), 121)))), 121)  + ' ' + CONVERT(NVARCHAR, [ar].[DaylightTransitionStartTimeOfDay]), 121)
                                END
                        END
                    AND DATEADD(SECOND, [tz].[BaseUtcOffsetSec] + [ar].[DaylightDeltaSec], @UtcDate) <=
                        CASE
                            WHEN [ar].[DaylightTransitionEndIsFixedDateRule] = 1 THEN CONVERT(DATETIME2, RIGHT('0000' + CONVERT(NVARCHAR, YEAR(@UtcDate)), 4) + '-' + RIGHT('00' + CONVERT(NVARCHAR, [ar].[DaylightTransitionEndMonth]), 2) + '-' + RIGHT('00' + CONVERT(NVARCHAR, [ar].[DaylightTransitionEndDay]), 2) + ' ' + CONVERT(NVARCHAR, [ar].[DaylightTransitionEndTimeOfDay]), 121)
                            ELSE 
                                CASE
                                    WHEN [ar].[DaylightTransitionEndWeek] = 5 THEN CONVERT(DATETIME2, CONVERT(NVARCHAR, [DateTimeUtil].[UDF_GetLastWeekdayInMonth] ([ar].[DaylightTransitionEndDayOfWeek], CONVERT(DATETIME2, RIGHT('0000' + CONVERT(NVARCHAR, YEAR(@UtcDate)), 4) + '-' + RIGHT('00' + CONVERT(NVARCHAR, [ar].[DaylightTransitionEndMonth]), 2) + '-' + RIGHT('00' + CONVERT(NVARCHAR, [ar].[DaylightTransitionEndDay]), 2), 121)), 121)  + ' ' + CONVERT(NVARCHAR, [ar].[DaylightTransitionEndTimeOfDay]), 121)
                                    ELSE CONVERT(DATETIME2, CONVERT(NVARCHAR, CONVERT(DATE, DATEADD(DAY, ([ar].[DaylightTransitionEndWeek] - 1) * 7, [DateTimeUtil].[UDF_GetFirstWeekdayInMonth] ([ar].[DaylightTransitionEndDayOfWeek], CONVERT(DATETIME2, RIGHT('0000' + CONVERT(NVARCHAR, YEAR(@UtcDate)), 4) + '-' + RIGHT('00' + CONVERT(NVARCHAR, [ar].[DaylightTransitionEndMonth]), 2) + '-' + RIGHT('00' + CONVERT(NVARCHAR, [ar].[DaylightTransitionEndDay]), 2), 121)))), 121)  + ' ' + CONVERT(NVARCHAR, [ar].[DaylightTransitionEndTimeOfDay]), 121)
                                END
                        END
                )              
            )
    WHERE 1 = 1
        AND [tz].[Identifier] = @TargetTimezoneIdentifier
    ;

	RETURN @Result;

END
GO

/*
=============================================
Author:         Degen, Andreas
Copyright:      All rights reserved
Create date:    2014-05-12
Description:    Returns the end of day for a given reference date

Parameters:

    @ReferenceDate DATETIME2: The reference date used as basis for the calculation

Return value:

    @Result DATETIME2: The end of the day for the given @ReferenceDate (time value is set to 23:59:59.9999999)

Remarks:

Samples:

    SELECT [DateTimeUtil].[UDF_GetEndOfDay] (GETDATE())
    SELECT [DateTimeUtil].[UDF_GetEndOfDay] ('2014-05-03 12:45:13')

Change log:
    
=============================================
*/
CREATE FUNCTION [DateTimeUtil].[UDF_GetEndOfDay]
(
    @ReferenceDate DATETIME2
)
RETURNS DATETIME2
AS
BEGIN
	DECLARE
        @Result DATETIME2 = NULL
    ;  

    SELECT @Result =
        CONVERT(DATETIME2, CONVERT(NVARCHAR, CONVERT(DATE, @ReferenceDate), 121) + ' 23:59:59.9999999')
    ;

	RETURN @Result;

END
GO

/*
=============================================
Author:         Degen, Andreas
Copyright:      All rights reserved
Create date:    2014-05-12
Description:    Returns the end of month for a given reference date

Parameters:

    @ReferenceDate DATETIME2: The reference date used as basis for the calculation

Return value:

    @Result DATETIME2: The end of the month for the given @ReferenceDate

Remarks:

Samples:

    SELECT [DateTimeUtil].[UDF_GetEndOfMonth] (GETDATE())
    SELECT [DateTimeUtil].[UDF_GetEndOfMonth] ('2014-02-03 12:45:13')

Change log:
    
=============================================
*/
CREATE FUNCTION [DateTimeUtil].[UDF_GetEndOfMonth]
(
    @ReferenceDate DATETIME2
)
RETURNS DATETIME2
AS
BEGIN
	DECLARE
        @Result DATETIME2 = NULL
    ;  

    SELECT @Result =
        [DateTimeUtil].[UDF_GetEndOfDay] (DATEADD(MONTH, DATEDIFF(MONTH, -1, @ReferenceDate), -1))
    ;

	RETURN @Result;

END
GO

/*
=============================================
Author:         Degen, Andreas
Copyright:      All rights reserved
Create date:    2014-05-12
Description:    Returns the end of quarter for a given reference date

Parameters:

    @ReferenceDate DATETIME2: The reference date used as basis for the calculation

Return value:

    @Result DATETIME2: The end of the quarter for the given @ReferenceDate

Remarks:

Samples:

    SELECT [DateTimeUtil].[UDF_GetEndOfQuarter] (GETDATE())
    SELECT [DateTimeUtil].[UDF_GetEndOfQuarter] ('2014-02-03 12:45:13')

Change log:
    
=============================================
*/
CREATE FUNCTION [DateTimeUtil].[UDF_GetEndOfQuarter]
(
    @ReferenceDate DATETIME2
)
RETURNS DATETIME2
AS
BEGIN
	DECLARE
        @Result DATETIME2 = NULL
    ;  

    SELECT @Result =
        [DateTimeUtil].[UDF_GetEndOfDay] (DATEADD(QUARTER, DATEDIFF(QUARTER, -1, @ReferenceDate), -1))
    ;

	RETURN @Result;

END
GO

/*
=============================================
Author:         Degen, Andreas
Copyright:      All rights reserved
Create date:    2014-05-12
Description:    Returns the end of week for a given reference date based on your @@DATEFIRST setting

Parameters:

    @ReferenceDate DATETIME2: The reference date used as basis for the calculation

Return value:

    @Result DATETIME2: The end of the week for the given @ReferenceDate based on your @@DATEFIRST setting

Remarks:

Samples:

    SET DATEFIRST 7 -- Sunday
    SET DATEFIRST 1 -- Monday

    SELECT [DateTimeUtil].[UDF_GetEndOfWeek] (GETDATE())
    SELECT [DateTimeUtil].[UDF_GetEndOfWeek] ('2014-02-03 12:45:13')

Change log:
    
=============================================
*/
CREATE FUNCTION [DateTimeUtil].[UDF_GetEndOfWeek]
(
    @ReferenceDate DATETIME2
)
RETURNS DATETIME2
AS
BEGIN
	DECLARE
        @Result DATETIME2 = NULL
        ,@DayOfWeek INT = @@DATEFIRST
    ;  

    DECLARE
        @FirstOfWeekday DATETIME2 = DATEADD(DAY, @DayOfWeek - 1, 0)
    ;

    SELECT @Result =
        [DateTimeUtil].[UDF_GetEndOfDay](DATEADD(DAY, 6, [DateTimeUtil].[UDF_GetStartOfWeek](@ReferenceDate)))
    ;

	RETURN @Result;

END
GO

/*
=============================================
Author:         Degen, Andreas
Copyright:      All rights reserved
Create date:    2014-05-12
Description:    Returns the end of year for a given reference date

Parameters:

    @ReferenceDate DATETIME2: The reference date used as basis for the calculation

Return value:

    @Result DATETIME2: The end of the year for the given @ReferenceDate

Remarks:

Samples:

    SELECT [DateTimeUtil].[UDF_GetEndOfYear] (GETDATE())
    SELECT [DateTimeUtil].[UDF_GetEndOfYear] ('2013-02-03 12:45:13')

Change log:
    
=============================================
*/
CREATE FUNCTION [DateTimeUtil].[UDF_GetEndOfYear]
(
    @ReferenceDate DATETIME2
)
RETURNS DATETIME2
AS
BEGIN
	DECLARE
        @Result DATETIME2 = NULL
    ;  

    SELECT @Result =
        [DateTimeUtil].[UDF_GetEndOfDay] (DATEADD(YEAR, DATEDIFF(YEAR, -1, @ReferenceDate), -1))
    ;

	RETURN @Result;

END
GO

/*
=============================================
Author:         Degen, Andreas
Copyright:      All rights reserved
Create date:    2014-04-26
Description:    Returns the first occurence of a given weekday in a month

Parameters:
    
    @DayOfWeek INT: The day of week you search for; see remarks for supported values
    @ReferenceDate DATETIME2: The reference date used as basis for the calculation

Return value:
    
    @Result DATETIME2: The first occurence of a given weekday in a month

Remarks:

    Supported values for parameter @DayOfWeek
        1 = Monday
        2 = Tuesday
        3 = Wednesday
        4 = Thursday
        5 = Friday
        6 = Saturday
        0 or 7 = Sunday

Samples:

    SELECT [DateTimeUtil].[UDF_GetFirstWeekdayInMonth] (0, GETDATE())
    SELECT [DateTimeUtil].[UDF_GetFirstWeekdayInMonth] (1, GETDATE())
    SELECT [DateTimeUtil].[UDF_GetFirstWeekdayInMonth] (2, GETDATE())
    SELECT [DateTimeUtil].[UDF_GetFirstWeekdayInMonth] (3, GETDATE())
    SELECT [DateTimeUtil].[UDF_GetFirstWeekdayInMonth] (4, GETDATE())
    SELECT [DateTimeUtil].[UDF_GetFirstWeekdayInMonth] (5, GETDATE())
    SELECT [DateTimeUtil].[UDF_GetFirstWeekdayInMonth] (6, GETDATE())
    SELECT [DateTimeUtil].[UDF_GetFirstWeekdayInMonth] (7, GETDATE())

Change log:
    
=============================================
*/
CREATE FUNCTION [DateTimeUtil].[UDF_GetFirstWeekdayInMonth]
(
    @DayOfWeek INT
    ,@ReferenceDate DATETIME2
)
RETURNS DATE
AS
BEGIN
	DECLARE
        @Result DATE = NULL
    ;  

    SELECT @Result =
        DATEADD(
            DAY
            ,7
            ,[DateTimeUtil].[UDF_GetLastWeekdayInMonth] (@DayOfWeek, DATEADD(MONTH, -1, @ReferenceDate))
        )
    ;

	RETURN @Result;

END
GO


/*
=============================================
Author:         Degen, Andreas
Copyright:      All rights reserved
Create date:    2014-04-26
Description:    Returns the last occurence of a given weekday in a month

Parameters:
    
    @DayOfWeek INT: The day of week you search for; see remarks for supported values
    @ReferenceDate DATETIME2: The reference date used as basis for the calculation

Return value:
    
    @Result DATETIME2: The last occurence of a given weekday in a month

Remarks:

    Supported values for parameter @DayOfWeek
        1 = Monday
        2 = Tuesday
        3 = Wednesday
        4 = Thursday
        5 = Friday
        6 = Saturday
        0 or 7 = Sunday

Samples:

    SELECT [DateTimeUtil].[UDF_GetLastWeekdayInMonth] (0, GETDATE())
    SELECT [DateTimeUtil].[UDF_GetLastWeekdayInMonth] (1, GETDATE())
    SELECT [DateTimeUtil].[UDF_GetLastWeekdayInMonth] (2, GETDATE())
    SELECT [DateTimeUtil].[UDF_GetLastWeekdayInMonth] (3, GETDATE())
    SELECT [DateTimeUtil].[UDF_GetLastWeekdayInMonth] (4, GETDATE())
    SELECT [DateTimeUtil].[UDF_GetLastWeekdayInMonth] (5, GETDATE())
    SELECT [DateTimeUtil].[UDF_GetLastWeekdayInMonth] (6, GETDATE())
    SELECT [DateTimeUtil].[UDF_GetLastWeekdayInMonth] (7, GETDATE())

Change log:
    
=============================================
*/
CREATE FUNCTION [DateTimeUtil].[UDF_GetLastWeekdayInMonth]
(
    @DayOfWeek INT
    ,@ReferenceDate DATETIME2
)
RETURNS DATE
AS
BEGIN
	DECLARE
        @Result DATE = NULL
    ;

    -- support of .NET values
    IF @DayOfWeek = 0 BEGIN
        SET @DayOfWeek = 7;
    END;  

    DECLARE
        @FirstOfWeekday DATETIME2 = DATEADD(DAY, @DayOfWeek - 1, 0)
    ;  

    SELECT @Result =
        DATEADD(
            DAY
            ,DATEDIFF(
                DAY
                ,@FirstOfWeekday
                ,DATEADD(
                    MONTH
                    ,DATEDIFF(
                        MONTH
                        ,0
                        ,@ReferenceDate
                    )
                    ,DATEADD(
                        DAY
                        ,-1
                        ,DATEADD(
                            MONTH
                            ,1
                            ,0
                        )
                    )
                )
            ) / 7 * 7
            ,@FirstOfWeekday
        )
    ;

	RETURN @Result;

END
GO

/*
=============================================
Author:         Degen, Andreas
Copyright:      All rights reserved
Create date:    2014-05-12
Description:    Returns the start of day for a given reference date

Parameters:

    @ReferenceDate DATETIME2: The reference date used as basis for the calculation

Return value:

    @Result DATETIME2: The start of the day for the given @ReferenceDate (time value is set to 00:00:00)

Remarks:

Samples:

    SELECT [DateTimeUtil].[UDF_GetStartOfDay] (GETDATE())
    SELECT [DateTimeUtil].[UDF_GetStartOfDay] ('2014-05-03 12:45:13')

Change log:
    
=============================================
*/
CREATE FUNCTION [DateTimeUtil].[UDF_GetStartOfDay]
(
    @ReferenceDate DATETIME2
)
RETURNS DATETIME2
AS
BEGIN
	DECLARE
        @Result DATETIME2 = NULL
    ;  

    SELECT @Result =
        CONVERT(DATETIME2, CONVERT(DATE, @ReferenceDate))
    ;

	RETURN @Result;

END
GO

/*
=============================================
Author:         Degen, Andreas
Copyright:      All rights reserved
Create date:    2014-05-12
Description:    Returns the start of month for a given reference date

Parameters:

    @ReferenceDate DATETIME2: The reference date used as basis for the calculation

Return value:

    @Result DATETIME2: The start of the month for the given @ReferenceDate

Remarks:

Samples:

    SELECT [DateTimeUtil].[UDF_GetStartOfMonth] (GETDATE())
    SELECT [DateTimeUtil].[UDF_GetStartOfMonth] ('2014-02-03 12:45:13')

Change log:
    
=============================================
*/
CREATE FUNCTION [DateTimeUtil].[UDF_GetStartOfMonth]
(
    @ReferenceDate DATETIME2
)
RETURNS DATETIME2
AS
BEGIN
	DECLARE
        @Result DATETIME2 = NULL
    ;  

    SET @ReferenceDate = CONVERT(DATE, @ReferenceDate);

    SELECT @Result =
        DATEADD(MONTH, DATEDIFF(MONTH, 0, @ReferenceDate), 0)
    ;

	RETURN @Result;

END
GO

/*
=============================================
Author:         Degen, Andreas
Copyright:      All rights reserved
Create date:    2014-05-12
Description:    Returns the start of quarter for a given reference date

Parameters:

    @ReferenceDate DATETIME2: The reference date used as basis for the calculation

Return value:

    @Result DATETIME2: The start of the quarter for the given @ReferenceDate

Remarks:

Samples:

    SELECT [DateTimeUtil].[UDF_GetStartOfQuarter] (GETDATE())
    SELECT [DateTimeUtil].[UDF_GetStartOfQuarter] ('2014-02-03 12:45:13')

Change log:
    
=============================================
*/
CREATE FUNCTION [DateTimeUtil].[UDF_GetStartOfQuarter]
(
    @ReferenceDate DATETIME2
)
RETURNS DATETIME2
AS
BEGIN
	DECLARE
        @Result DATETIME2 = NULL
    ;  

    SELECT @Result =
        DATEADD(QUARTER, DATEDIFF(QUARTER, 0, @ReferenceDate), 0)
    ;

	RETURN @Result;

END
GO

/*
=============================================
Author:         Degen, Andreas
Copyright:      All rights reserved
Create date:    2014-05-12
Description:    Returns the start of week for a given reference date based on your @@DATEFIRST setting

Parameters:

    @ReferenceDate DATETIME2: The reference date used as basis for the calculation

Return value:

    @Result DATETIME2: The start of the week for the given @ReferenceDate based on your @@DATEFIRST setting

Remarks:

Samples:

    SET DATEFIRST 7 -- Sunday
    SET DATEFIRST 1 -- Monday

    SELECT [DateTimeUtil].[UDF_GetStartOfWeek] (GETDATE())
    SELECT [DateTimeUtil].[UDF_GetStartOfWeek] ('2014-02-03 12:45:13')

Change log:
    2015-01-13 | adss | Bug fix with DATEDIFF(WEEK...) (Issue #1248)
	
=============================================
*/
CREATE FUNCTION [DateTimeUtil].[UDF_GetStartOfWeek]
(
    @ReferenceDate DATETIME2
)
RETURNS DATETIME2
AS
BEGIN
	DECLARE
        @Result DATETIME2 = NULL
        ,@DayOfWeek INT = @@DATEFIRST
    ;  

    DECLARE
        @FirstOfWeekday DATETIME2 = DATEADD(DAY, @DayOfWeek - 1, 0)
    ;

    SELECT @Result =
        --DATEADD(WEEK, DATEDIFF(WEEK, @FirstOfWeekday, @ReferenceDate), @FirstOfWeekday)
        DATEADD(WEEK, DATEDIFF(DAY, @FirstOfWeekday, @ReferenceDate) / 7, @FirstOfWeekday)
    ;

	RETURN @Result;

END
GO

/*
=============================================
Author:         Degen, Andreas
Copyright:      All rights reserved
Create date:    2014-05-12
Description:    Returns the start of year for a given reference date

Parameters:

    @ReferenceDate DATETIME2: The reference date used as basis for the calculation

Return value:

    @Result DATETIME2: The start of the year for the given @ReferenceDate

Remarks:

Samples:

    SELECT [DateTimeUtil].[UDF_GetStartOfYear] (GETDATE())
    SELECT [DateTimeUtil].[UDF_GetStartOfYear] ('2013-05-03 12:45:13')

Change log:
    
=============================================
*/
CREATE FUNCTION [DateTimeUtil].[UDF_GetStartOfYear]
(
    @ReferenceDate DATETIME2
)
RETURNS DATETIME2
AS
BEGIN
	DECLARE
        @Result DATETIME2 = NULL
    ;  

    SET @ReferenceDate = CONVERT(DATE, @ReferenceDate);

    SELECT @Result =
        DATEADD(YEAR, DATEDIFF(YEAR, 0, @ReferenceDate), 0)
    ;

	RETURN @Result;

END
GO;


ALTER TABLE [TSqlToolbox].[DateTimeUtil].[Timezone] ADD UTC NVARCHAR(12);

UPDATE [TSqlToolbox].[DateTimeUtil].[Timezone]
SET UTC = (
		CASE 
			WHEN (substring(DisplayName, charindex('(', [DisplayName]) + 1, 4)) = 'UTC)'
				THEN 'UTC'
			ELSE LTRIM(RTRIM(REPLACE(substring([DisplayName], charindex('(', [DisplayName]) + 1, 9), 'UTC', 'UTC ')))
			END
		);
